### Menampilkan Detail Produk

Setelah homepage sudah kita buat agar menampilkan data secara dinamis. Selanjutnya kita akan membuat halaman detail produk dengan tiga langkah sederhana.

1. Koreksi **HomepageController**
2. Koreksi model **Produk**
3. Koreksi views **produkdetail**
4. Buat route **produkdetail**
5. Edit menu toko (**menu.blade.php**)

**#1. HomepageController**

Buka file **HomepageController.php** kemudian koreksi `function produkdetail($id)`

```php
public function produkdetail($id) {
    $itemproduk = Produk::where('slug_produk', $id)
                        ->where('status', 'publish')
                        ->first();
    if ($itemproduk) {
        $data = array('title' => $itemproduk->nama_produk,
                    'itemproduk' => $itemproduk);
        return view('homepage.produkdetail', $data);
    } else {
        // kalo produk ga ada, jadinya tampil halaman tidak ditemukan (error 404)
        return abort('404');
    }
}
```

**#2. Model Produk**

Pada model **Produk** tambahkan 1 buah function untuk menampilkan list produk promo.

```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    use HasFactory;

    protected $table = 'produk';
    protected $fillable = [
        'kategori_id',
        'user_id',
        'kode_produk',
        'nama_produk',
        'slug_produk',
        'deskripsi_produk',
        'foto',
        'qty',
        'satuan',
        'harga',
        'status',
    ];

    public function kategori()
    {
        return $this->belongsTo(Kategori::class, 'kategori_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function images()
    {
        return $this->hasMany(ProdukImage::class, 'produk_id');
    }
    // function untuk menampilkan list produk promo
    public function promo() {
        return $this->hasOne(ProdukPromo::class, 'produk_id');
    }
}
```

**#3. Koreksi views produkdetail.blade.php**

xxxxxxxxxx <?php​use Illuminate\Database\Migrations\Migration;use Illuminate\Database\Schema\Blueprint;use Illuminate\Support\Facades\Schema;​class CreateCartsTable extends Migration{    /**     * Run the migrations.     *     * @return void     */    public function up()    {        Schema::create('cart', function (Blueprint $table) {            $table->increments('id');            $table->integer('user_id')->unsigned();            $table->string('no_invoice');            $table->string('status_cart'); // ada 2 yaitu cart, checkout            $table->string('status_pembayaran'); // ada 2 sudah dan belum            $table->string('status_pengiriman'); // ada 2 yaitu belum dan sudah            $table->string('no_resi')->nullable();            $table->string('ekspedisi')->nullable();            $table->double('subtotal', 12, 2)->default(0);            $table->double('ongkir', 12, 2)->default(0);            $table->double('diskon', 12, 2)->default(0);            $table->double('total', 12, 2)->default(0);            $table->foreign('user_id')->references('id')->on('users');            $table->timestamps();        });    }​    /**     * Reverse the migrations.     *     * @return void     */    public function down()    {        Schema::dropIfExists('cart');    }}php

![image-20220421103535977](img\image-20220421103535977.png)

```php+HTML
@extends('layouts.template')
@section('content')
<div class="container">
  <div class="row mt-4">
    <div class="col col-lg-8 col-md-8">
      <div id="carousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
        @foreach($itemproduk->images as $index => $image)
        @if($index == 0)
          <div class="carousel-item active">
              <img src="{{ \Storage::url($image->foto) }}" class="d-block w-100" alt="...">
          </div>
        @else
          <div class="carousel-item">
              <img src="{{ \Storage::url($image->foto) }}" class="d-block w-100" alt="...">
          </div>
        @endif
        @endforeach
        </div>
        <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>
    <!-- deskripsi produk -->
    <div class="col col-lg-4 col-md-4">
      <div class="row">
        <div class="col">
          <div class="card">
            <div class="card-body">
              <span class="small">{{ $itemproduk->kategori->nama_kategori }}</span>
              <h5>{{ $itemproduk->nama_produk }}</h5>
              <!-- cek apakah ada promo -->
              @if($itemproduk->promo != null)
              <p>
                Rp. <del>{{ number_format($itemproduk->promo->harga_awal, 2) }}</del>
                <br />
                Rp. {{ number_format($itemproduk->promo->harga_akhir, 2) }}
              </p>
              @else
              <p>
                Rp. {{ number_format($itemproduk->harga, 2) }}
              </p>
              @endif
              <button class="btn btn-sm btn-outline-secondary">
              <i class="far fa-heart"></i> Tambah ke wishlist
              </button>
            </div>
          </div>
        </div>
      </div>
      <div class="row mt-4">
        <div class="col">
          <div class="card">
            <div class="card-body">
              <button class="btn btn-block btn-primary">
              <i class="fa fa-shopping-cart"></i> Tambahkan Ke Keranjang
              </button>
              <button class="btn btn-block btn-danger mt-4">
              <i class="fa fa-shopping-basket"></i> Beli Sekarang
              </button>
            </div>
            <div class="card-footer">
              <div class="row mt-4">
                <div class="col text-center">
                  <i class="fa fa-truck-moving"></i> 
                  <p>Pengiriman Cepat</p>
                </div>
                <div class="col text-center">
                  <i class="fa fa-calendar-week"></i> 
                  <p>Garansi 7 hari</p>
                </div>
                <div class="col text-center">
                  <i class="fa fa-money-bill"></i> 
                  <p>Pembayaran Aman</p>
                </div>
              </div>            
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row mt-4">
    <div class="col">
      <div class="card">
        <div class="card-header">
          Deskripsi
        </div>
        <div class="card-body">
          {{ $itemproduk->deskripsi_produk }}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
```

**#4. Buat route produkdetail**

Buka file **web.php** dan tambahkan kode berikut di atas `Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function ()`

```php
// produkdetail
Route::get('produk/{id}', [HomepageController::class, 'produkdetail']);
```

Sekarang buka halaman homepage kemudian klik salah satu produk.

![image-20220421104741691](img\image-20220421104741691.png)

### Produk per Kategori

Pada bagian ini kita akan mengelompokkan produk per kategori. Di tutorial sebelumnya kita telah membuat desain tampilannya. Jadi sekarang kita cukup mengoreksinya agar menampilkan data yang kita inputkan di dashboard.

Langkah-langkahnya :

1. Koreksi **HomepageController**
2. Koreksi **views kategori**
3. Koreksi **views produk**
4. Tambahkan route ke **web.php**

**#1. Koreksi HomepageController**

Buka file **HomepageController.php** dan koreksi menjadi seperti berikut

```php
<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use App\Models\Produk;
use App\Models\ProdukPromo;
use App\Models\Slideshow;
use Illuminate\Http\Request;

class HomepageController extends Controller
{
    public function index()
    {
        $itemproduk = Produk::orderBy('created_at', 'desc')
            ->limit(6)
            ->get();
        $itempromo = ProdukPromo::orderBy('created_at', 'desc')
            ->limit(6)
            ->get();
        $itemkategori = Kategori::orderBy('nama_kategori', 'asc')
            ->limit(6)
            ->get();
        $itemslide = Slideshow::get();
        $data = [
            'title' => 'Homepage',
            'itemproduk' => $itemproduk,
            'itempromo' => $itempromo,
            'itemkategori' => $itemkategori,
            'itemslide' => $itemslide,
        ];
        return view('homepage.index', $data);
    }
    public function about()
    {
        $data = ['title' => 'Tentang Kami'];
        return view('homepage.about', $data);
    }

    public function kontak()
    {
        $data = ['title' => 'Kontak Kami'];
        return view('homepage.kontak', $data);
    }

    public function kategori()
    {
        $itemkategori = Kategori::orderBy('nama_kategori', 'asc')
            ->limit(6)
            ->get();
        $itemproduk = Produk::orderBy('created_at', 'desc')
            ->limit(6)
            ->get();
        $data = ['title' => 'Kategori Produk', 'itemkategori' => $itemkategori, 'itemproduk' => $itemproduk];
        return view('homepage.kategori', $data);
    }

    public function kategoribyslug(Request $request, $slug)
    {
        $itemproduk = Produk::orderBy('nama_produk', 'desc')
            ->where('status', 'publish')
            ->whereHas('kategori', function ($q) use ($slug) {
                $q->where('slug_kategori', $slug);
            })
            ->paginate(18);
        $listkategori = Kategori::orderBy('nama_kategori', 'asc')
            ->where('status', 'publish')
            ->get();
        $itemkategori = Kategori::where('slug_kategori', $slug)
            ->where('status', 'publish')
            ->first();
        if ($itemkategori) {
            $data = ['title' => $itemkategori->nama_kategori, 'itemproduk' => $itemproduk, 'listkategori' => $listkategori, 'itemkategori' => $itemkategori];
            return view('homepage.produk', $data)->with('no', ($request->input('page') - 1) * 18);
        } else {
            return abort('404');
        }
    }

    public function produk(Request $request)
    {
        $itemproduk = Produk::orderBy('nama_produk', 'desc')
            ->where('status', 'publish')
            ->paginate(18);
        $listkategori = Kategori::orderBy('nama_kategori', 'asc')
            ->where('status', 'publish')
            ->get();
        $data = ['title' => 'Produk', 'itemproduk' => $itemproduk, 'listkategori' => $listkategori];
        return view('homepage.produk', $data)->with('no', ($request->input('page') - 1) * 18);
    }

    public function produkdetail($id)
    {
        $itemproduk = Produk::where('slug_produk', $id)
            ->where('status', 'publish')
            ->first();
        if ($itemproduk) {
            $data = ['title' => $itemproduk->nama_produk, 'itemproduk' => $itemproduk];
            return view('homepage.produkdetail', $data);
        } else {
            // kalo produk ga ada, jadinya tampil halaman tidak ditemukan (error 404)
            return abort('404');
        }
    }
}
```

**#2. Koreksi file views homepage/kategori.blade.php**

```php+HTML
@extends('layouts.template')
@section('content')
<div class="container">
  <!-- kategori produk -->
  <div class="row mt-4">
    <div class="col col-md-12 col-sm-12 mb-4">
      <h2 class="text-center">Kategori Produk</h2>
    </div>
    @foreach($itemkategori as $kategori)
    <!-- kategori pertama -->
    <div class="col-md-4">
      <div class="card mb-4 shadow-sm">
        <a href="{{ URL::to('kategori/'.$kategori->slug_kategori) }}">
          @if($kategori->foto != null)
          <img src="{{ \Storage::url($kategori->foto) }}" alt="{{ $kategori->nama_kategori }}" class="card-img-top">
          @else
          <img src="{{asset('images/bag.jpg') }}" alt="{{ $kategori->nama_kategori }}" class="card-img-top">
          @endif
        </a>
        <div class="card-body">
          <a href="{{ URL::to('kategori/'.$kategori->slug_kategori) }}" class="text-decoration-none">
            <p class="card-text">{{ $kategori->nama_kategori }}</p>
          </a>
        </div>
      </div>
    </div>
    @endforeach
  <!-- end kategori produk -->
  <!-- produk Terbaru-->
  <div class="row mt-4">
    <div class="col col-md-12 col-sm-12 mb-4">
      <h2 class="text-center">Terbaru</h2>
    </div>
    @foreach($itemproduk as $produk)
    <!-- produk pertama -->
    <div class="col-md-4">
      <div class="card mb-4 shadow-sm">
        <a href="{{ URL::to('produk/'.$produk->slug_produk) }}">
          @if($produk->foto != null)
          <img src="{{ \Storage::url($produk->foto) }}" alt="{{ $produk->nama_produk }}" class="card-img-top">
          @else
          <img src="{{ asset('images/bag.jpg') }}" alt="{{ $produk->nama_produk }}" class="card-img-top">
          @endif
        </a>
        <div class="card-body">
          <a href="{{ URL::to('produk/'.$produk->slug_produk ) }}" class="text-decoration-none">
            <p class="card-text">
              {{ $produk->nama_produk }}
            </p>
          </a>
          <div class="row mt-4">
            <div class="col">
              <button class="btn btn-light">
                <i class="far fa-heart"></i>
              </button>
            </div>
            <div class="col-auto">
              <p>
                Rp. {{ number_format($produk->harga, 2) }}
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
    @endforeach
</div>
@endsection
```

**#3. Koreksi file views homepage/produk.blade.php**

```php+HTML
@extends('layouts.template')
@section('content')
    <div class="container">
        <div class="row mt-4">
            <div class="col col-lg-3 col-md-3 mb-2">
                <div class="card">
                    <div class="card-header">
                        Kategori
                    </div>
                    <ul class="list-group list-group-flush">
                        @foreach ($listkategori as $kategori)
                            <a href="{{ URL::to('kategori/' . $kategori->slug_kategori) }}" class="text-decoration-none">
                                <li class="list-group-item">{{ $kategori->nama_kategori }}</li>
                            </a>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col col-lg-9 col-md-9 mb-2">
                @if (isset($itemkategori))
                    <h3>{{ $itemkategori->nama_kategori }}</h3>
                @else
                    <h3>Semua Kategori</h3>
                @endif
                <div class="row mt-4">
                    @foreach ($itemproduk as $produk)
                        <div class="col-md-4">
                            <div class="card mb-4 shadow-sm">
                                <a href="{{ URL::to('produk/' . $produk->slug_produk) }}">
                                    @if ($produk->foto != null)
                                        <img src="{{ \Storage::url($produk->foto) }}" alt="{{ $produk->nama_produk }}"
                                            class="card-img-top">
                                    @else
                                        <img src="{{ asset('images/bag.jpg') }}" alt="{{ $produk->nama_produk }}"
                                            class="card-img-top">
                                    @endif
                                </a>
                                <div class="card-body">
                                    <a href="{{ URL::to('produk/' . $produk->slug_produk) }}" class="text-decoration-none">
                                        <p class="card-text">
                                            {{ $produk->nama_produk }}
                                        </p>
                                    </a>
                                    <div class="row mt-4">
                                        <div class="col">
                                            <button class="btn btn-light">
                                                <i class="far fa-heart"></i>
                                            </button>
                                        </div>
                                        <div class="col-auto">
                                            <p>
                                                Rp. {{ number_format($produk->harga, 2) }}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="row">
                    <div class="col">
                        {{ $itemproduk->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
```

**#4. Tambahkan route kategoribyslug di web.php**

Buka file **web.php** dan ubah kode sesuai berikut: 

```php
    <?php

    use App\Http\Controllers\CustomerController;
    use App\Http\Controllers\DashboardController;
    use App\Http\Controllers\HomepageController;
    use App\Http\Controllers\ImageController;
    use App\Http\Controllers\KategoriController;
    use App\Http\Controllers\LaporanController;
    use App\Http\Controllers\LatihanController;
    use App\Http\Controllers\ProdukController;
    use App\Http\Controllers\ProdukPromoController;
    use App\Http\Controllers\SlideshowController;
    use App\Http\Controllers\TransaksiController;
    use App\Http\Controllers\UserController;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Route;

    /*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register web routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | contains the "web" middleware group. Now create something great!
    |
    */

    Route::get('/', [HomepageController::class, 'index']);
    Route::get('/about', [HomepageController::class, 'about']);
    Route::get('/kontak', [HomepageController::class, 'kontak']);
    Route::get('/kategori', [HomepageController::class, 'kategori']);
    Route::get('/kategori/{slug}', [HomepageController::class, 'kategoribyslug']);
    Route::get('/produk', [HomepageController::class, 'produk']);
    Route::get('produk/{id}', [HomepageController::class, 'produkdetail']);

    Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
        Route::get('/', [DashboardController::class, 'index']);
        // route kategori
        Route::resource('kategori', KategoriController::class);
        // route produk
        Route::resource('produk', ProdukController::class);
        // route customer
        Route::resource('customer', CustomerController::class);
        // route transaksi
        Route::resource('transaksi', TransaksiController::class);
        // profil
        Route::get('profil', [UserController::class, 'index']);
        // setting profil
        Route::get('setting', [UserController::class, 'setting']);
        // form laporan
        Route::get('laporan', [LaporanController::class, 'index']);
        // proses laporan
        Route::get('proseslaporan', [LaporanController::class, 'proses']);

        // image
        Route::get('image', [ImageController::class, 'index']);
        // simpan image
        Route::post('image', [ImageController::class, 'store']);
        // hapus image by id
        Route::delete('image/{id}', [ImageController::class, 'destroy']);


        // upload image kategori
        Route::post('imagekategori', [KategoriController::class, 'uploadimage']);
        // hapus image kategori
        Route::delete('imagekategori/{id}', [KategoriController::class, 'deleteimage']);

        // upload image produk
        Route::post('produkimage', [ProdukController::class, 'uploadimage']);
        // hapus image produk
        Route::delete('produkimage/{id}', [ProdukController::class, 'deleteimage']);

        // hapus image produk
        Route::delete('produkimage/{id}', [ProdukController::class, 'deleteimage']);
        // slideshow
        Route::resource('slideshow', SlideshowController::class);

        // produk promo
        Route::resource('promo', ProdukPromoController::class);
        // load async produk
        Route::get('loadprodukasync/{id}', [ProdukController::class, 'loadasync']);
    });

    // Route::get('/', function () {
    //     return view('welcome');
    // });
    // Route::get('/halo', function () {
    //     return "Halo nama saya fadlur";
    // });
    // Route::get('/latihan', [LatihanController::class,'index']);
    // Route::get('/blog/{id}', [LatihanController::class,'blog']);
    // Route::get('/blog/{idblog}/komentar/{idkomentar}',[LatihanController::class,'komentar']);
    // Route::get('/beranda', [LatihanController::class,'beranda']);

    Auth::routes();

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

```

**#5. Edit menu toko (menu.blade.php)**

![image-20220421105823813](img\image-20220421105823813.png)

```php+HTML
<nav class="navbar navbar-expand-lg navbar-light bg-light mb-4">
    <div class="container">
        <a class="navbar-brand" href="/">Toko</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="mr-auto navbar-nav"></ul>
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="/">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL::to('produk') }}">Produk</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL::to('kategori') }}">Kategori</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL::to('kontak') }}">Kontak</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL::to('about') }}">Tentang Kami</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL::to('login') }}">Login</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
```

Sekarang coba akses halaman produk di http://toko-klowor.local/kategori kemudian klik salah satu kategori.

![image-20220421105950938](img\image-20220421105950938.png)

### Wishlist

Pada halaman detail produk kita akan melakukan beberapa aksi, salah satunya adalah memasukkan produk tersebut ke wishlist kita dengan mengklik tombol "hati". Fungsi wishlist hanya bisa diakses oleh user yang telah login.

Langkah-langkah untuk membuat wishlist :

1. Buat **migrations wishlist**
2. Buat **model wishlist**
3. Buat **Controller Wishlist**
4. Buat **views wishlist** dan koreksi **views produkdetail.blade.php**
5. Koreksi **HomepageController**
6. Koreksi **menu dashboard**
7. Tambahkan **route wishlist ke web.php**

Sekarang kita mulai dengan menjalankan perintah untuk membuat model sekaligus migration dan controller. Buka terminal dan ketikkan perintah berikut kemudian tekan enter.

```markup
php artisan make:model Wishlist -crm
```

Setelah proses selesai, akan terdapat 3 buah file baru yaitu:

1. Migrations
2. Model
3. Controller

**#1. Update file migrations**

Buka file **migrations _create_wishtlists_table.php** kemudian koreksi isinya.

```php
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWishlistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wishlist', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produk_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('produk_id')->references('id')->on('produk');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wishlist');
    }
}
```

Setelah disimpan, kembali ke terminal dan jalankan perintah migrate kemudian tekan enter untuk mengeksekusi migrations tersebut.

```php
php artisan migrate
```

![image-20220421121140991](D:/www/pemrogaman-web-laravel/img/image-20220421121140991.png)

**#2. Model Wishlist**

Setelah selesai mengedit file migrations, selanjutnya kita perlu mengedit file model agar bisa menghandle transaksi database pada table wishlist sesuai yang telah kita buat di file migrations.

Buka file **Wishlist.php** dan update seperti berikut

```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
    protected $table = 'wishlist';
    protected $fillable = [
        'produk_id',
        'user_id',
    ];

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
```

**#3. Wishtlist Controller**

Migrations dan model telah selesai kita update, sekarang kita lanjut untuk file controller-nya. Di dalam file controller kita hanya akan membuat fungsi untuk menyimpan wishlist sesuai produk yang dipilih dan menghapus wishlist tersebut. Jadi semisal proses "A" diklik tombol wishlist akan dicek oleh sistem, kalau belum ada dalam wishlist maka akan ditambahkan. Tetapi kalau sudah ada malah akan dihapus.

Buka file **WishlistController.php** dan update menjadi seperti berikut.

```php
<?php

namespace App\Http\Controllers;

use App\Models\Wishlist;
use Illuminate\Http\Request;

class WishlistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $itemuser = $request->user();
        $itemwishlist = Wishlist::where('user_id', $itemuser->id)
            ->paginate(10);
        $data = array(
            'title' => 'Wishlist',
            'itemwishlist' => $itemwishlist
        );
        return view('wishlist.index', $data)->with('no', ($request->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'produk_id' => 'required',
        ]);
        $itemuser = $request->user();
        $validasiwishlist = Wishlist::where('produk_id', $request->produk_id)
            ->where('user_id', $itemuser->id)
            ->first();
        if ($validasiwishlist) {
            $validasiwishlist->delete(); //kalo udah ada, berarti wishlist dihapus
            return back()->with('success', 'Wishlist berhasil dihapus');
        } else {
            $inputan = $request->all();
            $inputan['user_id'] = $itemuser->id;
            $itemwishlist = Wishlist::create($inputan);
            return back()->with('success', 'Produk berhasil ditambahkan ke wishlist');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Wishlist  $wishlist
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Wishlist  $wishlist
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Wishlist  $wishlist
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Wishlist  $wishlist
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $itemwishlist = Wishlist::findOrFail($id);
        if ($itemwishlist->delete()) {
            return back()->with('success', 'Wishlist berhasil dihapus');
        } else {
            return back()->with('error', 'Wishlist gagal dihapus');
        }
    }
}
```

**#4. Views wishlist**

Untuk file views silahkan buat 1 buah folder di dalam folder views dengan nama **wishlist**, kemudian dalam folder **wishlist** buah 1 buah file **index.blade.php**

![image-20220421121527186](D:/www/pemrogaman-web-laravel/img/image-20220421121527186.png)

```php+HTML
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Wishlist</h3>
                    </div>
                    <div class="card-body">
                        @if ($message = Session::get('error'))
                            <div class="alert alert-warning">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode Produk</th>
                                        <th>Nama Produk</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($itemwishlist as $wish)
                                        <tr>
                                            <td>
                                                {{ ++$no }}
                                            </td>
                                            <td>
                                                {{ $wish->produk->kode_produk }}
                                            </td>
                                            <td>
                                                {{ $wish->produk->nama_produk }}
                                            </td>
                                            <td>
                                                <form action="{{ route('wishlist.destroy', $wish->id) }}" method="post"
                                                    style="display:inline;">
                                                    @csrf
                                                    {{ method_field('delete') }}
                                                    <button type="submit" class="btn btn-sm btn-danger mb-2">
                                                        Hapus
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $itemwishlist->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
```

Halaman wishlist ini digunakan untuk menampilkan daftar wishlist yang telah dibuat oleh user dan juga menghapusnya.

Setelah membuat file views **wishlist/index.blade.php** kita perlu mengoreksi file views **produkdetail.blade.php** pada bagian button wishtlist agar user bisa menambahkan wishlist cukup mengklik tombol tersebut.

Buka file **homepage/produkdetail.blade.php** dan koreksi pada bagian button "tambah ke wishlist".

```php+HTML
@extends('layouts.template')
@section('content')
    <div class="container">
        <div class="row mt-4">
            <div class="col col-lg-8 col-md-8">
                <div id="carousel" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        @foreach ($itemproduk->images as $index => $image)
                            @if ($index == 0)
                                <div class="carousel-item active">
                                    <img src="{{ \Storage::url($image->foto) }}" class="d-block w-100" alt="...">
                                </div>
                            @else
                                <div class="carousel-item">
                                    <img src="{{ \Storage::url($image->foto) }}" class="d-block w-100" alt="...">
                                </div>
                            @endif
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <!-- deskripsi produk -->
            <div class="col col-lg-4 col-md-4">
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-body">
                                @if (count($errors) > 0)
                                    @foreach ($errors->all() as $error)
                                        <div class="alert alert-warning">{{ $error }}</div>
                                    @endforeach
                                @endif
                                @if ($message = Session::get('error'))
                                    <div class="alert alert-warning">
                                        <p>{{ $message }}</p>
                                    </div>
                                @endif
                                @if ($message = Session::get('success'))
                                    <div class="alert alert-success">
                                        <p>{{ $message }}</p>
                                    </div>
                                @endif
                                <span class="small">{{ $itemproduk->kategori->nama_kategori }}</span>
                                <h5>{{ $itemproduk->nama_produk }}</h5>
                                <!-- cek apakah ada promo -->
                                @if ($itemproduk->promo != null)
                                    <p>
                                        Rp. <del>{{ number_format($itemproduk->promo->harga_awal, 2) }}</del>
                                        <br />
                                        Rp. {{ number_format($itemproduk->promo->harga_akhir, 2) }}
                                    </p>
                                @else
                                    <p>
                                        Rp. {{ number_format($itemproduk->harga, 2) }}
                                    </p>
                                @endif
                                <form action="{{ route('wishlist.store') }}" method="post">
                                    @csrf
                                    <input type="hidden" name="produk_id" value={{ $itemproduk->id }}>
                                    <button type="submit" class="btn btn-sm btn-outline-secondary">
                                        @if (isset($itemwishlist) && $itemwishlist)
                                            <i class="fas fa-heart"></i> Tambah ke wishlist
                                        @else
                                            <i class="far fa-heart"></i> Tambah ke wishlist
                                        @endif
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col">
                        <div class="card">
                            <div class="card-body">
                                <button class="btn btn-block btn-primary">
                                    <i class="fa fa-shopping-cart"></i> Tambahkan Ke Keranjang
                                </button>
                                <button class="btn btn-block btn-danger mt-4">
                                    <i class="fa fa-shopping-basket"></i> Beli Sekarang
                                </button>
                            </div>
                            <div class="card-footer">
                                <div class="row mt-4">
                                    <div class="col text-center">
                                        <i class="fa fa-truck-moving"></i>
                                        <p>Pengiriman Cepat</p>
                                    </div>
                                    <div class="col text-center">
                                        <i class="fa fa-calendar-week"></i>
                                        <p>Garansi 7 hari</p>
                                    </div>
                                    <div class="col text-center">
                                        <i class="fa fa-money-bill"></i>
                                        <p>Pembayaran Aman</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        Deskripsi
                    </div>
                    <div class="card-body">
                        {{ $itemproduk->deskripsi_produk }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
```

**#5. Koreksi HomepageController**

Agar bisa menampilkan apakah produk tersebut sudah dimasukkan ke wishlist atau belum, kita perlu melakukan sedikit koreksi pada file **HomepageController.php** terutama pada bagian `function produkdetail($id)`.

```php
    public function produkdetail($id)
    {
        $itemproduk = Produk::where('slug_produk', $id)
            ->where('status', 'publish')
            ->first();
        if ($itemproduk) {
            if (Auth::user()) { //cek kalo user login
                $itemuser = Auth::user();
                $itemwishlist = Wishlist::where('produk_id', $itemproduk->id)
                    ->where('user_id', $itemuser->id)
                    ->first();
                $data = array(
                    'title' => $itemproduk->nama_produk,
                    'itemproduk' => $itemproduk,
                    'itemwishlist' => $itemwishlist
                );
            } else {
                $data = array(
                    'title' => $itemproduk->nama_produk,
                    'itemproduk' => $itemproduk
                );
            }
            return view('homepage.produkdetail', $data);
        } else {
            // kalo produk ga ada, jadinya tampil halaman tidak ditemukan (error 404)
            return abort('404');
        }
    }
```

Selain `function produkdetail($id)` kita perlu juga memasukkan model **Wishlist** dan juga **Auth** di bawah baris kode "**use App\Models\Slideshow;**" dengan cara menambahkan baris kode berikut.

```php
use App\Models\Wishlist;
use Illuminate\Support\Facades\Auth;
```

Cukup untuk **HomepageController.php** selanjutnya kita akan menambahkan menu dashboard.

**#6. Tambahkan menu item di menudashboard.blade.php**

Buka file **menudashboard.blade.php** dan tambahkan menu item persis di bawah menu item **Data Transaksi.**

```php+HTML
<li class="nav-item">
    <a href="{{ route('wishlist.index') }}" class="nav-link">
        <i class="far fa-circle nav-icon"></i>
        <p>Wishlist</p>
    </a>
</li>
```

**#7. Tambahkan route wishlist ke web.php**

Terakhir kita perlu menambahkan route ke file **web.php** agar menu wishlist dapat diakses hanya oleh user yang telah login. Buka file **web.php** dan tambakan route persis di bawah route "`Route::get('loadprodukasync/{id}', 'ProdukController@loadasync');`"

```php
// wishlist
Route::resource('wishlist', WishlistController::class);
```

Jangan lupa masukkan kelas "`use App\Http\Controllers\WishlistController;`" ke dalam web.php seperti digambar

![image-20220421122726255](D:/www/pemrogaman-web-laravel/img/image-20220421122726255.png)

Selain itu kita perlu juga mengubah route ke **'/home'** kita arahkan ke **'/admin'**, ganti `Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');` menjadi

```php
Route::get('/home', function() {
        return redirect('/admin');
    });
```

Untuk file **web.php** lengkapnya akan menjadi seperti berikut.

```php
    <?php

    use App\Http\Controllers\CustomerController;
    use App\Http\Controllers\DashboardController;
    use App\Http\Controllers\HomepageController;
    use App\Http\Controllers\ImageController;
    use App\Http\Controllers\KategoriController;
    use App\Http\Controllers\LaporanController;
    use App\Http\Controllers\LatihanController;
    use App\Http\Controllers\ProdukController;
    use App\Http\Controllers\ProdukPromoController;
    use App\Http\Controllers\SlideshowController;
    use App\Http\Controllers\TransaksiController;
    use App\Http\Controllers\UserController;
    use App\Http\Controllers\WishlistController;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Route;

    /*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register web routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | contains the "web" middleware group. Now create something great!
    |
    */

    Route::get('/', [HomepageController::class, 'index']);
    Route::get('/about', [HomepageController::class, 'about']);
    Route::get('/kontak', [HomepageController::class, 'kontak']);
    Route::get('/kategori', [HomepageController::class, 'kategori']);
    Route::get('/kategori/{slug}', [HomepageController::class, 'kategoribyslug']);
    Route::get('/produk', [HomepageController::class, 'produk']);
    Route::get('produk/{id}', [HomepageController::class, 'produkdetail']);

    Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
        Route::get('/', [DashboardController::class, 'index']);
        // route kategori
        Route::resource('kategori', KategoriController::class);
        // route produk
        Route::resource('produk', ProdukController::class);
        // route customer
        Route::resource('customer', CustomerController::class);
        // route transaksi
        Route::resource('transaksi', TransaksiController::class);
        // profil
        Route::get('profil', [UserController::class, 'index']);
        // setting profil
        Route::get('setting', [UserController::class, 'setting']);
        // form laporan
        Route::get('laporan', [LaporanController::class, 'index']);
        // proses laporan
        Route::get('proseslaporan', [LaporanController::class, 'proses']);

        // image
        Route::get('image', [ImageController::class, 'index']);
        // simpan image
        Route::post('image', [ImageController::class, 'store']);
        // hapus image by id
        Route::delete('image/{id}', [ImageController::class, 'destroy']);


        // upload image kategori
        Route::post('imagekategori', [KategoriController::class, 'uploadimage']);
        // hapus image kategori
        Route::delete('imagekategori/{id}', [KategoriController::class, 'deleteimage']);

        // upload image produk
        Route::post('produkimage', [ProdukController::class, 'uploadimage']);
        // hapus image produk
        Route::delete('produkimage/{id}', [ProdukController::class, 'deleteimage']);

        // hapus image produk
        Route::delete('produkimage/{id}', [ProdukController::class, 'deleteimage']);
        // slideshow
        Route::resource('slideshow', SlideshowController::class);

        // produk promo
        Route::resource('promo', ProdukPromoController::class);
        // load async produk
        Route::get('loadprodukasync/{id}', [ProdukController::class, 'loadasync']);
        // wishlist
        Route::resource('wishlist', WishlistController::class);
    });

    // Route::get('/', function () {
    //     return view('welcome');
    // });
    // Route::get('/halo', function () {
    //     return "Halo nama saya fadlur";
    // });
    // Route::get('/latihan', [LatihanController::class,'index']);
    // Route::get('/blog/{id}', [LatihanController::class,'blog']);
    // Route::get('/blog/{idblog}/komentar/{idkomentar}',[LatihanController::class,'komentar']);
    // Route::get('/beranda', [LatihanController::class,'beranda']);

    Auth::routes();
    Route::get('/home', function() {
        return redirect('/admin');
    });
```

Sekarang coba jalankan aplikasi laravel kemudian klik salah satu produknya. Pada tombol tambahkan ke wishlist coba tekan, kalau diarahkan ke halaman login coba buat akun dulu di halaman register untuk mencoba menambahkan wishlist.

![image-20220421123520721](D:/www/pemrogaman-web-laravel/img/image-20220421123520721.png)

![image-20220421123540108](img\image-20220421123540108.png)