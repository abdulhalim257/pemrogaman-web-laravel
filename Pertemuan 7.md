# CRUD Kategori lanjt

### Image Kategori

Setelah fungsi upload selesai kita buat, sekarang kita bisa menambahkan fungsi upload image kategori dan hapus image kategori.

**# Tambah function upload dan hapus image**

Pertama-tama buka file **KategoriController.php** dan di dalam `class KategoriController` tambahkan 2 function di bawah function destroy.

```php+HTML
public function uploadimage(Request $request)
{
    $this->validate($request, [
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'kategori_id' => 'required',
    ]);
    $itemuser = $request->user();
    $itemkategori = Kategori::where('user_id', $itemuser->id)
        ->where('id', $request->kategori_id)
        ->first();
    if ($itemkategori) {
        $fileupload = $request->file('image');
        $folder = $request->file('image')->store('public/images');
        $itemgambar = (new ImageController())->upload($fileupload, $itemuser, $folder);
        $inputan['foto'] = $itemgambar->url; //ambil url file yang barusan diupload
        $itemkategori->update($inputan);
        return back()->with('success', 'Image berhasil diupload');
    } else {
        return back()->with('error', 'Kategori tidak ditemukan');
    }
}

public function deleteimage(Request $request, $id)
{
    $itemuser = $request->user();
    $itemkategori = Kategori::where('user_id', $itemuser->id)
        ->where('id', $id)
        ->first();
    if ($itemkategori) {
        // kita cari dulu database berdasarkan url gambar
        $itemgambar = \App\Image::where('url', $itemkategori->foto)->first();
        // hapus imagenya
        if ($itemgambar) {
            \Storage::delete($itemgambar->url);
            $itemgambar->delete();
        }
        // baru update foto kategori
        $itemkategori->update(['foto' => null]);
        return back()->with('success', 'Data berhasil dihapus');
    } else {
        return back()->with('error', 'Data tidak ditemukan');
    }
}
```

**# web route**

Buka file **routes/web.php** di folder routes dan tambahkan 2 buah route **upload** dan **delete image.**

```php+HTML
<?php

use App\Http\Controllers\CustomerController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HomepageController;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\LaporanController;
use App\Http\Controllers\LatihanController;
use App\Http\Controllers\ProdukController;
use App\Http\Controllers\TransaksiController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomepageController::class, 'index']);
Route::get('/about', [HomepageController::class, 'about']);
Route::get('/kontak', [HomepageController::class, 'kontak']);
Route::get('/kategori', [HomepageController::class, 'kategori']);
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::get('/', [DashboardController::class, 'index']);
    // route kategori
    Route::resource('kategori', KategoriController::class);
    // route produk
    Route::resource('produk', ProdukController::class);
    // route customer
    Route::resource('customer', CustomerController::class);
    // route transaksi
    Route::resource('transaksi', TransaksiController::class);
    // profil
    Route::get('profil', [UserController::class, 'index']);
    // setting profil
    Route::get('setting', [UserController::class, 'setting']);
    // form laporan
    Route::get('laporan', [LaporanController::class, 'index']);
    // proses laporan
    Route::get('proseslaporan', [LaporanController::class, 'proses']);

    // image
    Route::get('image', [ImageController::class, 'index']);
    // simpan image
    Route::post('image', [ImageController::class, 'store']);
    // hapus image by id
    Route::delete('image/{id}', [ImageController::class, 'destroy']);


  // upload image kategori
  Route::post('imagekategori', [KategoriController::class,'uploadimage']);
  // hapus image kategori
  Route::delete('imagekategori/{id}', [KategoriController::class,'deleteimage']);
});

// Route::get('/', function () {
//     return view('welcome');
// });
// Route::get('/halo', function () {
//     return "Halo nama saya fadlur";
// });
// Route::get('/latihan', [LatihanController::class,'index']);
// Route::get('/blog/{id}', [LatihanController::class,'blog']);
// Route::get('/blog/{idblog}/komentar/{idkomentar}',[LatihanController::class,'komentar']);
// Route::get('/beranda', [LatihanController::class,'beranda']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
```

**# Update file index.blade.php**

Terakhir buka file **index.blade.php** di dalam folder **kategori**, dan ubah bagian image kategori menjadi seperti berikut.

```php+HTML
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <!-- table kategori -->
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Kategori Produk</h4>
                        <div class="card-tools">
                            <a href="{{ route('kategori.create') }}" class="btn btn-sm btn-primary">
                                Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="#">
                            <div class="row">
                                <div class="col">
                                    <input type="text" name="keyword" id="keyword" class="form-control"
                                        placeholder="ketik keyword disini">
                                </div>
                                <div class="col-auto">
                                    <button class="btn btn-primary">
                                        Cari
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-body">
                        @if ($message = Session::get('error'))
                            <div class="alert alert-warning">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th width="50px">No</th>
                                        <th>Gambar</th>
                                        <th>Kode</th>
                                        <th>Nama</th>
                                        <th>Jumlah Produk</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($itemkategori as $kategori)
                                        <tr>
                                            <td>
                                                {{ ++$no }}
                                            </td>
                                            <td>
                                                <!-- image kategori -->
                                                @if ($kategori->foto != null)
                                                    <img src="{{ \Storage::url($kategori->foto) }}"
                                                        alt="{{ $kategori->nama_kategori }}" width='150px'
                                                        class="img-thumbnail mb-2">
                                                    <br>
                                                    <form action="{{ url('/admin/imagekategori/' . $kategori->id) }}"
                                                        method="post" style="display:inline;">
                                                        @csrf
                                                        {{ method_field('delete') }}
                                                        <button type="submit" class="btn btn-sm btn-danger mb-2">
                                                            Hapus
                                                        </button>
                                                    </form>
                                                @else
                                                    <form action="{{ url('/admin/imagekategori') }}" method="post"
                                                        enctype="multipart/form-data" class="form-inline">
                                                        @csrf
                                                        <div class="form-group">
                                                            <input type="file" name="image" id="image">
                                                            <input type="hidden" name="kategori_id"
                                                                value={{ $kategori->id }}>
                                                        </div>
                                                        <div class="form-group">
                                                            <button class="btn btn-primary">Upload</button>
                                                        </div>
                                                    </form>
                                                @endif
                                                <!-- end image kategori -->
                                            </td>
                                            <td>
                                                {{ $kategori->kode_kategori }}
                                            </td>
                                            <td>
                                                {{ $kategori->nama_kategori }}
                                            </td>
                                            <td>
                                                {{ $kategori->produk ? $kategori->produk()->count() : '0' }} Produk
                                            </td>
                                            <td>
                                                {{ $kategori->status }}
                                            </td>
                                            <td>
                                                <a href="{{ route('kategori.edit', $kategori->id) }}"
                                                    class="btn btn-sm btn-primary mr-2 mb-2">
                                                    Edit
                                                </a>
                                                <form action="{{ route('kategori.destroy', $kategori->id) }}"
                                                    method="post" style="display:inline;">
                                                    @csrf
                                                    {{ method_field('delete') }}
                                                    <button type="submit" class="btn btn-sm btn-danger mb-2">
                                                        Hapus
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <!-- untuk menampilkan link page, tambahkan skrip di bawah ini -->
                            {{ $itemkategori->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
```

Hasil jadinya akan seperti ini ketika salah satu kategori di upload gambarnya.

![image-20220419091101234](img\image-20220419091101234.png)

Sekarang coba buat kategori baru dan upload gambar kategorinya.

# CRUD Produk

Sama seperti pada Dashboard Kategori, kita akan membuat fungsi CRUD untuk produk.

**# Index Produk**

Untuk halaman index produk, kita perlu mengedit **ProdukController.php** dan menambahkan 2 model yaitu **Produk** dan **Kategori** di atas tulisan `class ProdukController`.

```php
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Produk;
use App\Models\Kategori;
```

Selanjutnya edit function index di **ProdukController.php** menjadi seperti berikut.

```php
public function index(Request $request)
{
    //
    $itemproduk = Produk::orderBy('created_at', 'desc')->paginate(20);
    $data = array(
        'title' => 'Produk',
        'itemproduk' => $itemproduk
    );
    return view('produk.index', $data)->with('no', ($request->input('page', 1) - 1) * 20);
}
```

Terakhir untuk halaman index, edit file views (**index.blade.php**) di folder produk. 

```php
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <!-- table produk -->
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Produk</h4>
                        <div class="card-tools">
                            <a href="{{ route('produk.create') }}" class="btn btn-sm btn-primary">
                                Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="#">
                            <div class="row">
                                <div class="col">
                                    <input type="text" name="keyword" id="keyword" class="form-control"
                                        placeholder="ketik keyword disini">
                                </div>
                                <div class="col-auto">
                                    <button class="btn btn-primary">
                                        Cari
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-body">
                        @if ($message = Session::get('error'))
                            <div class="alert alert-warning">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th width="50px">No</th>
                                        <th>Gambar</th>
                                        <th>Kode</th>
                                        <th>Nama</th>
                                        <th>Jumlah</th>
                                        <th>Harga</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($itemproduk as $produk)
                                        <tr>
                                            <td>
                                                {{ ++$no }}
                                            </td>
                                            <td>
                                                <img src="{{ asset('images/slide1.jpg') }}" alt="produk 1" width='150px'>
                                                <div class="row mt-2">
                                                    <div class="col">
                                                        <input type="file" name="gambar" id="gambar">
                                                    </div>
                                                    <div class="col-auto">
                                                        <button class="btn btn-sm btn-primary">Upload</button>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                {{ $produk->kode_produk }}
                                            </td>
                                            <td>
                                                {{ $produk->nama_produk }}
                                            </td>
                                            <td>
                                                {{ $produk->qty }} {{ $produk->satuan }}
                                            </td>
                                            <td>
                                                {{ number_format($produk->harga, 2) }}
                                            </td>
                                            <td>
                                                {{ $produk->status }}
                                            </td>
                                            <td>
                                                <a href="{{ route('produk.show', $produk->id) }}"
                                                    class="btn btn-sm btn-primary mr-2 mb-2">
                                                    Detail
                                                </a>
                                                <a href="{{ route('produk.edit', $produk->id) }}"
                                                    class="btn btn-sm btn-primary mr-2 mb-2">
                                                    Edit
                                                </a>
                                                <form action="{{ route('produk.destroy', $produk->id) }}" method="post"
                                                    style="display:inline;">
                                                    @csrf
                                                    {{ method_field('delete') }}
                                                    <button type="submit" class="btn btn-sm btn-danger mb-2">
                                                        Hapus
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $itemproduk->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

```

![image-20220419091756048](img\image-20220419091756048.png)

Karena sudah kita batasi hanya yang login, maka login dulu kemudian klik halaman dashboard produk. Tampilannya akan menjadi seperti berikut.

**# Create Produk**

Untuk membuat data produk baru, kita membutuhkan form create produk dan function untuk menyimpannya. Karena di form produk dibutuhkan data pilihan kategori, maka kita harus memanggil data kategori yang sudah diinput di `function create()`.

Buka **ProdukController.php** dan edit `function create` menjadi seperti berikut.

```php+HTML
public function create()
{
    //
    $itemkategori = Kategori::orderBy('nama_kategori', 'asc')->get();
    $data = array(
        'title' => 'Form Produk Baru',
        'itemkategori' => $itemkategori
    );
    return view('produk.create', $data);
}
```

Masih di folder produk, edit file **create.blade.php** menjadi seperti berikut.

```php+HTML
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Form Produk</h3>
                        <div class="card-tools">
                            <a href="{{ route('produk.index') }}" class="btn btn-sm btn-danger">
                                Tutup
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        @if (count($errors) > 0)
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-warning">{{ $error }}</div>
                            @endforeach
                        @endif
                        @if ($message = Session::get('error'))
                            <div class="alert alert-warning">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        <form action="{{ route('produk.store') }}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="kategori_id">Kategori Produk</label>
                                <select name="kategori_id" id="kategori_id" class="form-control">
                                    <option value="">Pilih Kategori</option>
                                    @foreach ($itemkategori as $kategori)
                                        <option value="{{ $kategori->id }}">{{ $kategori->nama_kategori }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="kode_produk">Kode Produk</label>
                                <input type="text" name="kode_produk" id="kode_produk" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="nama_produk">Nama Produk</label>
                                <input type="text" name="nama_produk" id="nama_produk" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="slug_produk">Slug Produk</label>
                                <input type="text" name="slug_produk" id="slug_produk" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="deskripsi_produk">Deskripsi</label>
                                <textarea name="deskripsi_produk" id="deskripsi_produk" cols="30" rows="5" class="form-control"></textarea>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="qty">Qty</label>
                                        <input type="text" name="qty" id="qty" class="form-control">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="satuan">Satuan</label>
                                        <input type="text" name="satuan" id="satuan" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="harga">Harga</label>
                                <input type="text" name="harga" id="harga" class="form-control">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <button type="reset" class="btn btn-warning">Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

```

Terakhir untuk proses simpan, masih di file **ProdukController.php** edit `function store` menjadi seperti berikut.

```php+HTML
public function store(Request $request)
{
    $this->validate($request, [
        'kode_produk' => 'required|unique:produk',
        'nama_produk' => 'required',
        'slug_produk' => 'required',
        'deskripsi_produk' => 'required',
        'kategori_id' => 'required',
        'qty' => 'required|numeric',
        'satuan' => 'required',
        'harga' => 'required|numeric'
    ]);
    $itemuser = $request->user(); //ambil data user yang login
    $slug = \Str::slug($request->slug_produk); //buat slug dari input slug produk
    $inputan = $request->all();
    $inputan['slug_produk'] = $slug;
    $inputan['user_id'] = $itemuser->id;
    $inputan['status'] = 'publish';
    $itemproduk = Produk::create($inputan);
    return redirect()->route('produk.index')->with('success', 'Data berhasil disimpan');
}
```

Sekarang klik button dengan tulisan 'baru' di kanan atas, maka akan tampil form create produk. Coba isi kolom-kolom yang ada, kemudian klik simpan.

![image-20220419092608405](img\image-20220419092608405.png)

![image-20220419092432281](img\image-20220419092432281.png)

**# Edit Produk**

Untuk mengubah data yang udah dimasukkan, kita butuh fungsi edit. Masih di file **ProdukController.php** edit `function edit` menjadi seperti berikut.

```php
public function edit($id)
{
    //
    $itemproduk = Produk::findOrFail($id);
    $itemkategori = Kategori::orderBy('nama_kategori', 'asc')->get();
    $data = array(
        'title' => 'Form Edit Produk',
        'itemproduk' => $itemproduk,
        'itemkategori' => $itemkategori
    );
    return view('produk.edit', $data);
}
```

dan file views (**form edit.blade.php**) di folder produk menjadi seperti berikut.

```php+HTML
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Form Produk</h3>
                        <div class="card-tools">
                            <a href="{{ route('produk.index') }}" class="btn btn-sm btn-danger">
                                Tutup
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        @if (count($errors) > 0)
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-warning">{{ $error }}</div>
                            @endforeach
                        @endif
                        @if ($message = Session::get('error'))
                            <div class="alert alert-warning">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        <form action="{{ route('produk.update', $itemproduk->id) }}" method="post">
                            {{ method_field('patch') }}
                            @csrf
                            <div class="form-group">
                                <label for="kategori_id">Kategori Produk</label>
                                <select name="kategori_id" id="kategori_id" class="form-control">
                                    <option value="">Pilih Kategori</option>
                                    @foreach ($itemkategori as $kategori)
                                        <option value="{{ $kategori->id }}"
                                            {{ $itemproduk->kategori_id == $kategori->id ? 'selected' : '' }}>
                                            {{ $kategori->nama_kategori }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="kode_produk">Kode Produk</label>
                                <input type="text" name="kode_produk" id="kode_produk"
                                    value={{ $itemproduk->kode_produk }} class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="nama_produk">Nama Produk</label>
                                <input type="text" name="nama_produk" id="nama_produk"
                                    value={{ $itemproduk->nama_produk }} class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="slug_produk">Slug Produk</label>
                                <input type="text" name="slug_produk" id="slug_produk"
                                    value={{ $itemproduk->slug_produk }} class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="deskripsi_produk">Deskripsi</label>
                                <textarea name="deskripsi_produk" id="deskripsi_produk" cols="30" rows="5"
                                    class="form-control">{{ $itemproduk->deskripsi_produk }}</textarea>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="qty">Qty</label>
                                        <input type="text" name="qty" id="qty" value={{ $itemproduk->qty }}
                                            class="form-control">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="satuan">Satuan</label>
                                        <input type="text" name="satuan" id="satuan" value={{ $itemproduk->satuan }}
                                            class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="harga">Harga</label>
                                <input type="text" name="harga" id="harga" value={{ $itemproduk->harga }}
                                    class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="status">Status</label>
                                <select name="status" id="status" class="form-control">
                                    <option value="publish" {{ $itemproduk->status == 'publish' ? 'selected' : '' }}>Publish
                                    </option>
                                    <option value="unpublish" {{ $itemproduk->status == 'unpublish' ? 'selected' : '' }}>
                                        Unpublish</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Update</button>
                                <button type="reset" class="btn btn-warning">Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
```

Untuk proses updatenya, kita perlu mengedit function update di file **ProdukController.php** menjadi

```php+HTML
public function update(Request $request, $id)
{
    $this->validate($request, [
        'kode_produk' => 'required|unique:produk,id,' . $id,
        'nama_produk' => 'required',
        'slug_produk' => 'required',
        'deskripsi_produk' => 'required',
        'kategori_id' => 'required',
        'qty' => 'required|numeric',
        'satuan' => 'required',
        'harga' => 'required|numeric'
    ]);
    $itemproduk = Produk::findOrFail($id);
    // kalo ga ada error page not found 404
    $slug = \Str::slug($request->slug_produk); //slug kita gunakan nanti pas buka produk
    // kita validasi dulu, biar tidak ada slug yang sama
    $validasislug = Produk::where('id', '!=', $id) //yang id-nya tidak sama dengan $id
        ->where('slug_produk', $slug)
        ->first();
    if ($validasislug) {
        return back()->with('error', 'Slug sudah ada, coba yang lain');
    } else {
        $inputan = $request->all();
        $inputan['slug'] = $slug;
        $itemproduk->update($inputan);
        return redirect()->route('produk.index')->with('success', 'Data berhasil diupdate');
    }
}
```

Sekarang coba klik edit data produk di halaman dashboard produk, kemudian edit beberapa kolom yang ada terus klik tombol update.

![image-20220419093231700](img\image-20220419093231700.png)

**# Hapus produk**

Untuk menghapus produk, kita cukup mengedit `function destroy` di file **ProdukController.php** menjadi seperti berikut.

```php+HTML
public function destroy($id)
{
    $itemproduk = Produk::findOrFail($id); //cari berdasarkan id = $id,
    // kalo ga ada error page not found 404
    if ($itemproduk->delete()) {
        return back()->with('success', 'Data berhasil dihapus');
    } else {
        return back()->with('error', 'Data gagal dihapus');
    }
}
```

Sekarang coba klik tombol hapus di halaman dashboard produk.

Sampai disini untuk CRUD Produk, setelah ini akan kita tambahkan untuk proses input foto produk.

![image-20220419093311698](img\image-20220419093311698.png)

# Dashboard Produk Image

Proses input sampai hapus produk sudah kita buat, sekarang kita akan membuat daftar gambar atau image untuk produk. Untuk daftar image kita membutuhkan 1 buah table lagi, kita kasih nama **produk_images**.

**# Buat model ProdukImage dan migrations**

Buka terminal dan jalan perintah berikut.

```php
php artisan make:model ProdukImage -m
```

kemudian pencet enter.

Edit file migration dengan akhiran **produk_images_table.php**

```php
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdukImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produk_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produk_id')->unsigned();
            $table->string('foto')->nullable();
            $table->foreign('produk_id')->references('id')->on('produk');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produk_images');
    }
}
```

Kemudian buka file model **ProdukImage.php**

```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProdukImage extends Model
{
    protected $fillable = [
        'produk_id',
        'foto',
    ];

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_id');
    }
}

```

Setelah selesai, jalankan perintah migrate untuk memasukkan table ke database

```php
php artisan migrate
```

Kemudian pencet enter.

![image-20220419094330214](img\image-20220419094330214.png)

**# Edit Model Produk**

Setelah produk image selesai kita update, selanjutnya update model **Produk.php** untuk menambahkan relasi ke table **produk_images**. Buka file **Produk.php** dan edit.

```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    use HasFactory;

    protected $table = 'produk';
    protected $fillable = [
        'kategori_id',
        'user_id',
        'kode_produk',
        'nama_produk',
        'slug_produk',
        'deskripsi_produk',
        'foto',
        'qty',
        'satuan',
        'harga',
        'status',
    ];

    public function kategori()
    {
        return $this->belongsTo(Kategori::class, 'kategori_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function images()
    {
        return $this->hasMany(ProdukImage::class, 'produk_id');
    }
}

```

**# Edit function show**

Buka controller **ProdukController.php** dan edit pada bagian `function show` menjadi seperti berikut.

```php
public function show($id)
{
    $itemproduk = Produk::findOrFail($id);
    $data = array(
        'title' => 'Foto Produk',
        'itemproduk' => $itemproduk
    );
    return view('produk.show', $data);
}
```

**# views show**

Setelah `function show` dilengkapi, selanjutnya buat 1 buah file di dalam folder **views/produk** dengan nama **show.blade.php**

![image-20220419094924490](img\image-20220419094924490.png)

```php+HTML
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-lg-4 col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Detail Produk</h3>
                        <div class="card-tools">
                            <a href="{{ route('produk.index') }}" class="btn btn-sm btn-danger">
                                Tutup
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <td>Kode Produk</td>
                                    <td>
                                        {{ $itemproduk->kode_produk }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Nama Produk</td>
                                    <td>
                                        {{ $itemproduk->nama_produk }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Qty</td>
                                    <td>
                                        {{ $itemproduk->qty }} {{ $itemproduk->satuan }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Harga</td>
                                    <td>
                                        Rp. {{ number_format($itemproduk->harga, 2) }}
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-lg-8 col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Foto Produk</h3>
                    </div>
                    <div class="card-body">
                        <form action="{{ url('/admin/produkimage') }}" method="post" enctype="multipart/form-data"
                            class="form-inline">
                            @csrf
                            <div class="form-group">
                                <input type="file" name="image" id="image">
                                <input type="hidden" name="produk_id" value={{ $itemproduk->id }}>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary">Upload</button>
                            </div>
                        </form>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col mb-2">
                                @if (count($errors) > 0)
                                    @foreach ($errors->all() as $error)
                                        <div class="alert alert-warning">{{ $error }}</div>
                                    @endforeach
                                @endif
                                @if ($message = Session::get('error'))
                                    <div class="alert alert-warning">
                                        <p>{{ $message }}</p>
                                    </div>
                                @endif
                                @if ($message = Session::get('success'))
                                    <div class="alert alert-success">
                                        <p>{{ $message }}</p>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            @foreach ($itemproduk->images as $image)
                                <div class="col-md-3 mb-2">
                                    <img src="{{ \Storage::url($image->foto) }}" alt="image" class="img-thumbnail mb-2">
                                    <form action="{{ url('/admin/produkimage/' . $image->id) }}" method="post"
                                        style="display:inline;">
                                        @csrf
                                        {{ method_field('delete') }}
                                        <button type="submit" class="btn btn-sm btn-danger mb-2">
                                            Hapus
                                        </button>
                                    </form>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
```

**# Tambahkan function upload dan delete image**

Buka file **ProdukController.php** dan tambahkan 2 buah function di bawah `function destroy`.

```php
public function uploadimage(Request $request) {
    $this->validate($request, [
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'produk_id' => 'required',
    ]);
    $itemuser = $request->user();
    $itemproduk = Produk::where('user_id', $itemuser->id)
                            ->where('id', $request->produk_id)
                            ->first();
    if ($itemproduk) {
        $fileupload = $request->file('image');
        $folder = $request->file('image')->store('public/images');
        $itemgambar = (new ImageController)->upload($fileupload, $itemuser, $folder);
        // simpan ke table produk_images
        $inputan = $request->all();
        $inputan['foto'] = $itemgambar->url;//ambil url file yang barusan diupload
        // simpan ke produk image
        ProdukImage::create($inputan);
        // update banner produk
        $itemproduk->update(['foto' => $itemgambar->url]);
        // end update banner produk
        return back()->with('success', 'Image berhasil diupload');
    } else {
        return back()->with('error', 'Kategori tidak ditemukan');
    }
}

public function deleteimage(Request $request, $id) {
    // ambil data produk image by id
    $itemprodukimage = ProdukImage::findOrFail($id);
    // ambil produk by produk_id kalau tidak ada maka error 404
    $itemproduk = Produk::findOrFail($itemprodukimage->produk_id);
    // kita cari dulu database berdasarkan url gambar
    $itemgambar = Image::where('url', $itemprodukimage->foto)->first();
    // hapus imagenya
    if ($itemgambar) {
        \Storage::delete($itemgambar->url);
        $itemgambar->delete();
    }
    // baru update hapus produk image
    $itemprodukimage->delete();
    //ambil 1 buah produk image buat diupdate jadi banner produk
    $itemsisaprodukimage = ProdukImage::where('produk_id', $itemproduk->id)->first();
    if ($itemsisaprodukimage) {
        $itemproduk->update(['foto' => $itemsisaprodukimage->foto]);
    } else {
        $itemproduk->update(['foto' => null]);
    }
    //end update jadi banner produk
    return back()->with('success', 'Data berhasil dihapus');
}
```

**# web route**

Agar bisa jalan produk upload dan delete produk image, update file **web.php** di dalam folder routes

```php+HTML
<?php

use App\Http\Controllers\CustomerController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HomepageController;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\LaporanController;
use App\Http\Controllers\LatihanController;
use App\Http\Controllers\ProdukController;
use App\Http\Controllers\TransaksiController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomepageController::class, 'index']);
Route::get('/about', [HomepageController::class, 'about']);
Route::get('/kontak', [HomepageController::class, 'kontak']);
Route::get('/kategori', [HomepageController::class, 'kategori']);
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::get('/', [DashboardController::class, 'index']);
    // route kategori
    Route::resource('kategori', KategoriController::class);
    // route produk
    Route::resource('produk', ProdukController::class);
    // route customer
    Route::resource('customer', CustomerController::class);
    // route transaksi
    Route::resource('transaksi', TransaksiController::class);
    // profil
    Route::get('profil', [UserController::class, 'index']);
    // setting profil
    Route::get('setting', [UserController::class, 'setting']);
    // form laporan
    Route::get('laporan', [LaporanController::class, 'index']);
    // proses laporan
    Route::get('proseslaporan', [LaporanController::class, 'proses']);

    // image
    Route::get('image', [ImageController::class, 'index']);
    // simpan image
    Route::post('image', [ImageController::class, 'store']);
    // hapus image by id
    Route::delete('image/{id}', [ImageController::class, 'destroy']);


    // upload image kategori
    Route::post('imagekategori', [KategoriController::class, 'uploadimage']);
    // hapus image kategori
    Route::delete('imagekategori/{id}', [KategoriController::class, 'deleteimage']);


    // upload image produk
    Route::post('produkimage', [ProdukController::class, 'uploadimage']);
    // hapus image produk
    Route::delete('produkimage/{id}', [ProdukController::class, 'deleteimage']);
});

// Route::get('/', function () {
//     return view('welcome');
// });
// Route::get('/halo', function () {
//     return "Halo nama saya fadlur";
// });
// Route::get('/latihan', [LatihanController::class,'index']);
// Route::get('/blog/{id}', [LatihanController::class,'blog']);
// Route::get('/blog/{idblog}/komentar/{idkomentar}',[LatihanController::class,'komentar']);
// Route::get('/beranda', [LatihanController::class,'beranda']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
```

**# Update view index produk**

Karena pada proses upload dan delete image produk juga mengupdate foto atau banner produk, maka kita perlu mengupdate view index produk. Buka file **index.blade.php** di dalam folder **views/produk**

```php+HTML
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <!-- table produk -->
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Produk</h4>
                        <div class="card-tools">
                            <a href="{{ route('produk.create') }}" class="btn btn-sm btn-primary">
                                Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="#">
                            <div class="row">
                                <div class="col">
                                    <input type="text" name="keyword" id="keyword" class="form-control"
                                        placeholder="ketik keyword disini">
                                </div>
                                <div class="col-auto">
                                    <button class="btn btn-primary">
                                        Cari
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-body">
                        @if ($message = Session::get('error'))
                            <div class="alert alert-warning">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th width="50px">No</th>
                                        <th>Gambar</th>
                                        <th>Kode</th>
                                        <th>Nama</th>
                                        <th>Jumlah</th>
                                        <th>Harga</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($itemproduk as $produk)
                                        <tr>
                                            <td>
                                                {{ ++$no }}
                                            </td>
                                            <td>
                                                @if ($produk->foto != null)
                                                    <img src="{{ \Storage::url($produk->foto) }}"
                                                        alt="{{ $produk->nama_produk }}" width='150px'
                                                        class="img-thumbnail">
                                                @endif
                                            </td>
                                            <td>
                                                {{ $produk->kode_produk }}
                                            </td>
                                            <td>
                                                {{ $produk->nama_produk }}
                                            </td>
                                            <td>
                                                {{ $produk->qty }} {{ $produk->satuan }}
                                            </td>
                                            <td>
                                                {{ number_format($produk->harga, 2) }}
                                            </td>
                                            <td>
                                                {{ $produk->status }}
                                            </td>
                                            <td>
                                                <a href="{{ route('produk.show', $produk->id) }}"
                                                    class="btn btn-sm btn-primary mr-2 mb-2">
                                                    Detail
                                                </a>
                                                <a href="{{ route('produk.edit', $produk->id) }}"
                                                    class="btn btn-sm btn-primary mr-2 mb-2">
                                                    Edit
                                                </a>
                                                <form action="{{ route('produk.destroy', $produk->id) }}" method="post"
                                                    style="display:inline;">
                                                    @csrf
                                                    {{ method_field('delete') }}
                                                    <button type="submit" class="btn btn-sm btn-danger mb-2">
                                                        Hapus
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $itemproduk->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
```

Pada bagian gambar produk update seperti skrip di atas.

**# Trial**

Buka halaman dashboard produk, kemudian klik tombol detail di baris produk yang diinput. tampilannya akan seperti berikut.

![image-20220419095703841](img\image-20220419095703841.png)

Sekarang coba upload beberapa gambar dan hapus sebagian.