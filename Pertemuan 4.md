# Membuat Toko Online .lanj

### Desain Dashboard

Sebelumnya kita telah membuat template dashboard dan halaman dashboardnya yang hanya menampilkan informasi "ini dashboard" seperti gambar di bawah ini.

![image-20220324214700592](img\image-20220324214700592.png)

Sekarang kita akan mengedit file **views/dashboard/index.blade.php** (halaman dashboard) sehingga akan menampilkan informasi singkat tentang order terbaru, jumlah produk, jumlah transaksi, jumlah member dan juga tabel yang berisi produk terbaru.

Buka file **views/dashboard/index.blade.php** kemudian edit seperti kode di bawah ini.

```php+HTML
@extends('layouts.dashboard')
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-6 col-lg-3">
      <div class="small-box bg-primary">
        <div class="inner">
          <h3>150</h3>

          <p>Order Baru</p>
        </div>
        <div class="icon">
          <i class="ion ion-bag"></i>
        </div>
        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <div class="col-6 col-lg-3">
      <div class="small-box bg-info">
        <div class="inner">
          <h3>150</h3>

          <p>Produk</p>
        </div>
        <div class="icon">
          <i class="ion ion-pie-graph"></i>
        </div>
        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <div class="col-6 col-lg-3">
      <div class="small-box bg-warning">
        <div class="inner">
          <h3>150</h3>

          <p>Member</p>
        </div>
        <div class="icon">
          <i class="ion ion-person-add"></i>
        </div>
        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>

    <div class="col-6 col-lg-3">
      <div class="small-box bg-success">
        <div class="inner">
          <h3>150</h3>

          <p>Transaksi</p>
        </div>
        <div class="icon">
          <i class="ion ion-stats-bars"></i>
        </div>
        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>
  </div>
  <!-- table produk baru -->
  <div class="row">
    <div class="col">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Produk Baru</h4>
          <div class="card-tools">
            <a href="#" class="btn btn-sm btn-primary">
              More
            </a>
          </div>
        </div>
        <div class="card-body">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th>No</th>
                <th>Kode</th>
                <th>Nama</th>
                <th>Kategori</th>
                <th>Qty</th>
                <th>Harga</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>PRO-1</td>
                <td>Baju Atasan</td>
                <td>Baju Anak</td>
                <td>12 kodi</td>
                <td>5.000</td>
              </tr>
              <tr>
                <td>2</td>
                <td>PRO-2</td>
                <td>Gamis</td>
                <td>Baju Wanita</td>
                <td>20 kodi</td>
                <td>25.000</td>
              </tr>
              <tr>
                <td>3</td>
                <td>PRO-3</td>
                <td>Daster</td>
                <td>Baju Wanita</td>
                <td>20 kodi</td>
                <td>125.000</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
```

Setelah selesai, refresh halaman dashboard yang kita buka pada tutorial sebelumnya. Kalau tidak ada error, tampilannya akan tampil seperti ini.

![image-20220331104049091](img\image-20220331104049091.png)

### Desain Dashboard Kategori

Template dashboard selesai kita buat, selanjutnya kita buat halaman dashboard kategori produk terlebih dahulu.

**# Buat Kategori Controller**

Buka terminal dan jalankan perintah

```markup
php artisan make:controller KategoriController -r
```

kemudian tekan enter. Setelah berhasil dibuat, edit file **KategoriController.php** dengan kode berikut.

```php+HTML
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = array('title' => 'Kategori Produk');
        return view('kategori.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data = array('title' => 'Form Kategori');
        return view('kategori.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = array('title' => 'Form Edit Kategori');
        return view('kategori.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

```

**# Tambahkan Route**

Pada file **routes/web.php** tambahkan sebagai berikut.

```php+HTML
Route::group(['prefix' => 'admin'], function () {
    Route::get('/', [DashboardController::class, 'index']);
    Route::resource('kategori', KategoriController::class);
});
```

Kemudian pada terminal, jalankan perintah php artisan serve dan akses halaman http://toko-klowor.local/admin/kategori untuk saat ini masih ada error karena belum ada file view-nya. Sekarang buat file view dengan mengikuti langkah di bawah ini.

**# Halaman index kategori**

Pada folder views buatlah 1 buah folder baru dengan nama **kategori**, kemudian buat 1 buah file dengan nama **index.blade.php** dan isi dengan kode berikut.

![image-20220331104855025](img\image-20220331104855025.png)

```php+HTML
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <!-- table kategori -->
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Kategori Produk</h4>
                        <div class="card-tools">
                            <a href="{{ route('kategori.create') }}" class="btn btn-sm btn-primary">
                                Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="#">
                            <div class="row">
                                <div class="col">
                                    <input type="text" name="keyword" id="keyword" class="form-control"
                                        placeholder="ketik keyword disini">
                                </div>
                                <div class="col-auto">
                                    <button class="btn btn-primary">
                                        Cari
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th width="50px">No</th>
                                        <th>Gambar</th>
                                        <th>Kode</th>
                                        <th>Nama</th>
                                        <th>Jumlah Produk</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>
                                            <img src="{{ asset('images/slide1.jpg') }}" alt="kategori 1" width='150px'>
                                            <div class="row mt-2">
                                                <div class="col">
                                                    <input type="file" name="gambar" id="gambar">
                                                </div>
                                                <div class="col-auto">
                                                    <button class="btn btn-sm btn-primary">Upload</button>
                                                </div>
                                            </div>
                                        </td>
                                        <td>KATE-1</td>
                                        <td>Baju Anak</td>
                                        <td>12 Produk</td>
                                        <td>
                                            <a href="{{ route('kategori.edit', 2) }}"
                                                class="btn btn-sm btn-primary mr-2 mb-2">
                                                Edit
                                            </a>
                                            <button class="btn btn-sm btn-danger mb-2">
                                                Hapus
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>
                                            <img src="{{ asset('images/slide1.jpg') }}" alt="kategori 1" width='150px'>
                                            <div class="row mt-2">
                                                <div class="col">
                                                    <input type="file" name="gambar" id="gambar">
                                                </div>
                                                <div class="col-auto">
                                                    <button class="btn btn-sm btn-primary">Upload</button>
                                                </div>
                                            </div>
                                        </td>
                                        <td>KATE-2</td>
                                        <td>Baju Wanita</td>
                                        <td>20 Produk</td>
                                        <td>
                                            <a href="{{ route('kategori.edit', 2) }}"
                                                class="btn btn-sm btn-primary mr-2 mb-2">
                                                Edit
                                            </a>
                                            <button class="btn btn-sm btn-danger mb-2">
                                                Hapus
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>
                                            <img src="{{ asset('images/slide1.jpg') }}" alt="kategori 1" width='150px'>
                                            <div class="row mt-2">
                                                <div class="col">
                                                    <input type="file" name="gambar" id="gambar">
                                                </div>
                                                <div class="col-auto">
                                                    <button class="btn btn-sm btn-primary">Upload</button>
                                                </div>
                                            </div>
                                        </td>
                                        <td>KATE-3</td>
                                        <td>Baju Wanita</td>
                                        <td>20 Produk</td>
                                        <td>
                                            <a href="{{ route('kategori.edit', 2) }}"
                                                class="btn btn-sm btn-primary mr-2 mb-2">
                                                Edit
                                            </a>
                                            <button class="btn btn-sm btn-danger mb-2">
                                                Hapus
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

```

Akses kembali di halaman http://toko-klowor.local/admin/kategori

![image-20220331105455165](img\image-20220331105455165.png)

**# Halaman Form Kategori Baru**

Masih di dalam folder yang sama yaitu **kategori**, buatlah 1 buah file lagi dengan nama **create.blade.php** kemudian isi dengan kode berikut.

```php+HTML
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Form Kategori</h3>
                        <div class="card-tools">
                            <a href="{{ route('kategori.index') }}" class="btn btn-sm btn-danger">
                                Tutup
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="#">
                            <div class="form-group">
                                <label for="nama_kategori">Nama Kategori</label>
                                <input type="text" name="nama_kategori" id="nama_kategori" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="slug_kategori">Slug Kategori</label>
                                <input type="text" name="slug_kategori" id="slug_kategori" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="deskripsi">Deskripsi</label>
                                <textarea name="deskripsi" id="deskripsi" cols="30" rows="5" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <button type="reset" class="btn btn-warning">Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

```

Untuk form kategori, akses di halaman http://toko-klowor.local/kategori/create atau pada halaman index tadi klik tombol "baru" di samping kanan atas

![image-20220331105755566](img\image-20220331105755566.png)

**# Form Edit Kategori**

Terakhir buatlah 1 buah file lagi di folder kategori dengan nama **edit.blade.php** dan isi dengan kode berikut.

```php+HTML
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Form Edit Kategori</h3>
                        <div class="card-tools">
                            <a href="{{ route('kategori.index') }}" class="btn btn-sm btn-danger">
                                Tutup
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="#">
                            <div class="form-group">
                                <label for="nama_kategori">Nama Kategori</label>
                                <input type="text" name="nama_kategori" id="nama_kategori" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="slug_kategori">Slug Kategori</label>
                                <input type="text" name="slug_kategori" id="slug_kategori" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="deskripsi">Deskripsi</label>
                                <textarea name="deskripsi" id="deskripsi" cols="30" rows="5" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Update</button>
                                <button type="reset" class="btn btn-warning">Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

```

Untuk mengetesnya akses dengan alamat [http://toko-klowor.local/admin/kategori/2/edit](http://toko-klowor.local/)

Kalau tidak ada error, maka akan tampil seperti berikut.

![image-20220331105923242](img\image-20220331105923242.png)

### Desain Dashboard Produk

Kategori produk sudah kita buat, selanjutnya kita membuat halaman dashboard produk.

**# Buat Produk Controller**

Buka terminal dan jalankan perintah

```markup
php artisan make:controller ProdukController -r
```

Kemudian buka file ProdukController.php dan isi dengan kode berikut.

```php
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = array('title' => 'Produk');
        return view('produk.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data = array('title' => 'Form Produk Baru');
        return view('produk.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = array('title' => 'Foto Produk');
        return view('produk.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = array('title' => 'Form Edit Produk');
        return view('produk.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

```

**# Tambahkan Router Produk**

Setelah **ProdukController.php** kita edit, selanjutnya buka file **routes/web.php** dan tambahkan route produk sehingga menjadi seperti berikut.

```php
Route::group(['prefix' => 'admin'], function () {
    Route::get('/', [DashboardController::class, 'index']);
    // route kategori
    Route::resource('kategori', KategoriController::class);
    // route produk
    Route::resource('produk', ProdukController::class);
});
```

Kemudian akses di http://toko-klowor.local/admin/produk

Akan tampil pesan error seperti pada kategori produk tadi. Sekarang kita tambahkan file view untuk dashboard produknya.

**# Halaman index produk**

Buat 1 folder lagi di dalam folder views dengan nama **produk**, kemudian buat 1 buah file dengan nama **index.blade.php** dan isi dengan kode berikut.

```s
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <!-- table produk -->
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Produk</h4>
                        <div class="card-tools">
                            <a href="{{ route('produk.create') }}" class="btn btn-sm btn-primary">
                                Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="#">
                            <div class="row">
                                <div class="col">
                                    <input type="text" name="keyword" id="keyword" class="form-control"
                                        placeholder="ketik keyword disini">
                                </div>
                                <div class="col-auto">
                                    <button class="btn btn-primary">
                                        Cari
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th width="50px">No</th>
                                        <th>Gambar</th>
                                        <th>Kode</th>
                                        <th>Nama</th>
                                        <th>Jumlah Produk</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>
                                            <img src="{{ asset('images/slide1.jpg') }}" alt="produk 1" width='150px'>
                                            <div class="row mt-2">
                                                <div class="col">
                                                    <input type="file" name="gambar" id="gambar">
                                                </div>
                                                <div class="col-auto">
                                                    <button class="btn btn-sm btn-primary">Upload</button>
                                                </div>
                                            </div>
                                        </td>
                                        <td>KATE-1</td>
                                        <td>Baju Anak</td>
                                        <td>12 Produk</td>
                                        <td>
                                            <a href="{{ route('produk.show', 2) }}"
                                                class="btn btn-sm btn-primary mr-2 mb-2">
                                                Detail
                                            </a>
                                            <a href="{{ route('produk.edit', 2) }}"
                                                class="btn btn-sm btn-primary mr-2 mb-2">
                                                Edit
                                            </a>
                                            <button class="btn btn-sm btn-danger mb-2">
                                                Hapus
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>
                                            <img src="{{ asset('images/slide1.jpg') }}" alt="produk 1" width='150px'>
                                            <div class="row mt-2">
                                                <div class="col">
                                                    <input type="file" name="gambar" id="gambar">
                                                </div>
                                                <div class="col-auto">
                                                    <button class="btn btn-sm btn-primary">Upload</button>
                                                </div>
                                            </div>
                                        </td>
                                        <td>KATE-2</td>
                                        <td>Baju Wanita</td>
                                        <td>20 Produk</td>
                                        <td>
                                            <a href="{{ route('produk.show', 2) }}"
                                                class="btn btn-sm btn-primary mr-2 mb-2">
                                                Detail
                                            </a>
                                            <a href="{{ route('produk.edit', 2) }}"
                                                class="btn btn-sm btn-primary mr-2 mb-2">
                                                Edit
                                            </a>
                                            <button class="btn btn-sm btn-danger mb-2">
                                                Hapus
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>
                                            <img src="{{ asset('images/slide1.jpg') }}" alt="produk 1" width='150px'>
                                            <div class="row mt-2">
                                                <div class="col">
                                                    <input type="file" name="gambar" id="gambar">
                                                </div>
                                                <div class="col-auto">
                                                    <button class="btn btn-sm btn-primary">Upload</button>
                                                </div>
                                            </div>
                                        </td>
                                        <td>KATE-3</td>
                                        <td>Baju Wanita</td>
                                        <td>20 Produk</td>
                                        <td>
                                            <a href="{{ route('produk.show', 2) }}"
                                                class="btn btn-sm btn-primary mr-2 mb-2">
                                                Detail
                                            </a>
                                            <a href="{{ route('produk.edit', 2) }}"
                                                class="btn btn-sm btn-primary mr-2 mb-2">
                                                Edit
                                            </a>
                                            <button class="btn btn-sm btn-danger mb-2">
                                                Hapus
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

```

Refresh halaman [http://toko-klowor.local/admin/produk](http://toko-klowor.local/admin)

Kalau tidak ada error, maka akan tampil seperti gambar di bawah ini.

![image-20220331110426827](img\image-20220331110426827.png)

**# Halaman Form Produk Baru**

Masih di dalam folder produk, tambahkan 1 buah file lagi dengan nama **create.blade.php** dan isi dengan kode berikut.

```php+HTML
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Form Produk</h3>
                        <div class="card-tools">
                            <a href="{{ route('produk.index') }}" class="btn btn-sm btn-danger">
                                Tutup
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="#">
                            <div class="form-group">
                                <label for="nama_produk">Nama Produk</label>
                                <input type="text" name="nama_produk" id="nama_produk" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="slug_produk">Slug Produk</label>
                                <input type="text" name="slug_produk" id="slug_produk" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="deskripsi">Deskripsi</label>
                                <textarea name="deskripsi" id="deskripsi" cols="30" rows="5" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <button type="reset" class="btn btn-warning">Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

```

Sekarang klik tombol "baru" pada kanan atas halaman produk tadi, atau akses langsung ke halaman http://toko-klowor.local/admin/produk/create

Kalau tidak ada error, maka akan tampil halaman form produk baru.

![image-20220331110608497](img\image-20220331110608497.png)

**# Halaman input gambar produk**

Setelah produk dibuat, maka selanjutnya adalah form input gambar produk. Di dalam folder **produk** tambahkan 1 buah file lagi dengan nama **show.blade.php** dan isi dengan kode berikut.

```php+HTML
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-lg-4 col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Detail Produk</h3>
                        <div class="card-tools">
                            <a href="{{ route('produk.index') }}" class="btn btn-sm btn-danger">
                                Tutup
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <td>Kode Produk</td>
                                    <td>
                                        PRO-12
                                    </td>
                                </tr>
                                <tr>
                                    <td>Nama Produk</td>
                                    <td>
                                        Baju Anak
                                    </td>
                                </tr>
                                <tr>
                                    <td>Qty</td>
                                    <td>
                                        12 pcs
                                    </td>
                                </tr>
                                <tr>
                                    <td>Harga</td>
                                    <td>
                                        Rp. 15.0000
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-lg-8 col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Foto Produk</h3>
                    </div>
                    <div class="card-body">
                        <form action="#">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <input type="file" name="image" id="image">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Upload</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3 mb-2">
                                <img src="{{ asset('images/slide1.jpg') }}" alt="image" class="img-thumbnail mb-2">
                                <button class="btn-sm btn-danger btn">
                                    Delete
                                </button>
                            </div>

                            <div class="col-md-3 mb-2">
                                <img src="{{ asset('images/slide1.jpg') }}" alt="image" class="img-thumbnail mb-2">
                                <button class="btn-sm btn-danger btn">
                                    Delete
                                </button>
                            </div>

                            <div class="col-md-3 mb-2">
                                <img src="{{ asset('images/slide1.jpg') }}" alt="image" class="img-thumbnail mb-2">
                                <button class="btn-sm btn-danger btn">
                                    Delete
                                </button>
                            </div>

                            <div class="col-md-3 mb-2">
                                <img src="{{ asset('images/slide1.jpg') }}" alt="image" class="img-thumbnail mb-2">
                                <button class="btn-sm btn-danger btn">
                                    Delete
                                </button>
                            </div>

                            <div class="col-md-3 mb-2">
                                <img src="{{ asset('images/slide1.jpg') }}" alt="image" class="img-thumbnail mb-2">
                                <button class="btn-sm btn-danger btn">
                                    Delete
                                </button>
                            </div>

                            <div class="col-md-3 mb-2">
                                <img src="{{ asset('images/slide1.jpg') }}" alt="image" class="img-thumbnail mb-2">
                                <button class="btn-sm btn-danger btn">
                                    Delete
                                </button>
                            </div>

                            <div class="col-md-3 mb-2">
                                <img src="{{ asset('images/slide1.jpg') }}" alt="image" class="img-thumbnail mb-2">
                                <button class="btn-sm btn-danger btn">
                                    Delete
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

```

Sekarang pada halaman data produk, klik tombol detail atau akses halaman http://toko-klowor.local/admin/produk/1 apabila tidak ada error maka akan tampil halaman seperti gambar di bawah ini.

![image-20220331110807996](img\image-20220331110807996.png)

**# Halaman Form edit produk**

Terakhir tambahkan 1 buah file lagi di dalam folder **produk** dengan nama **edit.blade.php** dan isi dengan kode berikut.

```php+HTML
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Form Edit Produk</h3>
                        <div class="card-tools">
                            <a href="{{ route('produk.index') }}" class="btn btn-sm btn-danger">
                                Tutup
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="#">
                            <div class="form-group">
                                <label for="nama_produk">Nama Produk</label>
                                <input type="text" name="nama_produk" id="nama_produk" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="slug_produk">Slug Produk</label>
                                <input type="text" name="slug_produk" id="slug_produk" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="deskripsi">Deskripsi</label>
                                <textarea name="deskripsi" id="deskripsi" cols="30" rows="5" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Update</button>
                                <button type="reset" class="btn btn-warning">Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

```

Sekarang klik tombol edit pada halaman data produk atau akses di halaman http://toko-klowor.local/admin/produk/2/edit

Kalau tidak ada error, maka akan tampil halaman seperti berikut.

![image-20220331111001123](img\image-20220331111001123.png)

### Desain Dashboard Customer

Desain dashboard customer adalah halaman yang digunakan untuk menampilkan daftar customer yang mempunyai akun di toko kita. Sama seperti dashboard sebelumnya, kita membutuhkan 1 buah file **controller** dan **view**. Selain itu juga kita harus mengedit **routes/web.php** untuk mengarahkan ke halaman dashboard customer.

**# Buat Customer Controller**

Untuk membuat customer controller, buka terminal dan jalankan perintah

```php
php artisan make:controller CustomerController -r
```

Kemudian tekan enter.

Buka file **CustomerController.php** dan isi dengan kode berikut.

```php+HTML
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = array('title' => 'Data Customer');
        return view('customer.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = array('title' => 'Form Edit Customer');
        return view('customer.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

```

**# Edit web.php**

Setelah controller selesai kita buat, selanjutnya edit file **routes/web.php** untuk mengarahkan ke halaman admin/customer. Sekarang buka file **routes/web.php** dan sesuaikan seperti kode di bawah ini.

```php+HTML
Route::group(['prefix' => 'admin'], function () {
    Route::get('/', [DashboardController::class, 'index']);
    // route kategori
    Route::resource('kategori', KategoriController::class);
    // route produk
    Route::resource('produk', ProdukController::class);
    // route customer
    Route::resource('customer', CustomerController::class);
});
```

Akses halaman http://toko-klowor.local/admin/customer

Untuk saat ini masih error karena belum membuat file views-nya.

**# Halaman index customer**

Buat 1 buah folder baru di dalam folder views dengan nama customer. Kemudian di dalam folder **customer** buat 1 buah file dengan nama **index.blade.php** dan isi dengan kode berikut.

```php+HTML
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            Data Customer
                        </h3>
                    </div>
                    <div class="card-body">
                        <form action="#">
                            <div class="row">
                                <div class="col">
                                    <input type="text" name="keyword" id="keyword" class="form-control"
                                        placeholder="ketik keyword disini">
                                </div>
                                <div class="col-auto">
                                    <button class="btn btn-primary">
                                        Cari
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>No Tlp</th>
                                        <th>Alamat</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Amin</td>
                                        <td>amin@gmail.com</td>
                                        <td>085852527575</td>
                                        <td>
                                            Jln. Jend Sudirman no.1, Kudus
                                        </td>
                                        <td>
                                            Aktif
                                        </td>
                                        <td>
                                            <a href="{{ route('customer.edit', 1) }}"
                                                class="btn btn-sm btn-primary">Edit</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Budi</td>
                                        <td>budi@gmail.com</td>
                                        <td>085852527576</td>
                                        <td>
                                            Jln. Jend Sudirman no.2, Kudus
                                        </td>
                                        <td>
                                            Aktif
                                        </td>
                                        <td>
                                            <a href="{{ route('customer.edit', 2) }}"
                                                class="btn btn-sm btn-primary">Edit</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Iwan</td>
                                        <td>iwan@gmail.com</td>
                                        <td>085852527577</td>
                                        <td>
                                            Jln. Jend Sudirman no.3, Kudus
                                        </td>
                                        <td>
                                            Aktif
                                        </td>
                                        <td>
                                            <a href="{{ route('customer.edit', 3) }}"
                                                class="btn btn-sm btn-primary">Edit</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Lala</td>
                                        <td>lala@gmail.com</td>
                                        <td>085852527578</td>
                                        <td>
                                            Jln. Jend Sudirman no.4, Kudus
                                        </td>
                                        <td>
                                            Aktif
                                        </td>
                                        <td>
                                            <a href="{{ route('customer.edit', 4) }}"
                                                class="btn btn-sm btn-primary">Edit</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>Didi</td>
                                        <td>didi@gmail.com</td>
                                        <td>085852527579</td>
                                        <td>
                                            Jln. Jend Sudirman no.5, Kudus
                                        </td>
                                        <td>
                                            Aktif
                                        </td>
                                        <td>
                                            <a href="{{ route('customer.edit', 5) }}"
                                                class="btn btn-sm btn-primary">Edit</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

```

Sekarang akses kembali halaman http://toko-klowor.local/admin/customer tampilannya akan berubah menjadi seperti gambar di bawah ini.

![image-20220331111548034](img\image-20220331111548034.png)

**# Halaman edit customer**

Masih di dalam folder **customer**, tambahkan 1 buah file lagi dengan nama **edit.blade.php** dan isi dengan kode berikut.

```php+HTML
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-lg-4 col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            Form Edit Customer
                        </h3>
                        <div class="card-tools">
                            <a href="{{ route('customer.index') }}" class="btn btn-sm btn-danger">
                                Tutup
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>
                                        Nama
                                    </td>
                                    <td>
                                        Amin
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Email
                                    </td>
                                    <td>
                                        amin@gmail.com
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        No Tlp
                                    </td>
                                    <td>
                                        Alamat
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Status
                                    </td>
                                    <td>
                                        <form action="#">
                                            <div class="form-group">
                                                <select name="status" id="status" class="form-control">
                                                    <option value="aktif">Aktif</option>
                                                    <option value="nonaktif">Non Aktif</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary">Update</button>
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

```

Sekarang buka halaman edit customer dengan mengakses halaman http://toko-klowor.local/admin/customer/1/edit tampilannya akan seperti gambar di bawah ini.

![image-20220331111701626](img\image-20220331111701626.png)

**# Edit menu dashboard**

Agar bisa diakses melalui menu di dashboard, sekarang buka file **menudashboard.blade.php** dan tambahkan link ke dashboard customer dengan mengubah kode berikut ini.

```php+HTML
<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
      <!-- Add icons to the links using the .nav-icon class
            with font-awesome or any other icon font library -->
      <li class="nav-item">
        <a href="#" class="nav-link">
          <i class="nav-icon fas fa-th"></i>
          <p>
            Dashboard
          </p>
        </a>
      </li>
      <li class="nav-item has-treeview">
        <a href="#" class="nav-link">
          <i class="nav-icon fas fa-folder-open"></i>
          <p>
            Produk
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="{{ route('produk.index') }}" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>Produk</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('kategori.index') }}" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>Kategori</p>
            </a>
          </li>
        </ul>
      </li>
      <li class="nav-item has-treeview">
        <a href="#" class="nav-link">
          <i class="nav-icon fas fa-shopping-cart"></i>
          <p>
            Transaksi
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>Transaksi Baru</p>
            </a>
          </li>
        </ul>
      </li>
      <li class="nav-item has-treeview">
        <a href="#" class="nav-link">
          <i class="nav-icon fas fa-folder"></i>
          <p>
            Data
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="{{ route('customer.index') }}" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>Customer</p>
            </a>
          </li>
        </ul>
      </li>
      <li class="nav-item has-treeview">
        <a href="#" class="nav-link">
          <i class="nav-icon fas fa-list"></i>
          <p>
            Laporan
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>Penjualan</p>
            </a>
          </li>
        </ul>
      </li>
      <li class="nav-item">
        <a href="#" class="nav-link">
          <i class="nav-icon fas fa-sign-out-alt"></i>
          <p>
            Sign Out
          </p>
        </a>
      </li>
    </ul>
  </nav>

```

Sekarang coba buka halaman dashboard customer dengan mengklik menu data -> customer. Sampai sini berarti desain dashboard customer selesai kita buat. Pada tutorial selanjutnya kita akan membuat halaman dashboard transaksi.

### Dashboard Transaksi

Dashboard customer selesai kita buat, sekarang kita akan membuat halaman dashboard transaksi. Dashboard transaksi adalah halaman yang menampilkan daftar penjualan. Sama seperti sebelumnya, kita akan membuat 1 buah controller dan 3 buah views. Dan juga mengedit file **routes/web.php** untuk mengarahkan ke halaman dashboard transaksi.

**# Buat Transaksi Controller**

Buka terminal dan jalankan perintah di bawah ini.

```php
php artisan make:controller TransaksiController -r
```

Kemudian tekan enter. Setelah itu, buka file TransaksiController.php dan isi dengan kode berikut.

```php+HTML
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = array('title' => 'Data Transaksi');
        return view('transaksi.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = array('title' => 'Detail Transaksi');
        return view('transaksi.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = array('title' => 'Form Edit Transaksi');
        return view('transaksi.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

```

**# Edit routes/web.php**

Untuk mengarahkan ke halaman dashboard transaksi, buka file **routes/web.php** dan tambahkan route transaksi.

```php+HTML
Route::group(['prefix' => 'admin'], function () {
    Route::get('/', [DashboardController::class, 'index']);
    // route kategori
    Route::resource('kategori', KategoriController::class);
    // route produk
    Route::resource('produk', ProdukController::class);
    // route customer
    Route::resource('customer', CustomerController::class);
    // route transaksi
    Route::resource('transaksi', TransaksiController::class);
});
```

Sekarang coba akses halaman http://toko-klowor.local/admin/transaksi sampai disini masih ada error. Kita akan memperbaikinya dengan membuat file viewsnya.

**# Buat halaman index.blade.php**

Buat 1 buah folder dengan nama **transaksi** di dalam folder views. Kemudian di dalam folder transaksi buat 1 buah file dengan nama **index.blade.php** dan isi dengan kode berikut.

```php+HTML
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            Data Transaksi
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Invoice</th>
                                        <th>Sub Total</th>
                                        <th>Diskon</th>
                                        <th>Ongkir</th>
                                        <th>Total</th>
                                        <th>Status Pembayaran</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            1
                                        </td>
                                        <td>
                                            Inv-01
                                        </td>
                                        <td>
                                            200.000
                                        </td>
                                        <td>
                                            0
                                        </td>
                                        <td>
                                            27.000
                                        </td>
                                        <td>
                                            227.000
                                        </td>
                                        <td>
                                            Belum dibayar
                                        </td>
                                        <td>
                                            Checkout
                                        </td>
                                        <td>
                                            <a href="{{ route('transaksi.show', 1) }}" class="btn btn-sm btn-info mb-2">
                                                Detail
                                            </a>
                                            <a href="{{ route('transaksi.edit', 1) }}"
                                                class="btn btn-sm btn-primary mb-2">
                                                Edit
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            2
                                        </td>
                                        <td>
                                            Inv-02
                                        </td>
                                        <td>
                                            200.000
                                        </td>
                                        <td>
                                            0
                                        </td>
                                        <td>
                                            27.000
                                        </td>
                                        <td>
                                            227.000
                                        </td>
                                        <td>
                                            Belum dibayar
                                        </td>
                                        <td>
                                            Checkout
                                        </td>
                                        <td>
                                            <a href="{{ route('transaksi.show', 2) }}" class="btn btn-sm btn-info mb-2">
                                                Detail
                                            </a>
                                            <a href="{{ route('transaksi.edit', 2) }}"
                                                class="btn btn-sm btn-primary mb-2">
                                                Edit
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            3
                                        </td>
                                        <td>
                                            Inv-03
                                        </td>
                                        <td>
                                            200.000
                                        </td>
                                        <td>
                                            0
                                        </td>
                                        <td>
                                            27.000
                                        </td>
                                        <td>
                                            227.000
                                        </td>
                                        <td>
                                            Belum dibayar
                                        </td>
                                        <td>
                                            Checkout
                                        </td>
                                        <td>
                                            <a href="{{ route('transaksi.show', 3) }}" class="btn btn-sm btn-info mb-2">
                                                Detail
                                            </a>
                                            <a href="{{ route('transaksi.edit', 3) }}"
                                                class="btn btn-sm btn-primary mb-2">
                                                Edit
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            4
                                        </td>
                                        <td>
                                            Inv-04
                                        </td>
                                        <td>
                                            200.000
                                        </td>
                                        <td>
                                            0
                                        </td>
                                        <td>
                                            27.000
                                        </td>
                                        <td>
                                            227.000
                                        </td>
                                        <td>
                                            Belum dibayar
                                        </td>
                                        <td>
                                            Checkout
                                        </td>
                                        <td>
                                            <a href="{{ route('transaksi.show', 4) }}" class="btn btn-sm btn-info mb-2">
                                                Detail
                                            </a>
                                            <a href="{{ route('transaksi.edit', 4) }}"
                                                class="btn btn-sm btn-primary mb-2">
                                                Edit
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            5
                                        </td>
                                        <td>
                                            Inv-05
                                        </td>
                                        <td>
                                            200.000
                                        </td>
                                        <td>
                                            0
                                        </td>
                                        <td>
                                            27.000
                                        </td>
                                        <td>
                                            227.000
                                        </td>
                                        <td>
                                            Belum dibayar
                                        </td>
                                        <td>
                                            Checkout
                                        </td>
                                        <td>
                                            <a href="{{ route('transaksi.show', 5) }}" class="btn btn-sm btn-info mb-2">
                                                Detail
                                            </a>
                                            <a href="{{ route('transaksi.edit', 5) }}"
                                                class="btn btn-sm btn-primary mb-2">
                                                Edit
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

```

Sekarang akses kembali http://toko-klowor.local/admin/transaksi 

Kalau tidak ada error, maka akan tampil seperti gambar di bawah ini.

![image-20220331112729980](img\image-20220331112729980.png)

**# Buat halaman show.blade.php**

Masih di dalam folder yang sama yaitu **transaksi**, buat 1 buah file lagi dengan nama **show.blade.php** dan isi dengan kode berikut.

```php+HTML
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-lg-8 col-md-8 mb-2">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Item</h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>
                                            No
                                        </th>
                                        <th>
                                            Kode
                                        </th>
                                        <th>
                                            Nama
                                        </th>
                                        <th>
                                            Harga
                                        </th>
                                        <th>
                                            Qty
                                        </th>
                                        <th>
                                            Subtotal
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>KATE-1</td>
                                        <td>Baju Anak</td>
                                        <td class="text-right">15.000</td>
                                        <td class="text-right">2</td>
                                        <td class="text-right">30.000</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>KATE-2</td>
                                        <td>Baju Anak</td>
                                        <td class="text-right">25.000</td>
                                        <td class="text-right">2</td>
                                        <td class="text-right">50.000</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>KATE-3</td>
                                        <td>Baju Anak</td>
                                        <td class="text-right">35.000</td>
                                        <td class="text-right">2</td>
                                        <td class="text-right">70.000</td>
                                    </tr>
                                    <tr>
                                        <td colspan="5">
                                            <b>Total</b>
                                        </td>
                                        <td class="text-right">
                                            <b>150.000</b>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="{{ route('transaksi.index') }}" class="btn btn-sm btn-danger">Tutup</a>
                    </div>
                </div>
            </div>
            <div class="col col-lg-4 col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Ringkasan</h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>
                                            Total
                                        </td>
                                        <td>
                                            127.000
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Subtotal
                                        </td>
                                        <td>
                                            150.000
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Diskon
                                        </td>
                                        <td>
                                            0
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Ongkir
                                        </td>
                                        <td>
                                            27.000
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Ekspedisi
                                        </td>
                                        <td>
                                            JNE
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            No. Resi
                                        </td>
                                        <td>
                                            123123123123123
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Status Pembayaran
                                        </td>
                                        <td>
                                            Sudah dibayar
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Status
                                        </td>
                                        <td>
                                            Dikirim
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

```

Halaman ini digunakan untuk menampilkan detail transaksi. Untuk mengaksesnya buka halaman http://toko-klowor.local/admin/transaksi/1 hasilnya akan tampil seperti berikut.

![image-20220331112912729](img\image-20220331112912729.png)

**# Halaman edit transaksi**

Masih di folder **transaksi**, buat 1 buah file lagi dengan nama **edit.blade.php** dan isi dengan kode berikut.

```php+HTML
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-lg-8 col-md-8 mb-2">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Item</h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>
                                            No
                                        </th>
                                        <th>
                                            Kode
                                        </th>
                                        <th>
                                            Nama
                                        </th>
                                        <th>
                                            Harga
                                        </th>
                                        <th>
                                            Qty
                                        </th>
                                        <th>
                                            Subtotal
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>KATE-1</td>
                                        <td>Baju Anak</td>
                                        <td class="text-right">15.000</td>
                                        <td class="text-right">2</td>
                                        <td class="text-right">30.000</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>KATE-2</td>
                                        <td>Baju Anak</td>
                                        <td class="text-right">25.000</td>
                                        <td class="text-right">2</td>
                                        <td class="text-right">50.000</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>KATE-3</td>
                                        <td>Baju Anak</td>
                                        <td class="text-right">35.000</td>
                                        <td class="text-right">2</td>
                                        <td class="text-right">70.000</td>
                                    </tr>
                                    <tr>
                                        <td colspan="5">
                                            <b>Total</b>
                                        </td>
                                        <td class="text-right">
                                            <b>150.000</b>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="{{ route('transaksi.index') }}" class="btn btn-sm btn-danger">Tutup</a>
                    </div>
                </div>
            </div>
            <div class="col col-lg-4 col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Ringkasan</h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <form action="">
                                    <tbody>
                                        <tr>
                                            <td>
                                                Total
                                            </td>
                                            <td>
                                                <input type="text" name="total" id="total" class="form-control"
                                                    value="150.000">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Subtotal
                                            </td>
                                            <td>
                                                <input type="text" name="subtotal" id="subtotal" class="form-control"
                                                    value="150.000">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Diskon
                                            </td>
                                            <td>
                                                <input type="text" name="diskon" id="diskon" class="form-control"
                                                    value="0">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Ongkir
                                            </td>
                                            <td>
                                                <input type="text" name="ongkir" id="ongkir" class="form-control"
                                                    value="27.000">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Ekspedisi
                                            </td>
                                            <td>
                                                <input type="text" name="ekspedisi" id="ekspedisi" class="form-control"
                                                    value="jne">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                No. Resi
                                            </td>
                                            <td>
                                                <input type="text" name="no_resi" id="no_resi" class="form-control"
                                                    value="123123123123">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Status Pembayaran
                                            </td>
                                            <td>
                                                <select name="status_pembayaran" id="status_pembayaran"
                                                    class="form-control">
                                                    <option value="sudah">Sudah Dibayar</option>
                                                    <option value="belum">Belum Dibayar</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Status
                                            </td>
                                            <td>
                                                <select name="status" id="status" class="form-control">
                                                    <option value="checkout">Checkout</option>
                                                    <option value="diproses">Diproses</option>
                                                    <option value="dikirim">Dikirim</option>
                                                    <option value="diterima">Diterima</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <button type="submit" class="btn btn-primary">Update</button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </form>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

```

Sesuai namanya, halaman edit ini digunakan untuk mengedit data transaksi. Untuk mengaksesnya buka halaman http://toko-klowor.local/admin/transaksi/1/edit tampilannya akan tampak seperti gambar berikut.

![image-20220331113053146](img\image-20220331113053146.png)

**# Ubah menu dashboard**

Ubah menu dashboard seperti yang pada tutorial dashboard customer tadi. Buka file **menudashboard.blade.php** dan ubah pada bagian menu transaksi dengan menambahkan link ke halaman dashboard transaksi.

```php+HTML
<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
            with font-awesome or any other icon font library -->
        <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-th"></i>
                <p>
                    Dashboard
                </p>
            </a>
        </li>
        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-folder-open"></i>
                <p>
                    Produk
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ route('produk.index') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Produk</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('kategori.index') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Kategori</p>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-shopping-cart"></i>
                <p>
                    Transaksi
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ route('transaksi.index') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Data Transaksi</p>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-folder"></i>
                <p>
                    Data
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ route('customer.index') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Customer</p>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-list"></i>
                <p>
                    Laporan
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Penjualan</p>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-sign-out-alt"></i>
                <p>
                    Sign Out
                </p>
            </a>
        </li>
    </ul>
</nav>

```

Refresh halaman dashboard, sekarang coba akses menggunakan menu dashboard yang barusan kita ubah.

### Desain Profil User

Desain profil user digunakan untuk menampilkan data masing-masing member, seperti foto profil, nama dan email. Untuk membuat profil dan setting profil, kita membutuhkan 1 buah controller dan 2 buah file views.

**# Buat User Controller**

Buka terminal dan ketikkan perintah

```markup
php artisan make:controller UserController
```

Tekan enter, kemudian buka file **UserController.php** dan isi dengan kode berikut.

```php+HTML
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    //
    public function index() {
        $data = array('title' => 'User Profil');
        return view('user.index', $data);
    }

    public function setting() {
        $data = array('title' => 'Setting Profil');
        return view('user.setting', $data);
    }
}

```

**# Edit Route**

Buka file routes/web.php dan edit dengan menambahkan 2 buah route

```php
Route::group(['prefix' => 'admin'], function () {
    Route::get('/', [DashboardController::class, 'index']);
    // route kategori
    Route::resource('kategori', KategoriController::class);
    // route produk
    Route::resource('produk', ProdukController::class);
    // route customer
    Route::resource('customer', CustomerController::class);
    // route transaksi
    Route::resource('transaksi', TransaksiController::class);
    // profil
    Route::get('profil', [UserController::class, 'index']);
    // setting profil
    Route::get('setting', [UserController::class, 'setting']);
});
```

Selanjutnya buatlah 2 buah file views.

**# Halaman profil**

Buat 1 buah folder baru di dalam folder views dengan nama **user**. Selanjutnya buat 1 buah file **index.blade.php** dan isi dengan kode berikut.

```php+HTML
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-lg-4 col-md-4">
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                        <div class="text-center">
                            <img src="{{ asset('img/user1-128x128.jpg') }}" alt="profil"
                                class="profile-user-img img-responsive img-circle">
                        </div>
                        <h3 class="profile-username text-center">Fadlur Rohman</h3>
                        <p class="text-muted text-center">Member sejak : 20 Des 2020</p>
                        <hr>
                        <strong>
                            <i class="fas fa-map-marker mr-2"></i>
                            Alamat
                        </strong>
                        <p class="text-muted">
                            Jekulo, Kab Kudus
                        </p>
                        <hr>
                        <strong>
                            <i class="fas fa-envelope mr-2"></i>
                            Email
                        </strong>
                        <p class="text-muted">
                            f4dlur@gmail.com
                        </p>
                        <hr>
                        <strong>
                            <i class="fas fa-phone mr-2"></i>
                            No Tlp
                        </strong>
                        <p class="text-muted">
                            Jekulo, Kab Kudus
                        </p>
                        <hr>
                        <a href="{{ URL::to('admin/setting') }}" class="btn btn-primary btn-block">Setting</a>
                    </div>
                </div>
            </div>
            <div class="col col-lg-8 col-md-8">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">History Transaksi</h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Invoice</th>
                                        <th>Sub Total</th>
                                        <th>Diskon</th>
                                        <th>Ongkir</th>
                                        <th>Total</th>
                                        <th>Status Pembayaran</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            1
                                        </td>
                                        <td>
                                            Inv-01
                                        </td>
                                        <td>
                                            200.000
                                        </td>
                                        <td>
                                            0
                                        </td>
                                        <td>
                                            27.000
                                        </td>
                                        <td>
                                            227.000
                                        </td>
                                        <td>
                                            Belum dibayar
                                        </td>
                                        <td>
                                            Checkout
                                        </td>
                                        <td>
                                            <a href="{{ route('transaksi.show', 1) }}" class="btn btn-sm btn-info mb-2">
                                                Detail
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            2
                                        </td>
                                        <td>
                                            Inv-02
                                        </td>
                                        <td>
                                            200.000
                                        </td>
                                        <td>
                                            0
                                        </td>
                                        <td>
                                            27.000
                                        </td>
                                        <td>
                                            227.000
                                        </td>
                                        <td>
                                            Belum dibayar
                                        </td>
                                        <td>
                                            Checkout
                                        </td>
                                        <td>
                                            <a href="{{ route('transaksi.show', 2) }}" class="btn btn-sm btn-info mb-2">
                                                Detail
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            3
                                        </td>
                                        <td>
                                            Inv-03
                                        </td>
                                        <td>
                                            200.000
                                        </td>
                                        <td>
                                            0
                                        </td>
                                        <td>
                                            27.000
                                        </td>
                                        <td>
                                            227.000
                                        </td>
                                        <td>
                                            Belum dibayar
                                        </td>
                                        <td>
                                            Checkout
                                        </td>
                                        <td>
                                            <a href="{{ route('transaksi.show', 3) }}" class="btn btn-sm btn-info mb-2">
                                                Detail
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            4
                                        </td>
                                        <td>
                                            Inv-04
                                        </td>
                                        <td>
                                            200.000
                                        </td>
                                        <td>
                                            0
                                        </td>
                                        <td>
                                            27.000
                                        </td>
                                        <td>
                                            227.000
                                        </td>
                                        <td>
                                            Belum dibayar
                                        </td>
                                        <td>
                                            Checkout
                                        </td>
                                        <td>
                                            <a href="{{ route('transaksi.show', 4) }}" class="btn btn-sm btn-info mb-2">
                                                Detail
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            5
                                        </td>
                                        <td>
                                            Inv-05
                                        </td>
                                        <td>
                                            200.000
                                        </td>
                                        <td>
                                            0
                                        </td>
                                        <td>
                                            27.000
                                        </td>
                                        <td>
                                            227.000
                                        </td>
                                        <td>
                                            Belum dibayar
                                        </td>
                                        <td>
                                            Checkout
                                        </td>
                                        <td>
                                            <a href="{{ route('transaksi.show', 5) }}" class="btn btn-sm btn-info mb-2">
                                                Detail
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

```

Akses halaman profil http://toko-klowor.local/admin/profil hasilnya akan tampak seperti gambar di bawah ini.

![image-20220331113712468](img\image-20220331113712468.png)

**# Buat halaman setting profil**

Setelah halaman profil selesai kita buat, selanjutnya buat 1 buah file lagi di dalam folder **user** dengan nama **setting.blade.php** dan isi dengan kode berikut

```php+HTML
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-lg-4 col-md-4">
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                        <div class="text-center">
                            <img src="{{ asset('img/user1-128x128.jpg') }}" alt="profil"
                                class="profile-user-img img-responsive img-circle">
                        </div>
                        <form action="">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <input type="file" name="foto" id="foto">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Upload</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <hr>
                        <form action="">
                            <div class="form-group">
                                <label for="name">Nama</label>
                                <input type="text" name="name" id="name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="phone">No Tlp</label>
                                <input type="text" name="phone" id="phone" class="form-control">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

```

Akses kembali dengan alamat http://toko-klowor.local/admin/setting

![image-20220331113839727](img\image-20220331113839727.png)

**# Tambahkan Menu Profil**

Untuk menambahkan menu ke halaman profil, buka file **menudashboard.blade.php** dan edit menjadi seperti berikut.

```php+HTML
<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
            with font-awesome or any other icon font library -->
        <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-th"></i>
                <p>
                    Dashboard
                </p>
            </a>
        </li>
        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-folder-open"></i>
                <p>
                    Produk
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ route('produk.index') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Produk</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('kategori.index') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Kategori</p>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-shopping-cart"></i>
                <p>
                    Transaksi
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ route('transaksi.index') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Data Transaksi</p>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-folder"></i>
                <p>
                    Data
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ route('customer.index') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Customer</p>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-list"></i>
                <p>
                    Laporan
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Penjualan</p>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item">
            <a href="{{ URL::to('admin/profil') }}" class="nav-link">
                <i class="nav-icon fas fa-users"></i>
                <p>
                    Profil
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-sign-out-alt"></i>
                <p>
                    Sign Out
                </p>
            </a>
        </li>
    </ul>
</nav>

```

Sekarang refresh halaman profil, dan klik menu profil yang barusan kita buat.

### Desain Laporan Penjualan

Setelah halaman transaksi dan customer sudah kita buat, sekarang kita akan membuat halaman laporan penjualan. Kita cukup membuat 1 buah controller dan 2 buah file views.

**# Buat Laporan Controller**

Buka terminal dan jalankan perintah

```markup
php artisan make:controller LaporanController
```

tekan enter dan tunggu sampai file **LaporanController.php** selesai dibuat. Selanjutnya buka file **LaporanController.php** dan isi dengan kode berikut.

```php+HTML
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LaporanController extends Controller
{
    //
    public function index()
    {
        $data = array('title' => 'Form Laporan Penjualan');
        return view('laporan.index', $data);
    }

    public function proses()
    {
        $data = array('title' => 'Laporan Penjualan');
        return view('laporan.proses', $data);
    }
}

```

**# Tambahkan route laporan**

Buka file **routes/web.php** dan tambahkan route ke form laporan dan proses laporan.

```php
Route::group(['prefix' => 'admin'], function () {
    Route::get('/', [DashboardController::class, 'index']);
    // route kategori
    Route::resource('kategori', KategoriController::class);
    // route produk
    Route::resource('produk', ProdukController::class);
    // route customer
    Route::resource('customer', CustomerController::class);
    // route transaksi
    Route::resource('transaksi', TransaksiController::class);
    // profil
    Route::get('profil', [UserController::class, 'index']);
    // setting profil
    Route::get('setting', [UserController::class, 'setting']);
    // form laporan
    Route::get('laporan', [LaporanController::class, 'index']);
    // proses laporan
    Route::get('proseslaporan', [LaporanController::class, 'proses']);
});
```

Selanjutnya buat 2 buah file views.

**# Buat file form laporan**

Buat 1 buah folder baru di folder views dengan nama **laporan**. Pada folder laporan buat 1 buah file dengan nama **index.blade.php** dan isi dengan kode berikut.

```php+HTML
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-lg-4 col-md-4">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">Form Laporan</h3>
                    </div>
                    <div class="card-body">
                        <form action="#">
                            <div class="form-group">
                                <label for="bulan">Bulan</label>
                                <select name="bulan" id="bulan" class="form-control">
                                    <option value="1">Januari</option>
                                    <option value="2">Februari</option>
                                    <option value="3">Maret</option>
                                    <option value="4">April</option>
                                    <option value="5">Mei</option>
                                    <option value="6">Juni</option>
                                    <option value="7">Juli</option>
                                    <option value="8">Agustus</option>
                                    <option value="9">September</option>
                                    <option value="10">Oktober</option>
                                    <option value="11">Nopember</option>
                                    <option value="12">Desember</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="tahun">Tahun</label>
                                <select name="tahun" id="tahun" class="form-control">
                                    @for ($a = 2019; $a <= 2050; $a++)
                                        <option value="{{ $a }}">{{ $a }}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Buka Laporan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

```

Tekan enter kemudian akses pada halaman http://toko-klowor.local/admin/laporan

Tampilannya akan menjadi seperti gambar di bawah ini.

![image-20220331114316813](img\image-20220331114316813.png)

**# Buat file proses laporan**

Setelah form laporan kita buat, selanjutnya buat 1 buah file untuk menampilkan hasil laporan. Masih di folder laporan, buat 1 buah file lagi dengan nama **proses.blade.php** dan isi dengan kode berikut.

```php+HTML
@extends('layouts.dashboard')
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col">
      <div class="card card-primary card-outline">
        <div class="card-header">
          <h3 class="card-title">Laporan Penjualan</h3>
        </div>
        <div class="card-body">
          <h3 class="text-center">Periode Bulan Januari 2021</h3>
          <div class="row">
            <div class="col col-lg-4 col-md-4">
              <h4 class="text-center">Ringkasan Transaksi</h4>
              <table class="table table-bordered">
                <tbody>
                  <tr>
                    <td>Total Penjualan</td>
                    <td>Rp. 15.000.000</td>
                  </tr>
                  <tr>
                    <td>Total Transaksi</td>
                    <td>200 Transaksi</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="col col-lg-8 col-md-8">
              <h4 class="text-center">Rincian Transaksi</h4>
              <div class="table-responsive">
                <table class="table table-stripped">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Invoice</th>
                      <th>Subtotal</th>
                      <th>Ongkir</th>
                      <th>Diskon</th>
                      <th>Total</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>Inv-01</td>
                      <td>100.000</td>
                      <td>27.000</td>
                      <td>0</td>
                      <td>127.000</td>
                    </tr>
                    <tr>
                      <td>2</td>
                      <td>Inv-02</td>
                      <td>100.000</td>
                      <td>27.000</td>
                      <td>0</td>
                      <td>127.000</td>
                    </tr>
                    <tr>
                      <td>3</td>
                      <td>Inv-03</td>
                      <td>100.000</td>
                      <td>27.000</td>
                      <td>0</td>
                      <td>127.000</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
```

Selanjutnya akses di halaman [http://toko-klowor.local/admin/proseslaporan ](http://toko-klowor.local/admin/proseslaporan)dan akan tampil seperti gambar

![image-20220331114436200](img\image-20220331114436200.png)

**# Edit menu dashboard**

Controller dan view telah kita buat, selanjutnya edit file menu dashboard agar bisa mengarah ke halaman form laporan. Buka file **menudashboard.blade.php** dan ubah pada bagian menu laporan -> penjualan menjadi seperti berikut.

```php+HTML
<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
            with font-awesome or any other icon font library -->
        <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-th"></i>
                <p>
                    Dashboard
                </p>
            </a>
        </li>
        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-folder-open"></i>
                <p>
                    Produk
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ route('produk.index') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Produk</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('kategori.index') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Kategori</p>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-shopping-cart"></i>
                <p>
                    Transaksi
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ route('transaksi.index') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Data Transaksi</p>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-folder"></i>
                <p>
                    Data
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ route('customer.index') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Customer</p>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-list"></i>
                <p>
                    Laporan
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ URL::to('admin/laporan') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Penjualan</p>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item">
            <a href="{{ URL::to('admin/profil') }}" class="nav-link">
                <i class="nav-icon fas fa-users"></i>
                <p>
                    Profil
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-sign-out-alt"></i>
                <p>
                    Sign Out
                </p>
            </a>
        </li>
    </ul>
</nav>
```

Proses desain tampilan toko kita hampir selesai, masih ada 3 bagian lagi yaitu desain login, register dan reset password.