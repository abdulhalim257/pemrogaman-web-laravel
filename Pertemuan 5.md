# Authentication Laravel

Salah satu fitur laravel adalah Authorization, fitur ini membantu kita untuk membuat fungsi authentication seperti register dan login menjadi lebih mudah. Langsung saja kita buat dengan membuat terminal dan ketikkan perintah berikut.

1. `composer require laravel/ui`

   ![image-20220402105238237](img\image-20220402105238237.png)

   Tunggu sampai proses selesai.

2. `php artisan ui bootstrap --auth`

   ![image-20220402105329019](img\image-20220402105329019.png)

   Tunggu sampai proses selesai.

3. `npm install && npm run dev`

   ![image-20220402105651154](img\image-20220402105651154.png)

   Tunggu sampai proses selesai. Jika terdapat error sepeti berikut, masukkan `npm run development`

   ![image-20220402105811052](img\image-20220402105811052.png)

4. Atur file **.env** sebagai berikut

   ![image-20220402110039059](img\image-20220402110039059.png)

   **Sesuaikan dengan setting masing-masing komputer!**

5. `php artisan migrate`

   ![image-20220402110112781](img\image-20220402110112781.png)

   Tunggu sampai proses selesai.

**Setelah proses selesai, maka di dalam folder controller akan ada 1 buah folder baru dengan nama Auth yang berisi beberapa file**.

![image-20220402110348708](img\image-20220402110348708.png)

Sekarang kita coba dulu akses halaman http://toko-klowor.local/login maka akan tampil halaman seperti berikut.

![image-20220402110428685](img\image-20220402110428685.png)

Ketika kita klik menu Register maka akan tampil form register.

![image-20220402110701906](img\image-20220402110701906.png)

Tampilan login dan register nanti akan kita edit agar sesuai dengan template yang telah kita buat sebelumnya. Untuk saat ini kita edit aja dulu menu biar bisa nyambung ke halaman login.

**# Edit menu**

Buka file **layouts/menu.blade.php** kemudian edit bagian Login menjadi seperti berikut.

Yang tadinya

```php+HTML
<li class="nav-item">
	<a class="nav-link" href="#">Login</a>
</li>
```

menjadi

```php+HTML
<li class="nav-item">
      <a class="nav-link" href="{{ URL::to('login') }}">Login</a>
</li>
```

Sekarang kita akan mengubah tampilan login menjadi sesuai dengan template yang telah kita buat.

**# Edit LoginController.php**

Buka file **Auth/LoginController.php** dan pada bagian bawah `function construct` kita tambahkan 1 buah function baru.

```php
public function showLoginForm()
{
    $data = array('title' => 'Login');
    return view('auth.login', $data);
}
```

Sehingga lengkapnya akan menjadi seperti ini.

```php+HTML
<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function showLoginForm()
    {
        $data = array('title' => 'Login');
        return view('auth.login', $data);
    }
}
```

Setelah controller selesai kita edit, selanjutnya kita ubah viewsnya.

**# Edit login.blade.php**

Pada folder views juga ada folder baru dengan nama *auth.* Buka file dengan nama **login.blade.php** di folder **auth** tadi dan ubah menjadi seperti berikut.

```php+HTML
@extends('layouts.template')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    @if(count($errors) > 0)
                    @foreach($errors->all() as $error)
                        <div class="alert alert-warning">{{ $error }}</div>
                    @endforeach
                    @endif
                    @if ($message = Session::get('error'))
                        <div class="alert alert-warning">
                            <p>{{ $message }}</p>
                        </div>
                    @endif
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" name="email" id="email" required class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" name="password" id="password" required class="form-control">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary mb-4">Login</button>
                            <p>Don't have an account? <a href="{{ route('register') }}" class="text-decoration-none">Sign up</a></p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
```

Kalau sudah coba akses lagi menu login. Maka tampilannya akan berubah menjadi seperti ini.

![image-20220402111254161](img\image-20220402111254161.png)

**# Edit RegisterController.php**

Kembali lagi ke folder **Auth** di dalam folder controller, buka file **RegisterController.php**, kemudian di bagian bawah `function create(array $data)` tambahkan 1 buah function baru. Sehingga keseluruhan kodenya menjadi seperti berikut.

```php+HTML
<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
    public function showRegistrationForm()
    {
        $data = array('title' => 'Register');
        return view('auth.register', $data);
    }
}

```

Controller registernya udah kita ubah, selanjutnya ubah juga file views registernya.

**# Edit register.blade.php**

Selanjutnya kita akan mengubah tampilan dari halaman register. Masih di folder **auth** yang di views, buka file **register.blade.php** dan ubah menjadi seperti ini.

```php+HTML
@extends('layouts.template')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">{{ __('Register') }}</div>

                    <div class="card-body">
                        @if (count($errors) > 0)
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-warning">{{ $error }}</div>
                            @endforeach
                        @endif
                        @if ($message = Session::get('error'))
                            <div class="alert alert-warning">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        <form method="POST" action="{{ route('register') }}">
                            <div class="row">
                                <div class="col">
                                    @csrf
                                    <div class="form-group">
                                        <label for="name">Nama</label>
                                        <input type="text" name="name" id="name" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="text" name="email" id="email" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" name="password" id="password" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="password_confirmation">Confirm Password</label>
                                        <input type="password" name="password_confirmation" id="password_confirmation"
                                            class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary mb-4">Register</button>
                                        <p>Sudah punya akun? Login <a href="{{ route('login') }}"
                                                class="text-decoration-none">disini</a></p>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

```

![image-20220402111910529](img\image-20220402111910529.png)

Sampai sini, selesai desain untuk tampilan toko online kita. Pada tutorial selanjutnya kita akan membuat databasenya.

### Setting Database

Desain tampilan toko online sudah kita buat begitu juga untuk dashboardnya. Sekarang kita akan mengatur database dan bagaimana cara menghubungkan dengan aplikasi kita.

Jika membuatnya dengan menggunakan laragon, maka kita tinggal melihat setting nya di **.env**

Yang perlu diedit adalah

- DB_DATABASE diisi tokoku sesuai database yang kita buat tadi
- DB_USERNAME diisi username mysql (bawaan laragon biasanya root)
- DB_PASSWORD diisi password mysql, kalau tidak ada bisa dikosongi saja.

Kalau sudah, langsung simpan aja dulu (CTRL + S)

Untuk mengetes, buka terminal dan jalankan perintah ini

```markup
php artisan migrate
```

Perintah itu adalah untuk menjalankan perintah migration table users yang dibuat saat kita membuat authentication pada tutorial sebelumnya. Kalau setting koneksi database maka akan tampil seperti gambar di bawah ini.

![image-20220402110112781](img\image-20220402110112781.png)

### Setting Role User

Untuk membuat table di laravel kita bisa menggunakan yang namanya migrations, untuk lebih jelasnya bisa dibaca dulu di https://laravel.com/docs/9.x/migrations.

Setelah database dibuat, selanjutnya adalah membuat migrations untuk masing-masing table. Lokasi migrations ada di dalam folder **database -> migrations**. Migrations biasanya secara bawaan membuat nama tablenya dengan nama **s** dalam bahasa inggris, jadi biasanya diakhiri dengan huruf 's'. Sebagai contoh, table **users** adalah hasil dari migrations yang dibuat saat kita membuat authentication pada tutorial sebelumnya.

**# Migration users**

Sekarang kita fokus ke migrations yang belakangnya ada tulisan **create_users_table.php**, kita akan menyesuaikan field2nya sesuai dengan kebutuhan kita. Jadi langsung saja, buka file migrations yang belakangnya **create_users_table.php** (karena angka di depannya beda2, dibuat otomatis berdasarkan waktu kita membuat migrations tersebut) kemudian edit seperti di bawah ini.

![image-20220402112457349](img\image-20220402112457349.png)

```php+HTML
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('phone');//no tlp
            $table->text('alamat')->nullable();//alamat
            $table->string('role')->default('member');//member non admin
            $table->string('foto')->nullable();//foto profil
            $table->string('status')->default('aktif');//aktif atau non aktif, customer bisa kita nonaktifkan tanpa menghapus datanya
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }

}

```

Pada skrip di atas, kita mengubah dan menambahkan beberapa field diantaranya :

- `phone` : kita gunakan untuk menyimpan no tlp typenya varchar, soal string, increment dll bisa dibaca di dokumentasinya (https://laravel.com/docs/9.x/migrations)
- `alamat` : kita gunakan untuk menyimpan alamat typenya text
- `role` : kita gunakan untuk menyimpan keterangan dia sebagai admin atau member (customer)
- `foto` : kita gunakan untuk menyimpan link ke foto profilnya

**# Model User**

Setelah kita mengedit migrations, jangan lupa untuk mengedit modelnya. **Model** adalah file yang akan menjembatani interaksi kita dengan table di database. Sebagai contoh, kita akan melakukan input data ke table users maka kita membuat file model dengan nama **User.php** biar lebih mudah mengingatnya. Karena model **User** sudah dibuat secara otomatis saat membuat authentication, maka sekarang kita cukup mengeditnya aja biar sesuai dengan migration yang barusan kita buat.

Lokasi file model ada di dalam folder **app/Models**, cari dan buka file **User.php** kemudian kita edit seperti berikut pada bagian **fillable**-nya.

![image-20220402112919860](img\image-20220402112919860.png)

```php
protected $fillable = [
    'name',
    'email',
    'password',
    'phone',
    'alamat',
    'role',
    'foto',
    'status'
];
```

Kalau sudah, langsung kita simpan saja.

**# Migrate**

Kalau migration dan model sudah selesai kita edit, sekarang kita *migrate* (istilah untuk mengubah migrations menjadi table) dengan membuat terminal dan jalankan perintah berikut.

```markup
php artisan migrate
```

Prosesnya akan tampak seperti berikut.

![image-20220402113247930](img\image-20220402113247930.png)

Untuk migrate ada beberapa command atau perintah yang lainnya seperti

```markup
php artisan migrate:rollback
```

*Untuk menarik migrations yang barusan kita buat (otomatis menghapus table yang barusan dibuat)

```markup
php artisan migrate:refresh
```

*Untuk menjalankan migrate kembali setelah dilakukan kita mengedit file migrationsnya. Semisal kita menghapus atau menambahkan field maka kita harus menjalankan perintah tersebut untuk mengubah table di database.

Karena kita sudah pernah melakukan migrasi, maka kita tinggal menjalankan `php artisan migrate:refresh`. Dan hasilnya,

![image-20220402113457115](img\image-20220402113457115.png)

Untuk setting role user sampai disini, pada tutorial selanjutnya kita akan berkenalan dengan yang namanya seeder (https://laravel.com/docs/9.x/seeding#introduction)

### Database Seeder

**Database seeder** adalah proses melakukan test memasukkan data ke database dengan membuat class seed. Sebagai contoh kita akan melakukan test input data user yang telah kita buat sebelumnya. Buka terminal dan jalankan perintah berikut.

```markup
php artisan make:seeder UserSeeder
```

![image-20220402114030113](img\image-20220402114030113.png)

*Lokasi hasil pembuatan `class seed UserSeeder` di atas adalah di folder database -> seeds.

Sekarang buka file **UserSeeder.php** yang barusan kita buat, kemudian edit menjadi seperti berikut.

![image-20220402114125067](img\image-20220402114125067.png)

```php
<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $inputan['name'] = 'Jor Klowor';
        $inputan['email'] = 'jorkloworbanget@gmail.com';//ganti pake emailmu
        $inputan['password'] = Hash::make('password123');//passwordnya 123258
        $inputan['phone'] = '085852527575';
        $inputan['alamat'] = 'Kalibeber Rt 03 Rw 05';
        $inputan['role'] = 'admin';//kita akan membuat akun atau users in dengan role admin
        User::create($inputan);
    }
}

```

Setelah selesai kita edit, kemudian kita edit file **DatabaseSeeder.php** yang berada di folder seeds juga.

```php
<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(UserSeeder::class);
    }
}
?>
```

File **DatabaseSeeder** adalah file yang nantinya kita eksekusi untuk menjalankan seeder yang kita buat sebelumnya. Caranya adalah dengan memasukkan **nama class** seed ke `function run()`. Sebagai contoh, tadi kita membuat **class UserSeeder** maka kita masukkan ke `function run()` seperti skrip di atas.

Sekarang kita jalankan proses seeding dengan membuka terminal dan jalankan perintah ini.

```markup
php artisan db:seed
```

![image-20220402114844301](img\image-20220402114844301.png)

Untuk mengeceknya kita buka navicat dan cek table users, disitu akan ada 1 data sesuai yang kita buat di **UserSeeder.php** tadi.

![image-20220402114932834](img\image-20220402114932834.png)

Sampai disini untuk database seedernya, tutorial selanjutnya kita akan membuat proses registrasi member (customer)

### Register Customer

Table users sudah kita siapkan beserta modelnya, form register juga sudah kita siapkan pada tutorial sebelumnya. Karena pada tutorial sebelumnya form register belum ada kolom no telp, sekarang kita edit dulu file **register.blade.php** untuk menambahkan kolom **no telp**.

```php+HTML
@extends('layouts.template')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">{{ __('Register') }}</div>

                    <div class="card-body">
                        @if (count($errors) > 0)
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-warning">{{ $error }}</div>
                            @endforeach
                        @endif
                        @if ($message = Session::get('error'))
                            <div class="alert alert-warning">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        <form method="POST" action="{{ route('register') }}">
                            <div class="row">
                                <div class="col">
                                    @csrf
                                    <div class="form-group">
                                        <label for="name">Nama</label>
                                        <input type="text" name="name" id="name" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="text" name="email" id="email" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="phone">No tlp</label>
                                        <input type="text" name="phone" id="phone" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" name="password" id="password" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="password_confirmation">Confirm Password</label>
                                        <input type="password" name="password_confirmation" id="password_confirmation"
                                            class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary mb-4">Register</button>
                                        <p>Sudah punya akun? Login <a href="{{ route('login') }}"
                                                class="text-decoration-none">disini</a></p>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
```

No tlp dengan nama field phone sudah kita tambahkan di bawah kolom email, selanjutnya kita akan buka file RegisterController.php

**# Edit RegisterController.php**

Pada bagian **$redirectTo** nilainya kita ubah menjadi `'/admin'` dan **validator** dan **create** akan kita tambahkan **field phone (no tlp)**. Lengkapnya edit saja sama persis seperti kode di bawah ini.

```php
<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'phone' => ['required']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'phone' => $data['phone'], //tambahan kolom phone (no tlp)
        ]);
    }
    public function showRegistrationForm()
    {
        $data = array('title' => 'Register');
        return view('auth.register', $data);
    }
}
?>
```

Karena setelah proses register member atau customer otomatis masuk ke admin, maka kita sesuaikan menu sign out agar berfungsi untuk sing out (logout).

**# Edit menu dashboard**

Buka file **menudashboard.blade.php** dan edit pada bagian sign out menjadi seperti berikut.

```php+HTML
<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
            with font-awesome or any other icon font library -->
        <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-th"></i>
                <p>
                    Dashboard
                </p>
            </a>
        </li>
        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-folder-open"></i>
                <p>
                    Produk
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ route('produk.index') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Produk</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('kategori.index') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Kategori</p>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-shopping-cart"></i>
                <p>
                    Transaksi
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ route('transaksi.index') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Data Transaksi</p>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-folder"></i>
                <p>
                    Data
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ route('customer.index') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Customer</p>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-list"></i>
                <p>
                    Laporan
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ URL::to('admin/laporan') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Penjualan</p>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item">
            <a href="{{ URL::to('admin/profil') }}" class="nav-link">
                <i class="nav-icon fas fa-users"></i>
                <p>
                    Profil
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link" onclick="event.preventDefault();
                                                       document.getElementById('logout-form').submit();">
                <i class="nav-icon fas fa-sign-out-alt"></i>
                <p>
                    Sign Out
                </p>
            </a>
        </li>
    </ul>
</nav>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
</form>

```

**# Testing**

Sekarang coba lakukan registrasi, dan cek ke table users di database. Kalau berhasil, kemudian login menggunakan email dan password yang didaftarkan tadi.

![image-20220406081459813](img\image-20220406081459813.png)

![image-20220406081634110](img\image-20220406081634110.png)

Gambar di atas adalah respon ketika kita klik register tanpa mengisi kolom-kolomnya, maka peringatan akan tampil.

**# Batasi /admin**

Untuk membatasi akses ke halaman **/admin** kita bisa mengubahnya pada file **routes/web.php** dengan menambahkan **"middleware"** auth pada **route::group**. Lengkapnya seperti kode di bawah ini.

```php
<?php

use App\Http\Controllers\CustomerController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HomepageController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\LaporanController;
use App\Http\Controllers\LatihanController;
use App\Http\Controllers\ProdukController;
use App\Http\Controllers\TransaksiController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomepageController::class, 'index']);
Route::get('/about', [HomepageController::class, 'about']);
Route::get('/kontak', [HomepageController::class, 'kontak']);
Route::get('/kategori', [HomepageController::class, 'kategori']);
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::get('/', [DashboardController::class, 'index']);
    // route kategori
    Route::resource('kategori', KategoriController::class);
    // route produk
    Route::resource('produk', ProdukController::class);
    // route customer
    Route::resource('customer', CustomerController::class);
    // route transaksi
    Route::resource('transaksi', TransaksiController::class);
    // profil
    Route::get('profil', [UserController::class, 'index']);
    // setting profil
    Route::get('setting', [UserController::class, 'setting']);
    // form laporan
    Route::get('laporan', [LaporanController::class, 'index']);
    // proses laporan
    Route::get('proseslaporan', [LaporanController::class, 'proses']);
});

// Route::get('/', function () {
//     return view('welcome');
// });
// Route::get('/halo', function () {
//     return "Halo nama saya fadlur";
// });
// Route::get('/latihan', [LatihanController::class,'index']);
// Route::get('/blog/{id}', [LatihanController::class,'blog']);
// Route::get('/blog/{idblog}/komentar/{idkomentar}',[LatihanController::class,'komentar']);
// Route::get('/beranda', [LatihanController::class,'beranda']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

```

Simpan dan coba akses http://toko-klowor.local/admin maka user akan dikembalikan ke halaman login. Login dengan email dan password yang didaftarkan tadi. Tapi sebelumnya edit dulu file **LoginController.php**

**\# Edit LoginController.php**

Buka file **LoginController.php** dan pada bagian **$redirectTo** ubah nilainya menjadi `'/admin'` seperti kode di bawah ini.

```php+HTML
<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function showLoginForm()
    {
        $data = array('title' => 'Login');
        return view('auth.login', $data);
    }
}

```

Silahkan dicoba.

### Database Kategori

Toko yang kita buat mengelompokkan produknya berdasarkan kategorinya. Karena itu, setelah membuat table users sekarang kita membuat table kategori dan juga modelnya. Untuk model bisa dibaca lebih lengkapnya di https://laravel.com/docs/9.x/eloquent

Pada bagian ini kita akan membuat model kategori sekaligus migrationnya. Buka terminal dan jalankan perintah ini.

```php
php artisan make:model Kategori -m
```

Kemudian pencet enter. Setelah proses selesai, maka kita akan punya 2 buah file. 1 buah file migrations dan 1 buah file model.

**# Lengkapi migration kategori**

Pada folder migrations, buka file yang belakangnya ada **create_kategoris_table.php** kemudian lengkapi seperti kode di bawah ini.

```php
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKategorisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kategori', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_kategori');
            $table->string('nama_kategori');
            $table->string('slug_kategori');
            $table->text('deskripsi_kategori');
            $table->string('status');
            $table->string('foto')->nullable();//foto atau banner kategori
            $table->integer('user_id')->unsigned();//user yang menginput kategori
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kategori');
    }
}

```

Untuk nama tablenya kita sesuaikan menjadi *kategori.* Simpan dan kemudian jalankan proses migrate di terminal.

```php
php artisan migrate
```

**# Buat model Kategori**

Di dalam folder app, cari dan buka file model **Kategori.php**.

```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    use HasFactory;

    protected $table = 'kategori';
    protected $fillable = [
        'kode_kategori',
        'nama_kategori',
        'slug_kategori',
        'deskripsi_kategori',
        'status',
        'foto',
        'user_id',
    ];

    public function user()
    { //user yang menginput data kategori
        return $this->belongsTo(User::class, 'user_id');
    }
}

```

Keterangan :

- **protected $table** adalah variabel untuk mengatur nama table
- **protected $fillable** adalah variabel untuk mengatur field mana saja yang bisa diinput atau diisi
- **function user()** dengan return `$this->belongsTo(User::class, 'user_id')` adalah relasi table ke users dengan foreign key user_id. Dokumentasi bisa dibaca di https://laravel.com/docs/9.x/eloquent-relationships#updating-belongs-to-relationships

Untuk table kategori selesai kita buat, pada tutorial selanjutnya kita akan membuat table produk.

### Database Produk

Setelah kategori selesai kita buat tablenya, sekarang kita akan membuat model dan table produk. Buka terminal dan jalankan perintah ini.

```php
php artisan make:model Produk -m
```

Tekan enter dan tunggu proses selesai. Setelah proses selesai, maka akan ada 2 buah file baru. 1 buah file model **Produk.php** dan migration yang belakangnya **create_produks_table.php**

**# Lengkapi migration**

Buka file migration produk yang belakangnya **create_produks_table.php**, kemudian lengkapi seperti kode di bawah ini.

```php
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProduksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produk', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kategori_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('kode_produk');
            $table->string('nama_produk');
            $table->string('slug_produk');
            $table->text('deskripsi_produk');
            $table->string('foto')->nullable(); //banner produknya
            $table->double('qty', 12, 2)->default(0);
            $table->string('satuan');
            $table->double('harga', 12, 2)->default(0);
            $table->string('status');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('kategori_id')->references('id')->on('kategori');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produk');
    }
}

```

Jangan lupa, nama tablenya ganti menjadi *produk.* Baik itu di bagian `function up()` maupun di `function down()`.

**# Lengkapi model Produk**

Setelah migrations selesai kita edit, sekarang buka file model **Produk.php** dan lengkapi menjadi seperti kode di bawah ini.

```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    use HasFactory;

    protected $table = 'produk';
    protected $fillable = [
        'kategori_id',
        'user_id',
        'kode_produk',
        'nama_produk',
        'slug_produk',
        'deskripsi_produk',
        'foto',
        'qty',
        'satuan',
        'harga',
        'status',
    ];

    public function kategori() {
        return $this->belongsTo(Kategori::class, 'kategori_id');
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
}

```

**# Migrate**

Setelah migration dan model telah kita buat, sekarang kita migrate.

```php
php artisan migrate
```

Jangan lupa pencet enter. Kalau tidak ada error, maka sekarang ada table baru dengan nama produk. Silahkan cek di navicat.

Tutorial selanjutnya kita akan membuat produk CRUD kategori.