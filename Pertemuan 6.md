# CRUD Kategori

Desain dan model untuk kategori sudah kita buat, link ke dashboard kategori juga udah kita buat. Sekarang kita tinggal mengimplementasikan fungsi-fungsi seperti **input**, **edit**, **update** dan **hapus** kategorinya. Fungsi ini akan lebih sering kita sebut sebagai `CRUD (Create, Read, Update and Delete)`.

**# Index Kategori**

Kita harus mengedit function index di **KategoriController** seperti berikut.

```php
public function index()
{
    // kita ambil data kategori per halaman 20 data dan (paginate(20))
    // kita urutkan yang terakhir diiput yang paling atas (orderBy)
    $itemkategori = Kategori::orderBy('created_at', 'desc')->paginate(20);
    $data = array('title' => 'Kategori Produk',
                'itemkategori' => $itemkategori);
    return view('kategori.index', $data)->with('no', ($request->input('page', 1) - 1) * 20);
}
```

jika nama model(Kategori) seperti berikut, maka kita masukkan nama kelasnya kedalam kontrollernnya. 

![image-20220412104648621](img\image-20220412104648621.png)

Caranya dengan menekan **Ctrl + Spasi**, kemudian pilih nama kelas yg kita butuhkan.

![image-20220412104816322](img\image-20220412104816322.png)

nanti kodenya akan seperti berikut:

```php+HTML
<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use Illuminate\Http\Request;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
public function index(Request $request)
{
    // kita ambil data kategori per halaman 20 data dan (paginate(20))
    // kita urutkan yang terakhir diiput yang paling atas (orderBy)
    $itemkategori = Kategori::orderBy('created_at', 'desc')->paginate(20);
    $data = array('title' => 'Kategori Produk', 'itemkategori' => $itemkategori);
    return view('kategori.index', $data)->with('no', ($request->input('page', 1) - 1) * 20);
}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data = array('title' => 'Form Kategori');
        return view('kategori.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = array('title' => 'Form Edit Kategori');
        return view('kategori.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
```

Kemudian untuk file viewsnya, buka file **index.blade.php** di folder **kategori** dan edit menjadi seperti berikut.

```php+HTML
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <!-- table kategori -->
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Kategori Produk</h4>
                        <div class="card-tools">
                            <a href="{{ route('kategori.create') }}" class="btn btn-sm btn-primary">
                                Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="#">
                            <div class="row">
                                <div class="col">
                                    <input type="text" name="keyword" id="keyword" class="form-control"
                                        placeholder="ketik keyword disini">
                                </div>
                                <div class="col-auto">
                                    <button class="btn btn-primary">
                                        Cari
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-body">
                        @if ($message = Session::get('error'))
                            <div class="alert alert-warning">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th width="50px">No</th>
                                        <th>Gambar</th>
                                        <th>Kode</th>
                                        <th>Nama</th>
                                        <th>Jumlah Produk</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($itemkategori as $kategori)
                                        <tr>
                                            <td>
                                                {{ ++$no }}
                                            </td>
                                            <td>
                                                <img src="{{ asset('images/slide1.jpg') }}" alt="kategori 1"
                                                    width='150px'>
                                                <div class="row mt-2">
                                                    <div class="col">
                                                        <input type="file" name="gambar" id="gambar">
                                                    </div>
                                                    <div class="col-auto">
                                                        <button class="btn btn-sm btn-primary">Upload</button>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                {{ $kategori->kode_kategori }}
                                            </td>
                                            <td>
                                                {{ $kategori->nama_kategori }}
                                            </td>
                                            <td>
                                                {{$kategori->produk ? $kategori->produk()->count() : '0'}} Produk
                                            </td>
                                            <td>
                                                {{ $kategori->status }}
                                            </td>
                                            <td>
                                                <a href="{{ route('kategori.edit', $kategori->id) }}"
                                                    class="btn btn-sm btn-primary mr-2 mb-2">
                                                    Edit
                                                </a>
                                                <form action="{{ route('kategori.destroy', $kategori->id) }}"
                                                    method="post" style="display:inline;">
                                                    @csrf
                                                    {{ method_field('delete') }}
                                                    <button type="submit" class="btn btn-sm btn-danger mb-2">
                                                        Hapus
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <!-- untuk menampilkan link page, tambahkan skrip di bawah ini -->
                            {{ $itemkategori->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

```

Sekarang buka terminal dan jalankan aplikasi dengan perintah.

```markup
php artisan serve
```

Karena sudah kita batasi hanya user yang login yang bisa akses, sekarang login dulu kemudian akses ke halaman dashboard kategori. Tampilannya akan menjadi seperti berikut.

![image-20220412105439223](img\image-20220412105439223.png)

Tampilan dashboard kategori setelah diinput data. Selanjutnya akan kita buat function untuk menyimpan data kategori.

** Gambar kategori akan kita buatkan function upload nanti setelah crud produk*

**# Create Kategori**

Untuk membuat kategori baru, yang perlu kita edit adalah file **create.blade.php** yang juga ada di folder **kategori** menjadi seperti berikut.

```php+HTML
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Form Kategori</h3>
                        <div class="card-tools">
                            <a href="{{ route('kategori.index') }}" class="btn btn-sm btn-danger">
                                Tutup
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        @if (count($errors) > 0)
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-warning">{{ $error }}</div>
                            @endforeach
                        @endif
                        @if ($message = Session::get('error'))
                            <div class="alert alert-warning">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        <form action="{{ route('kategori.store') }}" method='POST'>
                            @csrf
                            <div class="form-group">
                                <label for="kode_kategori">Kode Kategori</label>
                                <input type="text" name="kode_kategori" id="kode_kategori" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="nama_kategori">Nama Kategori</label>
                                <input type="text" name="nama_kategori" id="nama_kategori" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="slug_kategori">Slug Kategori</label>
                                <input type="text" name="slug_kategori" id="slug_kategori" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="deskripsi_kategori">Deskripsi</label>
                                <textarea name="deskripsi_kategori" id="deskripsi_kategori" cols="30" rows="5" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <button type="reset" class="btn btn-warning">Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
```

Sedangkan untuk **KategoriController.php** fokus ke function **store**, yang tadinya kosong kemudian edit seperti berikut.

```php+HTML
<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use Illuminate\Http\Request;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // kita ambil data kategori per halaman 20 data dan (paginate(20))
        // kita urutkan yang terakhir diiput yang paling atas (orderBy)
        $itemkategori = Kategori::orderBy('created_at', 'desc')->paginate(20);
        $data = array('title' => 'Kategori Produk', 'itemkategori' => $itemkategori);
        return view('kategori.index', $data)->with('no', ($request->input('page', 1) - 1) * 20);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data = array('title' => 'Form Kategori');
        return view('kategori.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'kode_kategori' => 'required|unique:kategori',
            'nama_kategori' => 'required',
            'slug_kategori' => 'required',
            'deskripsi_kategori' => 'required',
        ]);
        $itemuser = $request->user(); //kita panggil data user yang sedang login
        $inputan = $request->all(); //kita masukkan semua variabel data yang diinput ke variabel $inputan
        $inputan['user_id'] = $itemuser->id;
        $inputan['slug_kategori'] = \Str::slug($request->slug_kategori); //kita buat slug biar pemisahnya menjadi strip (-)
        //slug kita gunakan nanti pas buka produk per kategori
        $inputan['status'] = 'publish'; //status kita set langsung publish saja
        $itemkategori = Kategori::create($inputan);
        return redirect()->route('kategori.index')->with('success', 'Data kategori berhasil disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = array('title' => 'Form Edit Kategori');
        return view('kategori.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
```

Kemudian kita tambahkan relasi antara **Kategori** dan **produk**, buka Model **Kategori** kemudian edit seperti berikut.

```php+HTML
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    use HasFactory;

    protected $table = 'kategori';
    protected $fillable = [
        'kode_kategori',
        'nama_kategori',
        'slug_kategori',
        'deskripsi_kategori',
        'status',
        'foto',
        'user_id',
    ];

    public function user()
    { //user yang menginput data kategori
        return $this->belongsTo(User::class, 'user_id');
    }

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'id', 'kategori_id');
    }
}
```

Sekarang kita coba input datanya, apabila masih ada kolom yang kosong akan ditampilkan error kalo ada yang kosong.

![image-20220412105916481](img\image-20220412105916481.png)

Tampilan kalau ada kolom yang belum diisi.

![image-20220412113231579](img\image-20220412113231579.png)

Tampilan kategori setelah diisi. Tombol edit dan hapus juga sudah kita buat.

**# Edit Kategori**

Untuk mengedit kita perlu mengedit form **edit.blade.php** di folder kategori menjadi seperti berikut.

```php+HTML
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Form Edit Kategori</h3>
                        <div class="card-tools">
                            <a href="{{ route('kategori.index') }}" class="btn btn-sm btn-danger">
                                Tutup
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        @if (count($errors) > 0)
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-warning">{{ $error }}</div>
                            @endforeach
                        @endif
                        @if ($message = Session::get('error'))
                            <div class="alert alert-warning">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        <form action="{{ route('kategori.update', $itemkategori->id) }}" method="post">
                            @csrf
                            {{ method_field('patch') }}
                            <div class="form-group">
                                <label for="kode_kategori">Kode Kategori</label>
                                <input type="text" name="kode_kategori" id="kode_kategori" class="form-control"
                                    value={{ $itemkategori->kode_kategori }}>
                            </div>
                            <div class="form-group">
                                <label for="nama_kategori">Nama Kategori</label>
                                <input type="text" name="nama_kategori" id="nama_kategori" class="form-control"
                                    value={{ $itemkategori->nama_kategori }}>
                            </div>
                            <div class="form-group">
                                <label for="slug_kategori">Slug Kategori</label>
                                <input type="text" name="slug_kategori" id="slug_kategori" class="form-control"
                                    value={{ $itemkategori->slug_kategori }}>
                            </div>
                            <div class="form-group">
                                <label for="deskripsi_kategori">Deskripsi</label>
                                <textarea name="deskripsi_kategori" id="deskripsi_kategori" cols="30" rows="5"
                                    class="form-control">{{ $itemkategori->deskripsi_kategori }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="status">Status</label>
                                <select name="status" id="status" class="form-control">
                                    <option value="publish" {{ $itemkategori->status == 'publish' ? 'selected' : '' }}>
                                        Publish</option>
                                    <option value="unpublish" {{ $itemkategori->status == 'unpublish' ? 'selected' : '' }}>
                                        Unpublish</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Update</button>
                                <button type="reset" class="btn btn-warning">Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
```

Selanjutnya buka **KategoriController.php** dan edit bagian function **edit** yang tadinya kosong menjadi seperti berikut.

```php
public function edit($id)
{
    $itemkategori = Kategori::findOrFail($id); //cari berdasarkan id = $id,
    // kalo ga ada error page not found 404
    $data = array(
        'title' => 'Form Edit Kategori',
        'itemkategori' => $itemkategori
    );
    return view('kategori.edit', $data);
}
```

Itu baru function untuk menampilkan data kategori yang akan diedit, selanjutnya buat function untuk mengupdate. Masih di **KategoriController.php**, fokus ke function **update**. Yang tadinya kosong sekarang kita ubah menjadi seperti berikut.

```php+HTML
public function update(Request $request, $id)
{
    $this->validate($request, [
        'nama_kategori' => 'required',
        'slug_kategori' => 'required',
        'deskripsi_kategori' => 'required',
    ]);
    $itemkategori = Kategori::findOrFail($id); //cari berdasarkan id = $id,
    // kalo ga ada error page not found 404
    $slug = \Str::slug($request->slug_kategori); //slug kita gunakan nanti pas buka produk per kategori
    // kita validasi dulu, biar tidak ada slug yang sama
    $validasislug = Kategori::where('id', '!=', $id) //yang id-nya tidak sama dengan $id
        ->where('slug_kategori', $slug)
        ->first();
    if ($validasislug) {
        return back()->with('error', 'Slug sudah ada, coba yang lain');
    } else {
        $inputan = $request->all();
        $inputan['slug'] = $slug;
        $itemkategori->update($inputan);
        return redirect()->route('kategori.index')->with('success', 'Data berhasil diupdate');
    }
}
```

Untuk mencobanya, pada halaman dashboard kategori. Klik tombol edit, kemudian ubah isi kolom pada form edit. Selanjutnya klik tombol update.

![image-20220412113823319](img\image-20220412113823319.png)

**# Delete Kategori**

Proses hapus kategori kita perlu mengedit function **delete** di dalam **KategoriController.php** menjadi seperti berikut.

```php+HTML
public function destroy($id)
{
    $itemkategori = Kategori::findOrFail($id); //cari berdasarkan id = $id,
    // kalo ga ada error page not found 404
    if (count($itemkategori->produk) > 0) {
        // dicek dulu, kalo ada produk di dalam kategori maka proses hapus dihentikan
        return back()->with('error', 'Hapus dulu produk di dalam kategori ini, proses dihentikan');
    } else {
        if ($itemkategori->delete()) {
            return back()->with('success', 'Data berhasil dihapus');
        } else {
            return back()->with('error', 'Data gagal dihapus');
        }
    }
}
```

![image-20220412114107009](img\image-20220412114107009.png)

Sekarang coba input data kategori, edit dan hapusnya.

**# File KategoriController.php**

File **KategoriController.php** setelah kita edit akan menjadi seperti berikut.

```php+HTML
<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use Illuminate\Http\Request;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // kita ambil data kategori per halaman 20 data dan (paginate(20))
        // kita urutkan yang terakhir diiput yang paling atas (orderBy)
        $itemkategori = Kategori::orderBy('created_at', 'desc')->paginate(20);
        $data = array('title' => 'Kategori Produk', 'itemkategori' => $itemkategori);
        return view('kategori.index', $data)->with('no', ($request->input('page', 1) - 1) * 20);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data = array('title' => 'Form Kategori');
        return view('kategori.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'kode_kategori' => 'required|unique:kategori',
            'nama_kategori' => 'required',
            'slug_kategori' => 'required',
            'deskripsi_kategori' => 'required',
        ]);
        $itemuser = $request->user(); //kita panggil data user yang sedang login
        $inputan = $request->all(); //kita masukkan semua variabel data yang diinput ke variabel $inputan
        $inputan['user_id'] = $itemuser->id;
        $inputan['slug_kategori'] = \Str::slug($request->slug_kategori); //kita buat slug biar pemisahnya menjadi strip (-)
        //slug kita gunakan nanti pas buka produk per kategori
        $inputan['status'] = 'publish'; //status kita set langsung publish saja
        $itemkategori = Kategori::create($inputan);
        return redirect()->route('kategori.index')->with('success', 'Data kategori berhasil disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $itemkategori = Kategori::findOrFail($id); //cari berdasarkan id = $id,
        // kalo ga ada error page not found 404
        $data = array(
            'title' => 'Form Edit Kategori',
            'itemkategori' => $itemkategori
        );
        return view('kategori.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_kategori' => 'required',
            'slug_kategori' => 'required',
            'deskripsi_kategori' => 'required',
        ]);
        $itemkategori = Kategori::findOrFail($id); //cari berdasarkan id = $id,
        // kalo ga ada error page not found 404
        $slug = \Str::slug($request->slug_kategori); //slug kita gunakan nanti pas buka produk per kategori
        // kita validasi dulu, biar tidak ada slug yang sama
        $validasislug = Kategori::where('id', '!=', $id) //yang id-nya tidak sama dengan $id
            ->where('slug_kategori', $slug)
            ->first();
        if ($validasislug) {
            return back()->with('error', 'Slug sudah ada, coba yang lain');
        } else {
            $inputan = $request->all();
            $inputan['slug'] = $slug;
            $itemkategori->update($inputan);
            return redirect()->route('kategori.index')->with('success', 'Data berhasil diupdate');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
public function destroy($id)
{
    $itemkategori = Kategori::findOrFail($id); //cari berdasarkan id = $id,
    // kalo ga ada error page not found 404
    if ($itemkategori->produk()->count() > 0) {
        // dicek dulu, kalo ada produk di dalam kategori maka proses hapus dihentikan
        return back()->with('error', 'Hapus dulu produk di dalam kategori ini, proses dihentikan');
    } else {
        if ($itemkategori->delete()) {
            return back()->with('success', 'Data berhasil dihapus');
        } else {
            return back()->with('error', 'Data gagal dihapus');
        }
    }
}
}
```

### Database Image

Data foto produk dan foto kategori semuanya akan disimpan dalam dengan nama images. Untuk file fotonya akan diupload ke folder **storage**. Sekarang kita buat model dulu beserta migrationnya dengan perintah berikut.

```php
php artisan make:model Image -m
```

Kemudian tekan enter, tunggu beberapa saat sampai proses selesai.

Setelah selesai, ada 2 file yaitu **Image.php** di folder app sebagai model dan file migration dengan akhiran **create_images_table.php**.

**# Migrations**

Buka file migrations dengan akhiran **create_images_table.php**, kemudian ubah seperti kode berikut.

```php+HTML
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('url');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}
```

**# Model Image**

Setelah migrations, selanjutnya kita edit file model **Image** dengan kode berikut.

```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'images';
    protected $fillable = [
        'user_id',
        'url',
    ];

    public function user()
    { //user yang menginput data image
        return $this->belongsTo('App\User', 'user_id');
    }
}
```

**# Migrate**

Setelah selesai edit kedua file tersebut, sekarang jalankan perintah migrate.

```
php artisan migrate
```

kemudian tekan enter.

Akan ada tambahan 1 buah table dengan nama images di database. Pada tutorial selanjutnya kita akan membuat controller untuk mengetes fungsi upload images.

![image-20220412114808674](img\image-20220412114808674.png)

### Upload Image

Untuk membuat fungsi upload image, kita harus membuat link storage dulu di aplikasi laravel kita. Sekarang buka terminal dan jalankan perintah berikut.

```php
php artisan storage:link
```

Kemudian tekan enter.

![image-20220412114843998](img\image-20220412114843998.png)

Lebih lengkapnya bisa dibaca di dokumentasinya (https://laravel.com/docs/9.x/filesystem#introduction)

**# Setting**

Agar bisa berjalan dengan sepenuhnya, kita perlu melakukan beberapa setting.

 **.env**

Buka file **.env** kemudian cari bagian **APP_URL**, ubah nilainya menjadi seperti berikut.

```php
APP_URL=http://localhost:8000
```

**filesystems.php**

Buka file **filesystems.php** di dalam folder **config**, kemudian ubah bagian default menjadi '**public**'

```php
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'public'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "sftp", "s3", "rackspace"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL') . '/storage',
            'visibility' => 'public',
        ],

        's3' => [
            'driver' => 's3',
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET'),
            'url' => env('AWS_URL'),
        ],

    ],

];

```

**# ImageController**

Kembali ke terminal lagi dan buat file **ImageController.php** dengan mengetik perintah berikut.

```markup
php artisan make:controller ImageController
```

kemudian tekan enter.

Buka file **ImageController.php** dan isi dengan skrip berikut.

```php+HTML
<?php

namespace App\Http\Controllers;

use App\Models\Image;
use Illuminate\Http\Request;

class ImageController extends Controller
{
     public function index(Request $request) {
        $itemuser = $request->user();
        $itemgambar = Image::where('user_id', $itemuser->id)->paginate(20);
        $data = array('title' => 'Data Image',
                    'itemgambar' => $itemgambar);
        return view('image.index', $data)->with('no', ($request->input('page', 1) - 1) * 20);
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $itemuser = $request->user();
        $fileupload = $request->file('image');
        $folder = $request->file('image')->store('public/images');
        $itemgambar = $this->upload($fileupload, $itemuser, $folder);
        // $inputan = $request->all();
        // $inputan['user_id'] = $itemuser->id;
        // Image::create($inputan);
        return back()->with('success', 'Image berhasil diupload');
    }

    public function destroy(Request $request, $id) {
        $itemuser = $request->user();
        $itemgambar = Image::where('user_id', $itemuser->id)
                            ->where('id', $id)
                            ->first();
        if ($itemgambar) {
            \Storage::delete($itemgambar->url);
            $itemgambar->delete();
            return back()->with('success', 'Data berhasil dihapus');
        } else {
            return back()->with('error', 'Data tidak ditemukan');
        }
    }

    public function upload($fileupload, $itemuser, $folder) {
        $folder = 'assets/images';
        $path = $fileupload->store('images');
        $inputangambar['url'] = $path;
        $inputangambar['user_id'] = $itemuser->id;
        return Image::create($inputangambar);
    }
}
```

**# views image**

Setelah controller selesai kita buat, selanjutnya buat 1 buah folder dengan nama **image** di dalam views. Kemudian di dalam folder **image** buat 1 buah file dengan nama **index.blade.php** dan isi dengan skrip berikut.

```php+HTML
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Image</h3>
                    </div>
                    <div class="card-body">
                        <form action="{{ url('/admin/image') }}" method="post" enctype="multipart/form-data"
                            class="form-inline">
                            @csrf
                            <div class="form-group">
                                <input type="file" name="image" id="image">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary">Upload</button>
                            </div>
                        </form>
                    </div>
                    <div class="card-body">
                        @if (count($errors) > 0)
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-warning">{{ $error }}</div>
                            @endforeach
                        @endif
                        @if ($message = Session::get('error'))
                            <div class="alert alert-warning">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        <div class="row">
                            @foreach ($itemgambar as $gambar)
                                <div class="col col-lg-3 col-md-3 mb-2">
                                    <img src="{{ \Storage::url($gambar->url) }}" alt="img" class="img-thumbnail mb-2">
                                    <form action="{{ url('/admin/image/' . $gambar->id) }}" method="post"
                                        style="display:inline;">
                                        @csrf
                                        {{ method_field('delete') }}
                                        <button type="submit" class="btn btn-sm btn-danger mb-2">
                                            Hapus
                                        </button>
                                    </form>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
```

**# route**

Kita perlu menambahkan beberapa route di file **routes/web.php** untuk fungsi upload image.

```php
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::get('/', [DashboardController::class, 'index']);
    // route kategori
    Route::resource('kategori', KategoriController::class);
    // route produk
    Route::resource('produk', ProdukController::class);
    // route customer
    Route::resource('customer', CustomerController::class);
    // route transaksi
    Route::resource('transaksi', TransaksiController::class);
    // profil
    Route::get('profil', [UserController::class, 'index']);
    // setting profil
    Route::get('setting', [UserController::class, 'setting']);
    // form laporan
    Route::get('laporan', [LaporanController::class, 'index']);
    // proses laporan
    Route::get('proseslaporan', [LaporanController::class, 'proses']);

    // image
    Route::get('image', [ImageController::class, 'index']);
    // simpan image
    Route::post('image', [ImageController::class, 'store']);
    // hapus image by id
    Route::delete('image/{id}', [ImageController::class, 'destroy']);
});
```

**# Trial**

Karena kita tidak menambahkan menu upload image di menu dashboard, maka sekarang jalankan aplikasi kita kemudian akses halaman http://toko-klowor.local/admin/image

![image-20220412120144897](img\image-20220412120144897.png)

Coba upload beberapa gambar dan hapus lagi. Pastikan tidak ada error.