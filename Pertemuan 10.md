### Add to Cart

Untuk membuat `proses add to cart` kita perlu membuat 2 buah table yaitu **cart** dan **cart_detail**. Cart menyimpan ringkasan transaksi seperti **no_invoice**, **total** dan lain-lainnya, sedangkan `cart_detail` menyimpan item apa saja yang dimasukkan ke shopping cart.

Langkah-langkah untuk membuat fungsi shopping cart :

1. Buat **migrations cart, model dan controllernya**
2. Buat **migrations cart_detail, model dan controllernya**
3. Edit button "Tambahkan ke keranjang" di **produkdetail.blade.php**
4. Buat file views **cart/index.blade.php**
5. Tambahkan **route cart dan cart detail ke web.php**

Sekarang langsung kita buat migrations cart, model dan controllernya. Buka terminal dan jalankan perintah berikut dan tekan enter.

```php
php artisan make:model Cart -crm
```

Setelah kita proses selesai akan ada 3 buah file baru yaitu migrations, model dan controller.

![image-20220421130418341](img\image-20220421130418341.png)

**#1. Koreksi file migrations cart, model dan controllernya.**

Buka file **_create_carts_table.php** dan koreksi menjadi seperti berikut.

```php
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('no_invoice');
            $table->string('status_cart'); // ada 2 yaitu cart, checkout
            $table->string('status_pembayaran'); // ada 2 sudah dan belum
            $table->string('status_pengiriman'); // ada 2 yaitu belum dan sudah
            $table->string('no_resi')->nullable();
            $table->string('ekspedisi')->nullable();
            $table->double('subtotal', 12, 2)->default(0);
            $table->double('ongkir', 12, 2)->default(0);
            $table->double('diskon', 12, 2)->default(0);
            $table->double('total', 12, 2)->default(0);
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart');
    }
}
```

Buka file model **Cart.php** dan koreksi menjadi seperti berikut.

```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table = 'cart';
    protected $fillable = [
        'user_id',
        'no_invoice',
        'status_cart',
        'status_pembayaran',
        'status_pengiriman',
        'no_resi',
        'ekspedisi',
        'subtotal',
        'ongkir',
        'diskon',
        'total',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function detail()
    {
        return $this->hasMany(CartDetail::class, 'cart_id');
    }

    public function updatetotal($itemcart, $subtotal)
    {
        $this->attributes['subtotal'] = $itemcart->subtotal + $subtotal;
        $this->attributes['total'] = $itemcart->total + $subtotal;
        self::save();
    }
}
```

Terakhir untuk cart, kita edit file controller **CartController.php**

```php
<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $itemuser = $request->user(); //ambil data user
        $itemcart = Cart::where('user_id', $itemuser->id)
            ->where('status_cart', 'cart')
            ->first();
        $data = array(
            'title' => 'Shopping Cart',
            'itemcart' => $itemcart
        );
        return view('cart.index', $data)->with('no', 1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function kosongkan($id)
    {
        $itemcart = Cart::findOrFail($id);
        $itemcart->detail()->delete(); //hapus semua item di cart detail
        $itemcart->updatetotal($itemcart, '-' . $itemcart->subtotal);
        return back()->with('success', 'Cart berhasil dikosongkan');
    }
}
```

Untuk **Cart** sudah selesai, selanjutnya kita akan membuat migration, model dan controller cart_detail. Buka kembali terminal dan jalankan perintah berikut dan tekan enter.

```php
php artisan make:model CartDetail -crm
```

![image-20220421131431514](img\image-20220421131431514.png)

**#2. Koreksi file migrations, model dan controller Cart Detail**

Buka file **_create_cart_details_table.php** dan ubah menjadi seperti berikut.

```php
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produk_id')->unsigned();
            $table->integer('cart_id')->unsigned();
            $table->double('qty', 12, 2)->default(0);
            $table->double('harga', 12, 2)->default(0);
            $table->double('diskon', 12, 2)->default(0);
            $table->double('subtotal', 12, 2)->default(0);
            $table->foreign('cart_id')->references('id')->on('cart');
            $table->foreign('produk_id')->references('id')->on('produk');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_detail');
    }
}
```

Selesai dengan migrations, selanjutnya buka file model **CartDetail.php** dan ubah menjadi berikut.

```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CartDetail extends Model
{
    protected $table = 'cart_detail';
    protected $fillable = [
        'produk_id',
        'cart_id',
        'qty',
        'harga',
        'diskon',
        'subtotal',
    ];

    public function cart() {
        return $this->belongsTo(Cart::class, 'cart_id');
    }

    public function produk() {
        return $this->belongsTo(Produk::class, 'produk_id');
    }

    // function untuk update qty, sama subtotal
    public function updatedetail($itemdetail, $qty, $harga, $diskon) {
        $this->attributes['qty'] = $itemdetail->qty + $qty;
        $this->attributes['subtotal'] = $itemdetail->subtotal + ($qty * ($harga - $diskon));
        self::save();
    }
}
```

Setelah model Cart Detail, kemudian edit file **CartDetailController.php**

```php
<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\CartDetail;
use App\Models\Produk;
use Illuminate\Http\Request;

class CartDetailController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return abort('404');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'produk_id' => 'required',
        ]);
        $itemuser = $request->user();
        $itemproduk = Produk::findOrFail($request->produk_id);
        // cek dulu apakah sudah ada shopping cart untuk user yang sedang login
        $cart = Cart::where('user_id', $itemuser->id)
            ->where('status_cart', 'cart')
            ->first();

        if ($cart) {
            $itemcart = $cart;
        } else {
            $no_invoice = Cart::where('user_id', $itemuser->id)->count();
            //nyari jumlah cart berdasarkan user yang sedang login untuk dibuat no invoice
            $inputancart['user_id'] = $itemuser->id;
            $inputancart['no_invoice'] = 'INV ' . str_pad(($no_invoice + 1), '3', '0', STR_PAD_LEFT);
            $inputancart['status_cart'] = 'cart';
            $inputancart['status_pembayaran'] = 'belum';
            $inputancart['status_pengiriman'] = 'belum';
            $itemcart = Cart::create($inputancart);
        }
        // cek dulu apakah sudah ada produk di shopping cart
        $cekdetail = CartDetail::where('cart_id', $itemcart->id)
            ->where('produk_id', $itemproduk->id)
            ->first();
        $qty = 1; // diisi 1, karena kita set ordernya 1
        $harga = $itemproduk->harga; //ambil harga produk
        $diskon = $itemproduk->promo != null ? $itemproduk->promo->diskon_nominal : 0;
        $subtotal = ($qty * $harga) - $diskon;
        // diskon diambil kalo produk itu ada promo, cek materi sebelumnya
        if ($cekdetail) {
            // update detail di table cart_detail
            $cekdetail->updatedetail($cekdetail, $qty, $harga, $diskon);
            // update subtotal dan total di table cart
            $cekdetail->cart->updatetotal($cekdetail->cart, $subtotal);
        } else {
            $inputan = $request->all();
            $inputan['cart_id'] = $itemcart->id;
            $inputan['produk_id'] = $itemproduk->id;
            $inputan['qty'] = $qty;
            $inputan['harga'] = $harga;
            $inputan['diskon'] = $diskon;
            $inputan['subtotal'] = ($harga * $qty) - $diskon;
            $itemdetail = CartDetail::create($inputan);
            // update subtotal dan total di table cart
            $itemdetail->cart->updatetotal($itemdetail->cart, $subtotal);
        }
        return redirect()->route('cart.index')->with('success', 'Produk berhasil ditambahkan ke cart');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CartDetail  $cartDetail
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CartDetail  $cartDetail
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CartDetail  $cartDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $itemdetail = CartDetail::findOrFail($id);
        $param = $request->param;

        if ($param == 'tambah') {
            // update detail cart
            $qty = 1;
            $itemdetail->updatedetail($itemdetail, $qty, $itemdetail->harga, $itemdetail->diskon);
            // update total cart
            $itemdetail->cart->updatetotal($itemdetail->cart, ($itemdetail->harga - $itemdetail->diskon));
            return back()->with('success', 'Item berhasil diupdate');
        }
        if ($param == 'kurang') {
            // update detail cart
            $qty = 1;
            $itemdetail->updatedetail($itemdetail, '-' . $qty, $itemdetail->harga, $itemdetail->diskon);
            // update total cart
            $itemdetail->cart->updatetotal($itemdetail->cart, '-' . ($itemdetail->harga - $itemdetail->diskon));
            return back()->with('success', 'Item berhasil diupdate');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CartDetail  $cartDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $itemdetail = CartDetail::findOrFail($id);
        // update total cart dulu
        $itemdetail->cart->updatetotal($itemdetail->cart, '-' . $itemdetail->subtotal);
        if ($itemdetail->delete()) {
            return back()->with('success', 'Item berhasil dihapus');
        } else {
            return back()->with('error', 'Item gagal dihapus');
        }
    }
}
```

**#3. Edit file produkdetail.blade.php**

Agar bisa menambahkan produk ke shopping cart, edit tombol **"tambahkan ke keranjang"**. Buka file **produkdetail.blade.php** dan koreksi bagian **"Tambahkan ke keranjang"**.

```php+HTML
@extends('layouts.template')
@section('content')
    <div class="container">
        <div class="row mt-4">
            <div class="col col-lg-8 col-md-8">
                <div id="carousel" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        @foreach ($itemproduk->images as $index => $image)
                            @if ($index == 0)
                                <div class="carousel-item active">
                                    <img src="{{ \Storage::url($image->foto) }}" class="d-block w-100" alt="...">
                                </div>
                            @else
                                <div class="carousel-item">
                                    <img src="{{ \Storage::url($image->foto) }}" class="d-block w-100" alt="...">
                                </div>
                            @endif
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <!-- deskripsi produk -->
            <div class="col col-lg-4 col-md-4">
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-body">
                                @if (count($errors) > 0)
                                    @foreach ($errors->all() as $error)
                                        <div class="alert alert-warning">{{ $error }}</div>
                                    @endforeach
                                @endif
                                @if ($message = Session::get('error'))
                                    <div class="alert alert-warning">
                                        <p>{{ $message }}</p>
                                    </div>
                                @endif
                                @if ($message = Session::get('success'))
                                    <div class="alert alert-success">
                                        <p>{{ $message }}</p>
                                    </div>
                                @endif
                                <span class="small">{{ $itemproduk->kategori->nama_kategori }}</span>
                                <h5>{{ $itemproduk->nama_produk }}</h5>
                                <!-- cek apakah ada promo -->
                                @if ($itemproduk->promo != null)
                                    <p>
                                        Rp. <del>{{ number_format($itemproduk->promo->harga_awal, 2) }}</del>
                                        <br />
                                        Rp. {{ number_format($itemproduk->promo->harga_akhir, 2) }}
                                    </p>
                                @else
                                    <p>
                                        Rp. {{ number_format($itemproduk->harga, 2) }}
                                    </p>
                                @endif
                                <form action="{{ route('wishlist.store') }}" method="post">
                                    @csrf
                                    <input type="hidden" name="produk_id" value={{ $itemproduk->id }}>
                                    <button type="submit" class="btn btn-sm btn-outline-secondary">
                                        @if (isset($itemwishlist) && $itemwishlist)
                                            <i class="fas fa-heart"></i> Tambah ke wishlist
                                        @else
                                            <i class="far fa-heart"></i> Tambah ke wishlist
                                        @endif
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col">
                        <div class="card">
                            <div class="card-body">
                                <form action="{{ route('cartdetail.store') }}" method="POST">
                                    @csrf
                                    <input type="hidden" name="produk_id" value={{ $itemproduk->id }}>
                                    <button class="btn btn-block btn-primary" type="submit">
                                        <i class="fa fa-shopping-cart"></i> Tambahkan Ke Keranjang
                                    </button>
                                </form>
                                <button class="btn btn-block btn-danger mt-4">
                                    <i class="fa fa-shopping-basket"></i> Beli Sekarang
                                </button>
                            </div>
                            <div class="card-footer">
                                <div class="row mt-4">
                                    <div class="col text-center">
                                        <i class="fa fa-truck-moving"></i>
                                        <p>Pengiriman Cepat</p>
                                    </div>
                                    <div class="col text-center">
                                        <i class="fa fa-calendar-week"></i>
                                        <p>Garansi 7 hari</p>
                                    </div>
                                    <div class="col text-center">
                                        <i class="fa fa-money-bill"></i>
                                        <p>Pembayaran Aman</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        Deskripsi
                    </div>
                    <div class="card-body">
                        {{ $itemproduk->deskripsi_produk }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
```

Setelah sudah, kita perlu membuat 1 buah file views lagi untuk halaman cartnya.

**#4. Buat 1 buah file views cart**

Untuk menampilkan shopping cart, kita harus membuat 1 buah file views. Di dalam folder views buat folder **cart**, kemudian di dalam folder **cart** buat 1 buah file **index.blade.php**

![image-20220421131946218](img\image-20220421131946218.png)

```php+HTML
@extends('layouts.template')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col col-md-8">
                @if (count($errors) > 0)
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-warning">{{ $error }}</div>
                    @endforeach
                @endif
                @if ($message = Session::get('error'))
                    <div class="alert alert-warning">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        Item
                    </div>
                    <div class="card-body">
                        <table class="table table-stripped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Produk</th>
                                    <th>Harga</th>
                                    <th>Diskon</th>
                                    <th>Qty</th>
                                    <th>Subtotal</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($itemcart->detail as $detail)
                                    <tr>
                                        <td>
                                            {{ $no++ }}
                                        </td>
                                        <td>
                                            {{ $detail->produk->nama_produk }}
                                            <br />
                                            {{ $detail->produk->kode_produk }}
                                        </td>
                                        <td>
                                            {{ number_format($detail->harga, 2) }}
                                        </td>
                                        <td>
                                            {{ number_format($detail->diskon, 2) }}
                                        </td>
                                        <td>
                                            <div class="btn-group" role="group">
                                                <form action="{{ route('cartdetail.update', $detail->id) }}" method="post">
                                                    @method('patch')
                                                    @csrf()
                                                    <input type="hidden" name="param" value="kurang">
                                                    <button class="btn btn-primary btn-sm">
                                                        -
                                                    </button>
                                                </form>
                                                <button class="btn btn-outline-primary btn-sm" disabled="true">
                                                    {{ number_format($detail->qty, 2) }}
                                                </button>
                                                <form action="{{ route('cartdetail.update', $detail->id) }}"
                                                    method="post">
                                                    @method('patch')
                                                    @csrf()
                                                    <input type="hidden" name="param" value="tambah">
                                                    <button class="btn btn-primary btn-sm">
                                                        +
                                                    </button>
                                                </form>
                                            </div>
                                        </td>
                                        <td>
                                            {{ number_format($detail->subtotal, 2) }}
                                        </td>
                                        <td>
                                            <form action="{{ route('cartdetail.destroy', $detail->id) }}" method="post"
                                                style="display:inline;">
                                                @csrf
                                                {{ method_field('delete') }}
                                                <button type="submit" class="btn btn-sm btn-danger mb-2">
                                                    Hapus
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col col-md-4">
                <div class="card">
                    <div class="card-header">
                        Ringkasan
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <tr>
                                <td>No Invoice</td>
                                <td class="text-right">
                                    {{ $itemcart->no_invoice }}
                                </td>
                            </tr>
                            <tr>
                                <td>Subtotal</td>
                                <td class="text-right">
                                    {{ number_format($itemcart->subtotal, 2) }}
                                </td>
                            </tr>
                            <tr>
                                <td>Diskon</td>
                                <td class="text-right">
                                    {{ number_format($itemcart->diskon, 2) }}
                                </td>
                            </tr>
                            <tr>
                                <td>Total</td>
                                <td class="text-right">
                                    {{ number_format($itemcart->total, 2) }}
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col">
                                <button class="btn btn-primary btn-block">Checkout</button>
                            </div>
                            <div class="col">
                                <form action="{{ url('kosongkan') . '/' . $itemcart->id }}" method="post">
                                    @method('patch')
                                    @csrf()
                                    <button type="submit" class="btn btn-danger btn-block">Kosongkan</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
```

**#5. Tambahkan route untuk shopping cart**

Di dalam shopping **cart**, kita dapat mengupdate qty item dan juga mengosongkan cart. Oleh karena itu kita perlu menambahkan route ke file web.php

Buka file **web.php** dan tambahkan baris baru seperti berikut.

```php
    <?php

    use App\Http\Controllers\CartController;
    use App\Http\Controllers\CartDetailController;
    use App\Http\Controllers\CustomerController;
    use App\Http\Controllers\DashboardController;
    use App\Http\Controllers\HomepageController;
    use App\Http\Controllers\ImageController;
    use App\Http\Controllers\KategoriController;
    use App\Http\Controllers\LaporanController;
    use App\Http\Controllers\LatihanController;
    use App\Http\Controllers\ProdukController;
    use App\Http\Controllers\ProdukPromoController;
    use App\Http\Controllers\SlideshowController;
    use App\Http\Controllers\TransaksiController;
    use App\Http\Controllers\UserController;
    use App\Http\Controllers\WishlistController;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Route;

    /*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register web routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | contains the "web" middleware group. Now create something great!
    |
    */

    Route::get('/', [HomepageController::class, 'index']);
    Route::get('/about', [HomepageController::class, 'about']);
    Route::get('/kontak', [HomepageController::class, 'kontak']);
    Route::get('/kategori', [HomepageController::class, 'kategori']);
    Route::get('/kategori/{slug}', [HomepageController::class, 'kategoribyslug']);
    Route::get('/produk', [HomepageController::class, 'produk']);
    Route::get('produk/{id}', [HomepageController::class, 'produkdetail']);

    Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
        Route::get('/', [DashboardController::class, 'index']);
        // route kategori
        Route::resource('kategori', KategoriController::class);
        // route produk
        Route::resource('produk', ProdukController::class);
        // route customer
        Route::resource('customer', CustomerController::class);
        // route transaksi
        Route::resource('transaksi', TransaksiController::class);
        // profil
        Route::get('profil', [UserController::class, 'index']);
        // setting profil
        Route::get('setting', [UserController::class, 'setting']);
        // form laporan
        Route::get('laporan', [LaporanController::class, 'index']);
        // proses laporan
        Route::get('proseslaporan', [LaporanController::class, 'proses']);

        // image
        Route::get('image', [ImageController::class, 'index']);
        // simpan image
        Route::post('image', [ImageController::class, 'store']);
        // hapus image by id
        Route::delete('image/{id}', [ImageController::class, 'destroy']);


        // upload image kategori
        Route::post('imagekategori', [KategoriController::class, 'uploadimage']);
        // hapus image kategori
        Route::delete('imagekategori/{id}', [KategoriController::class, 'deleteimage']);

        // upload image produk
        Route::post('produkimage', [ProdukController::class, 'uploadimage']);
        // hapus image produk
        Route::delete('produkimage/{id}', [ProdukController::class, 'deleteimage']);

        // hapus image produk
        Route::delete('produkimage/{id}', [ProdukController::class, 'deleteimage']);
        // slideshow
        Route::resource('slideshow', SlideshowController::class);

        // produk promo
        Route::resource('promo', ProdukPromoController::class);
        // load async produk
        Route::get('loadprodukasync/{id}', [ProdukController::class, 'loadasync']);
        // wishlist
        Route::resource('wishlist', WishlistController::class);
    });
    // shopping cart
    Route::group(['middleware' => 'auth'], function () {
        // cart
        Route::resource('cart', CartController::class);
        Route::patch('kosongkan/{id}', [CartController::class, 'kosongkan']);
        // cart detail
        Route::resource('cartdetail', CartDetailController::class);
    });

    // Route::get('/', function () {
    //     return view('welcome');
    // });
    // Route::get('/halo', function () {
    //     return "Halo nama saya fadlur";
    // });
    // Route::get('/latihan', [LatihanController::class,'index']);
    // Route::get('/blog/{id}', [LatihanController::class,'blog']);
    // Route::get('/blog/{idblog}/komentar/{idkomentar}',[LatihanController::class,'komentar']);
    // Route::get('/beranda', [LatihanController::class,'beranda']);

    Auth::routes();
    Route::get('/home', function () {
        return redirect('/admin');
    });
```

Sampai disini, sudah selesai proses untuk shopping cart. Pada materi selanjutnya kita akan menambahkan fungsi untuk checkout.

Jangan lupa jalankan `php artisan migrate` di terminal(cmd)

![image-20220421132443506](img\image-20220421132443506.png)

![image-20220421132508727](img\image-20220421132508727.png)

### Checkout

Setelah item dimasukkan ke shopping cart, selanjutnya user harus menginput alamat pengiriman dan mengesetnya sebagai alamat pengiriman utama untuk membuat order. Jadi pada bagian ini nantinya akan ada 2 buah table baru dan secara otomatis 2 model baru juga, yaitu **AlamatPengiriman** dan **Order**.

Langkah-langkahnya adalah seperti berikut:

1. Koreksi migrations dan model **Alamat Pengiriman**
2. Koreksi Controller alamat pengiriman
3. Buat views **alamat pengiriman**
4. Buat dan Koreksi migrations dan model **Order**
5. Buat views **checkout**
6. Update **CartController**
7. Update `function index TransaksiController`
8. Update button checkout pada views **cart/index.blade.php**
9. Update views **transaksi/index.blade.php**
10. Tambahkan route alamat pengiriman ke **web.php**

Buka terminal dan jalankan perintah berikut kemudian tekan enter.

```php
php artisan make:model AlamatPengiriman -crm
```

![image-20220421133814112](img\image-20220421133814112.png)

Setelah dibuat akan akan ada 3 buah file baru. Sekarang kita eksekusi langkah yang pertama.

**\#1. Koreksi migrations dan model Alamat Pengiriman**

Buka file **_create_alamat_pengirimen_table.php** *(info: karena migrations menggunaan plural dalam bahasa inggris, maka yang tadinya pengiriman dibuat pengirimen)* dan update seperti berikut.

```php
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlamatPengirimenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alamat_pengiriman', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('status'); //utama atau tidak
            $table->string('nama_penerima');
            $table->string('no_tlp');
            $table->text('alamat');
            $table->string('provinsi');
            $table->string('kota');
            $table->string('kecamatan');
            $table->string('kelurahan');
            $table->string('kodepos');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alamat_pengiriman');
    }
}
```

Sedangkan untuk model **AlamatPengiriman.php** buat menjadi seperti berikut.

```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AlamatPengiriman extends Model
{
    protected $table = 'alamat_pengiriman';
    protected $fillable = [
        'user_id',
        'status',
        'nama_penerima',
        'no_tlp',
        'alamat',
        'provinsi',
        'kota',
        'kecamatan',
        'kelurahan',
        'kodepos',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}

```

**#2. Controller AlamatPengirimanController.php**

Pada controller **AlamatPengirimanController.php** hanya akan ada 3 function yang dipake, yaitu `index`, `store` (untuk menyimpan) dan `update` (untuk mengupdate status menjadi alamat utama).

Buka file **AlamatPengirimanController.php** dan update kodenya menjadi seperti berikut.

```php
<?php

namespace App\Http\Controllers;

use App\Models\AlamatPengiriman;
use Illuminate\Http\Request;

class AlamatPengirimanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $itemuser = $request->user();
        $itemalamatpengiriman = AlamatPengiriman::where('user_id', $itemuser->id)->paginate(10);
        $data = array(
            'title' => 'Alamat Pengiriman',
            'itemalamatpengiriman' => $itemalamatpengiriman
        );
        return view('alamatpengiriman.index', $data)->with('no', ($request->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_penerima' => 'required',
            'no_tlp' => 'required',
            'alamat' => 'required',
            'provinsi' => 'required',
            'kota' => 'required',
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'kodepos' => 'required',
        ]);
        $itemuser = $request->user(); //ambil data user yang sedang login
        $inputan = $request->all(); //buat variabel dengan nama $inputan
        $inputan['user_id'] = $itemuser->id;
        $inputan['status'] = 'utama';
        $itemalamatpengiriman = AlamatPengiriman::create($inputan);
        // set semua status alamat pengiriman bukan utama
        AlamatPengiriman::where('id', '!=', $itemalamatpengiriman->id)
            ->update(['status' => 'tidak']);
        return back()->with('success', 'Alamat pengiriman berhasil disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $itemalamatpengiriman = AlamatPengiriman::findOrFail($id);
        $itemalamatpengiriman->update(['status' => 'utama']);
        AlamatPengiriman::where('id', '!=', $id)->update(['status' => 'tidak']);
        return back()->with('success', 'Data berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
```

**#3. Buat views alamatpengiriman**

Pada folder views buat 1 buah folder baru dengan nama **alamatpengiriman** dan pada folder **alamatpengiriman** yang barusan kita buat kita tambahkan 1 buah file dengan nama **index.blade.php**

```php+HTML
@extends('layouts.template')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col col-12 mb-2">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col">
                                Alamat Pengiriman
                            </div>
                            <div class="col-auto">
                                <a href="{{ URL::to('checkout') }}" class="btn btn-sm btn-danger">
                                    Tutup
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-stripped">
                                <thead>
                                    <tr>
                                        <th>Nama Penerima</th>
                                        <th>Alamat</th>
                                        <th>No tlp</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($itemalamatpengiriman as $pengiriman)
                                        <tr>
                                            <td>
                                                {{ $pengiriman->nama_penerima }}
                                            </td>
                                            <td>
                                                {{ $pengiriman->alamat }}<br />
                                                {{ $pengiriman->kelurahan }}, {{ $pengiriman->kecamatan }}<br />
                                                {{ $pengiriman->kota }}, {{ $pengiriman->provinsi }} -
                                                {{ $pengiriman->kodepos }}
                                            </td>
                                            <td>
                                                {{ $pengiriman->no_tlp }}
                                            </td>
                                            <td>
                                                <form action="{{ route('alamatpengiriman.update', $pengiriman->id) }}"
                                                    method="post">
                                                    @method('patch')
                                                    @csrf()
                                                    @if ($pengiriman->status == 'utama')
                                                        <button type="submit" class="btn btn-primary btn-sm" disabled>Set
                                                            Utama</button>
                                                    @else
                                                        <button type="submit" class="btn btn-primary btn-sm">Set
                                                            Utama</button>
                                                    @endif
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-8">
                <div class="card">
                    <div class="card-header">
                        Form Alamat Pengiriman
                    </div>
                    <div class="card-body">
                        @if (count($errors) > 0)
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-warning">{{ $error }}</div>
                            @endforeach
                        @endif
                        @if ($message = Session::get('error'))
                            <div class="alert alert-warning">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        <form action="{{ route('alamatpengiriman.store') }}" method="post">
                            @csrf()
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="nama_penerima">Nama Penerima</label>
                                        <input type="text" name="nama_penerima" class="form-control"
                                            value={{ old('nama_penerima') }}>
                                    </div>
                                    <div class="form-group">
                                        <label for="alamat">Alamat</label>
                                        <input type="text" name="alamat" class="form-control" value={{ old('alamat') }}>
                                    </div>
                                    <div class="form-group">
                                        <label for="no_tlp">No Tlp</label>
                                        <input type="text" name="no_tlp" class="form-control" value={{ old('no_tlp') }}>
                                    </div>
                                    <div class="form-group">
                                        <label for="provinsi">Provinsi</label>
                                        <input type="text" name="provinsi" class="form-control"
                                            value={{ old('provinsi') }}>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="kota">Kota</label>
                                        <input type="text" name="kota" class="form-control" value={{ old('kota') }}>
                                    </div>
                                    <div class="form-group">
                                        <label for="kecamatan">Kecamatan</label>
                                        <input type="text" name="kecamatan" class="form-control"
                                            value={{ old('kecamatan') }}>
                                    </div>
                                    <div class="form-group">
                                        <label for="kelurahan">Kelurahan</label>
                                        <input type="text" name="kelurahan" class="form-control"
                                            value={{ old('kelurahan') }}>
                                    </div>
                                    <div class="form-group">
                                        <label for="kodepos">Kodepos</label>
                                        <input type="text" name="kodepos" class="form-control"
                                            value={{ old('kodepos') }}>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
```

**#4. Buat dan Koreksi migrations dan model Order**

Setelah alamat pengiriman sudah kita beresi, sekarang kita buat migrations dan juga model untuk **Order**. Buka kembali terminal dan jalankan perintah berikut kemudian tekan enter.

```php
php artisan make:model Order -m
```

Buka file migrations **_create_orders_table.php** dulu dan update menjadi seperti berikut.

```php+HTML
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cart_id')->unsigned();
            $table->string('nama_penerima');
            $table->string('no_tlp');
            $table->text('alamat');
            $table->string('provinsi');
            $table->string('kota');
            $table->string('kecamatan');
            $table->string('kelurahan');
            $table->string('kodepos');
            $table->foreign('cart_id')->references('id')->on('cart');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
```

Selanjutnya buka file model **Order.php** dan update menjadi seperti berikut.

```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'order';
    protected $fillable = [
        'cart_id',
        'nama_penerima',
        'no_tlp',
        'alamat',
        'provinsi',
        'kota',
        'kecamatan',
        'kelurahan',
        'kodepos',
    ];

    public function cart()
    {
        return $this->belongsTo(Cart::class, 'cart_id');
    }
}
```

Migrations dan model untuk alamat pengiriman dan order telah selesai kita buat. Sekarang migrate untuk membuat table dari migration tersebut. Buka terminal dan jalan perintah berikut. Jangan lupa tekan enter.

```php
php artisan migrate
```

![image-20220421134700987](img\image-20220421134700987.png)

**#5. Buat views checkout**

Untuk membuat halaman **checkout**, pada folder **views/cart** tambahkan 1 buah file dengan nama **checkout.blade.php**

![image-20220421134753338](img\image-20220421134753338.png)

```php+HTML
@extends('layouts.template')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col col-8">
                @if (count($errors) > 0)
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-warning">{{ $error }}</div>
                    @endforeach
                @endif
                @if ($message = Session::get('error'))
                    <div class="alert alert-warning">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <div class="row mb-2">
                    <div class="col col-12 mb-2">
                        <div class="card">
                            <div class="card-header">
                                Item
                            </div>
                            <div class="card-body">
                                <table class="table table-stripped">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Produk</th>
                                            <th>Harga</th>
                                            <th>Diskon</th>
                                            <th>Qty</th>
                                            <th>Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($itemcart->detail as $detail)
                                            <tr>
                                                <td>
                                                    {{ $no++ }}
                                                </td>
                                                <td>
                                                    {{ $detail->produk->nama_produk }}
                                                    <br />
                                                    {{ $detail->produk->kode_produk }}
                                                </td>
                                                <td>
                                                    {{ number_format($detail->harga, 2) }}
                                                </td>
                                                <td>
                                                    {{ number_format($detail->diskon, 2) }}
                                                </td>
                                                <td>
                                                    {{ number_format($detail->qty, 2) }}
                                                </td>
                                                <td>
                                                    {{ number_format($detail->subtotal, 2) }}
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col col-12">
                        <div class="card">
                            <div class="card-header">Alamat Pengiriman</div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-stripped">
                                        <thead>
                                            <tr>
                                                <th>Nama Penerima</th>
                                                <th>Alamat</th>
                                                <th>No tlp</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if ($itemalamatpengiriman)
                                                <tr>
                                                    <td>
                                                        {{ $itemalamatpengiriman->nama_penerima }}
                                                    </td>
                                                    <td>
                                                        {{ $itemalamatpengiriman->alamat }}<br />
                                                        {{ $itemalamatpengiriman->kelurahan }},
                                                        {{ $itemalamatpengiriman->kecamatan }}<br />
                                                        {{ $itemalamatpengiriman->kota }},
                                                        {{ $itemalamatpengiriman->provinsi }} -
                                                        {{ $itemalamatpengiriman->kodepos }}
                                                    </td>
                                                    <td>
                                                        {{ $itemalamatpengiriman->no_tlp }}
                                                    </td>
                                                    <td>
                                                        <a href="{{ route('alamatpengiriman.index') }}"
                                                            class="btn btn-success btn-sm">
                                                            Ubah Alamat
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer">
                                <a href="{{ route('alamatpengiriman.index') }}" class="btn btn-sm btn-primary">
                                    Tambah Alamat
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-4">
                <div class="card">
                    <div class="card-header">
                        Ringkasan
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <tr>
                                <td>No Invoice</td>
                                <td class="text-right">
                                    {{ $itemcart->no_invoice }}
                                </td>
                            </tr>
                            <tr>
                                <td>Subtotal</td>
                                <td class="text-right">
                                    {{ number_format($itemcart->subtotal, 2) }}
                                </td>
                            </tr>
                            <tr>
                                <td>Diskon</td>
                                <td class="text-right">
                                    {{ number_format($itemcart->diskon, 2) }}
                                </td>
                            </tr>
                            <tr>
                                <td>Total</td>
                                <td class="text-right">
                                    {{ number_format($itemcart->total, 2) }}
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="card-footer">
                        <form action="{{ route('transaksi.store') }}" method="post">
                            @csrf()
                            <button type="submit" class="btn btn-danger btn-block">Buat Pesanan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
```

**#6. Update CartController**

Di dalam file **CartController.php** kita akan menambahkan 1 buah function untuk menampilkan halaman **checkout** dan juga pada function `index`, kita akan mengoreksi sedikit agar tidak terjadi error semisal user belum login kemudian mengakses halaman cart. Selain itu juga kita akan menambahkan 2 buah model **Order** dan **AlamatPengiriman**.

Buka file **CartController.php** dan update menjadi seperti berikut.

```php
<?php

namespace App\Http\Controllers;

use App\Models\AlamatPengiriman;
use App\Models\Cart;
use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $itemuser = $request->user(); //ambil data user
        $itemcart = Cart::where('user_id', $itemuser->id)
            ->where('status_cart', 'cart')
            ->first();
        if ($itemcart) {
            $data = array(
                'title' => 'Shopping Cart',
                'itemcart' => $itemcart
            );
            return view('cart.index', $data)->with('no', 1);
        } else {
            return abort('404');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function kosongkan($id)
    {
        $itemcart = Cart::findOrFail($id);
        $itemcart->detail()->delete(); //hapus semua item di cart detail
        $itemcart->updatetotal($itemcart, '-' . $itemcart->subtotal);
        return back()->with('success', 'Cart berhasil dikosongkan');
    }

    public function checkout(Request $request)
    {
        $itemuser = $request->user();
        $itemcart = Cart::where('user_id', $itemuser->id)
            ->where('status_cart', 'cart')
            ->first();
        $itemalamatpengiriman = AlamatPengiriman::where('user_id', $itemuser->id)
            ->where('status', 'utama')
            ->first();
        if ($itemcart) {
            $data = array(
                'title' => 'Checkout',
                'itemcart' => $itemcart,
                'itemalamatpengiriman' => $itemalamatpengiriman
            );
            return view('cart.checkout', $data)->with('no', 1);
        } else {
            return abort('404');
        }
    }
}
```

**#7. Update function index TransaksiController**

Pada file controller **TransaksiController.php** kita akan menambahkan 2 model baru **(Order dan AlamatPengiriman)** dan juga megoreksi beberapa function yang ada.

Buka file **TransaksiController.php** dan update menjadi seperti berikut.

```php
<?php

namespace App\Http\Controllers;

use App\Models\AlamatPengiriman;
use App\Models\Cart;
use App\Models\Order;
use Illuminate\Http\Request;

class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $itemuser = $request->user();
        if ($itemuser->role == 'admin') {
            // kalo admin maka menampilkan semua cart
            $itemorder = Order::whereHas('cart', function ($q) use ($itemuser) {
                $q->where('status_cart', 'checkout');
            })
                ->orderBy('created_at', 'desc')
                ->paginate(20);
        } else {
            // kalo member maka menampilkan cart punyanya sendiri
            $itemorder = Order::whereHas('cart', function ($q) use ($itemuser) {
                $q->where('status_cart', 'checkout');
                $q->where('user_id', $itemuser->id);
            })
                ->orderBy('created_at', 'desc')
                ->paginate(20);
        }
        $data = array(
            'title' => 'Data Transaksi',
            'itemorder' => $itemorder,
            'itemuser' => $itemuser
        );
        return view('transaksi.index', $data)->with('no', ($request->input('page', 1) - 1) * 20);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $itemuser = $request->user();
        $itemcart = Cart::where('status_cart', 'cart')
            ->where('user_id', $itemuser->id)
            ->first();
        if ($itemcart) {
            $itemalamatpengiriman = AlamatPengiriman::where('user_id', $itemuser->id)
                ->where('status', 'utama')
                ->first();
            if ($itemalamatpengiriman) {
                // buat variabel inputan order
                $inputanorder['cart_id'] = $itemcart->id;
                $inputanorder['nama_penerima'] = $itemalamatpengiriman->nama_penerima;
                $inputanorder['no_tlp'] = $itemalamatpengiriman->no_tlp;
                $inputanorder['alamat'] = $itemalamatpengiriman->alamat;
                $inputanorder['provinsi'] = $itemalamatpengiriman->provinsi;
                $inputanorder['kota'] = $itemalamatpengiriman->kota;
                $inputanorder['kecamatan'] = $itemalamatpengiriman->kecamatan;
                $inputanorder['kelurahan'] = $itemalamatpengiriman->kelurahan;
                $inputanorder['kodepos'] = $itemalamatpengiriman->kodepos;
                $itemorder = Order::create($inputanorder); //simpan order
                // update status cart
                $itemcart->update(['status_cart' => 'checkout']);
                return redirect()->route('transaksi.index')->with('success', 'Order berhasil disimpan');
            } else {
                return back()->with('error', 'Alamat pengiriman belum diisi');
            }
        } else {
            return abort('404'); //kalo ternyata ga ada shopping cart, maka akan menampilkan error halaman tidak ditemukan
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = array('title' => 'Detail Transaksi');
        return view('transaksi.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $itemuser = $request->user();
        if ($itemuser->role == 'admin') {
            $itemorder = Order::findOrFail($id);
            $data = array(
                'title' => 'Form Edit Transaksi',
                'itemorder' => $itemorder
            );
            return view('transaksi.edit', $data)->with('no', 1);
        } else {
            return abort('404'); //kalo bukan admin maka akan tampil error halaman tidak ditemukan
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
```

**#8. Update button checkout pada views cart/index.blade.php**

Buka file **cart/index.blade.php** kemudian update bagian

```php
<button class="btn btn-primary btn-block">Checkout</button>
```

Menjadi

```php
<a href="{{ URL::to('checkout') }}" class="btn btn-primary btn-block">
    Checkout
</a>
```

**#9. Update views transaksi/index.blade.php**

Karena pada bagian ini transaksi sudah diisi oleh data yang sudah diinput lewat proses order/transaksi. Maka isinya akan kita update. Buka file views **transaksi/index.blade.php** yang telah kita buat sebelumnya. Hapus semua isinya kemudian ganti dengan kode di bawah ini.

```php+HTML
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            Data Transaksi
                        </h3>
                    </div>
                    <div class="card-body">
                        <!-- digunakan untuk menampilkan pesan error atau sukses -->
                        @if (count($errors) > 0)
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-warning">{{ $error }}</div>
                            @endforeach
                        @endif
                        @if ($message = Session::get('error'))
                            <div class="alert alert-warning">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Invoice</th>
                                        <th>Sub Total</th>
                                        <th>Diskon</th>
                                        <th>Ongkir</th>
                                        <th>Total</th>
                                        <th>Status Pembayaran</th>
                                        <th>Status Pengiriman</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($itemorder as $order)
                                        <tr>
                                            <td>
                                                {{ ++$no }}
                                            </td>
                                            <td>
                                                {{ $order->cart->no_invoice }}
                                            </td>
                                            <td>
                                                {{ number_format($order->cart->subtotal, 2) }}
                                            </td>
                                            <td>
                                                {{ number_format($order->cart->diskon, 2) }}
                                            </td>
                                            <td>
                                                {{ number_format($order->cart->ongkir, 2) }}
                                            </td>
                                            <td>
                                                {{ number_format($order->cart->total, 2) }}
                                            </td>
                                            <td>
                                                {{ $order->cart->status_pembayaran }}
                                            </td>
                                            <td>
                                                {{ $order->cart->status_pengiriman }}
                                            </td>
                                            <td>
                                                <a href="{{ route('transaksi.show', $order->id) }}"
                                                    class="btn btn-sm btn-info mb-2">
                                                    Detail
                                                </a>
                                                @if ($itemuser->role == 'admin')
                                                    <a href="{{ route('transaksi.edit', $order->id) }}"
                                                        class="btn btn-sm btn-primary mb-2">
                                                        Edit
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
```

**#10. Tambahkan route alamat pengiriman ke web.php**

Untuk route kita butuh menambahkan route ke halaman checkout dan juga alamatpengiriman. Buka file **web.php** dan tambahkan route baru di bawah `Route::resource('cartdetail', 'CartDetailController');`

```php
// alamat pengiriman
Route::resource('alamatpengiriman', AlamatPengirimanController::class);
// checkout
Route::get('checkout', [CartController::class,'checkout']);
```

Silahkan jalankan http://toko-klowor.local/

Kemudian masukkan item atau produk ke shopping cart, kemudian tekan checkout. Lanjutkan proses order sampai order berhasil tersimpan.

![image-20220421135919892](img\image-20220421135919892.png)

![image-20220421140028854](img\image-20220421140028854.png)

![image-20220421140055046](img\image-20220421140055046.png)