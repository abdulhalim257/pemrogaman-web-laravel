# Update Transaksi dan Laporan Transaksi

Setelah proses checkout dibuat, selanjutnya admin melakukan update transaksi untuk menginput resi pengiriman.

**Update file edit**

Buka file **views/transaksi/edit.blade.php**

```php+HTML
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-lg-8 col-md-8 mb-2">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Item</h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>
                                            No
                                        </th>
                                        <th>
                                            Kode
                                        </th>
                                        <th>
                                            Nama
                                        </th>
                                        <th>
                                            Harga
                                        </th>
                                        <th>
                                            Diskon
                                        </th>
                                        <th>
                                            Qty
                                        </th>
                                        <th>
                                            Subtotal
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($itemorder->cart->detail as $detail)
                                        <tr>
                                            <td>
                                                {{ $no++ }}
                                            </td>
                                            <td>
                                                {{ $detail->produk->kode_produk }}
                                            </td>
                                            <td>
                                                {{ $detail->produk->nama_produk }}
                                            </td>
                                            <td class="text-right">
                                                {{ number_format($detail->harga, 2) }}
                                            </td>
                                            <td class="text-right">
                                                {{ number_format($detail->diskon, 2) }}
                                            </td>
                                            <td class="text-right">
                                                {{ $detail->qty }}
                                            </td>
                                            <td class="text-right">
                                                {{ number_format($detail->subtotal, 2) }}
                                            </td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="6">
                                            <b>Total</b>
                                        </td>
                                        <td class="text-right">
                                            <b>
                                                {{ number_format($itemorder->cart->total, 2) }}
                                            </b>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="{{ route('transaksi.index') }}" class="btn btn-sm btn-danger">Tutup</a>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">Alamat Pengiriman</div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-stripped">
                                <thead>
                                    <tr>
                                        <th>Nama Penerima</th>
                                        <th>Alamat</th>
                                        <th>No tlp</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            {{ $itemorder->nama_penerima }}
                                        </td>
                                        <td>
                                            {{ $itemorder->alamat }}<br />
                                            {{ $itemorder->kelurahan }}, {{ $itemorder->kecamatan }}<br />
                                            {{ $itemorder->kota }}, {{ $itemorder->provinsi }} -
                                            {{ $itemorder->kodepos }}
                                        </td>
                                        <td>
                                            {{ $itemorder->no_tlp }}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-lg-4 col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Ringkasan</h3>
                    </div>
                    <div class="card-body">
                        @if (count($errors) > 0)
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-warning">{{ $error }}</div>
                            @endforeach
                        @endif
                        @if ($message = Session::get('error'))
                            <div class="alert alert-warning">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table">
                                <form action="{{ route('transaksi.update', $itemorder->id) }}" method='post'>
                                    @csrf
                                    {{ method_field('patch') }}
                                    <tbody>
                                        <tr>
                                            <td>
                                                Total
                                            </td>
                                            <td>
                                                <input type="text" name="total" id="total" class="form-control"
                                                    value="{{ $itemorder->cart->total }}">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Subtotal
                                            </td>
                                            <td>
                                                <input type="text" name="subtotal" id="subtotal" class="form-control"
                                                    value="{{ $itemorder->cart->subtotal }}">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Diskon
                                            </td>
                                            <td>
                                                <input type="text" name="diskon" id="diskon" class="form-control"
                                                    value="{{ $itemorder->cart->diskon }}">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Ongkir
                                            </td>
                                            <td>
                                                <input type="text" name="ongkir" id="ongkir" class="form-control"
                                                    value="{{ $itemorder->cart->ongkir }}">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Ekspedisi
                                            </td>
                                            <td>
                                                <input type="text" name="ekspedisi" id="ekspedisi" class="form-control"
                                                    value="{{ $itemorder->cart->ekspedisi }}">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                No. Resi
                                            </td>
                                            <td>
                                                <input type="text" name="no_resi" id="no_resi" class="form-control"
                                                    value="{{ $itemorder->cart->no_resi }}">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Status Pembayaran
                                            </td>
                                            <td>
                                                <select name="status_pembayaran" id="status_pembayaran"
                                                    class="form-control">
                                                    <option value="sudah"
                                                        {{ $itemorder->cart->status_pembayaran == 'sudah' ? 'selected' : '' }}>
                                                        Sudah Dibayar</option>
                                                    <option value="belum"
                                                        {{ $itemorder->cart->status_pembayaran == 'belum' ? 'selected' : '' }}>
                                                        Belum Dibayar</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Status Pengiriman
                                            </td>
                                            <td>
                                                <select name="status_pengiriman" id="status_pengiriman"
                                                    class="form-control">
                                                    <option value="sudah"
                                                        {{ $itemorder->cart->status_pengiriman == 'sudah' ? 'selected' : '' }}>
                                                        Sudah</option>
                                                    <option value="belum"
                                                        {{ $itemorder->cart->status_pengiriman == 'belum' ? 'selected' : '' }}>
                                                        Belum</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <button type="submit" class="btn btn-primary">Update</button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </form>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
```

**Update file show**

Untuk menampilkan detail transaksi update file **views/transaksi/show.blade.php**

```php+HTML
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-lg-8 col-md-8 mb-2">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Item</h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>
                                            No
                                        </th>
                                        <th>
                                            Kode
                                        </th>
                                        <th>
                                            Nama
                                        </th>
                                        <th>
                                            Harga
                                        </th>
                                        <th>
                                            Diskon
                                        </th>
                                        <th>
                                            Qty
                                        </th>
                                        <th>
                                            Subtotal
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($itemorder->cart->detail as $detail)
                                        <tr>
                                            <td>
                                                {{ $no++ }}
                                            </td>
                                            <td>
                                                {{ $detail->produk->kode_produk }}
                                            </td>
                                            <td>
                                                {{ $detail->produk->nama_produk }}
                                            </td>
                                            <td class="text-right">
                                                {{ number_format($detail->harga, 2) }}
                                            </td>
                                            <td class="text-right">
                                                {{ number_format($detail->diskon, 2) }}
                                            </td>
                                            <td class="text-right">
                                                {{ $detail->qty }}
                                            </td>
                                            <td class="text-right">
                                                {{ number_format($detail->subtotal, 2) }}
                                            </td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="6">
                                            <b>Total</b>
                                        </td>
                                        <td class="text-right">
                                            <b>
                                                {{ number_format($itemorder->cart->total, 2) }}
                                            </b>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="{{ route('transaksi.index') }}" class="btn btn-sm btn-danger">Tutup</a>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">Alamat Pengiriman</div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-stripped">
                                <thead>
                                    <tr>
                                        <th>Nama Penerima</th>
                                        <th>Alamat</th>
                                        <th>No tlp</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            {{ $itemorder->nama_penerima }}
                                        </td>
                                        <td>
                                            {{ $itemorder->alamat }}<br />
                                            {{ $itemorder->kelurahan }}, {{ $itemorder->kecamatan }}<br />
                                            {{ $itemorder->kota }}, {{ $itemorder->provinsi }} -
                                            {{ $itemorder->kodepos }}
                                        </td>
                                        <td>
                                            {{ $itemorder->no_tlp }}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-lg-4 col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Ringkasan</h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>
                                            Total
                                        </td>
                                        <td class="text-right">
                                            {{ number_format($itemorder->cart->total, 2) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Subtotal
                                        </td>
                                        <td class="text-right">
                                            {{ number_format($itemorder->cart->subtotal, 2) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Diskon
                                        </td>
                                        <td class="text-right">
                                            {{ number_format($itemorder->cart->diskon, 2) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Ongkir
                                        </td>
                                        <td class="text-right">
                                            {{ number_format($itemorder->cart->ongkir, 2) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Ekspedisi
                                        </td>
                                        <td class="text-right">
                                            {{ number_format($itemorder->cart->ekspedisi, 2) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            No. Resi
                                        </td>
                                        <td class="text-right">
                                            {{ number_format($itemorder->cart->no_resi, 2) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Status Pembayaran
                                        </td>
                                        <td class="text-right">
                                            {{ $itemorder->cart->status_pembayaran }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Status Pengiriman
                                        </td>
                                        <td class="text-right">
                                            {{ $itemorder->cart->status_pengiriman }}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
```

**Handle update transaksi dan menampilkan detail transaksi**

Kemudian untuk menghandle proses update transaksi, update `function update` di controller **TransaksiController.php**. Dan untuk menampilkan detail transaksi update `function show` di controller **TransaksiController.php** juga.

```php+HTML
<?php

namespace App\Http\Controllers;

use App\Models\AlamatPengiriman;
use App\Models\Cart;
use App\Models\Order;
use Illuminate\Http\Request;

class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $itemuser = $request->user();
        if ($itemuser->role == 'admin') {
            // kalo admin maka menampilkan semua cart
            $itemorder = Order::whereHas('cart', function ($q) use ($itemuser) {
                $q->where('status_cart', 'checkout');
            })
                ->orderBy('created_at', 'desc')
                ->paginate(20);
        } else {
            // kalo member maka menampilkan cart punyanya sendiri
            $itemorder = Order::whereHas('cart', function ($q) use ($itemuser) {
                $q->where('status_cart', 'checkout');
                $q->where('user_id', $itemuser->id);
            })
                ->orderBy('created_at', 'desc')
                ->paginate(20);
        }
        $data = array(
            'title' => 'Data Transaksi',
            'itemorder' => $itemorder,
            'itemuser' => $itemuser
        );
        return view('transaksi.index', $data)->with('no', ($request->input('page', 1) - 1) * 20);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $itemuser = $request->user();
        $itemcart = Cart::where('status_cart', 'cart')
            ->where('user_id', $itemuser->id)
            ->first();
        if ($itemcart) {
            $itemalamatpengiriman = AlamatPengiriman::where('user_id', $itemuser->id)
                ->where('status', 'utama')
                ->first();
            if ($itemalamatpengiriman) {
                // buat variabel inputan order
                $inputanorder['cart_id'] = $itemcart->id;
                $inputanorder['nama_penerima'] = $itemalamatpengiriman->nama_penerima;
                $inputanorder['no_tlp'] = $itemalamatpengiriman->no_tlp;
                $inputanorder['alamat'] = $itemalamatpengiriman->alamat;
                $inputanorder['provinsi'] = $itemalamatpengiriman->provinsi;
                $inputanorder['kota'] = $itemalamatpengiriman->kota;
                $inputanorder['kecamatan'] = $itemalamatpengiriman->kecamatan;
                $inputanorder['kelurahan'] = $itemalamatpengiriman->kelurahan;
                $inputanorder['kodepos'] = $itemalamatpengiriman->kodepos;
                $itemorder = Order::create($inputanorder); //simpan order
                // update status cart
                $itemcart->update(['status_cart' => 'checkout']);
                return redirect()->route('transaksi.index')->with('success', 'Order berhasil disimpan');
            } else {
                return back()->with('error', 'Alamat pengiriman belum diisi');
            }
        } else {
            return abort('404'); //kalo ternyata ga ada shopping cart, maka akan menampilkan error halaman tidak ditemukan
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {

        $itemuser = $request->user();
        if ($itemuser->role == 'admin') {
            $itemorder = Order::findOrFail($id);
            $data = array(
                'title' => 'Detail Transaksi',
                'itemorder' => $itemorder
            );
            return view('transaksi.show', $data)->with('no', 1);
        } else {
            $itemorder = Order::where('id', $id)
                ->whereHas('cart', function ($q) use ($itemuser) {
                    $q->where('user_id', $itemuser->id);
                })->first();
            if ($itemorder) {
                $data = array(
                    'title' => 'Detail Transaksi',
                    'itemorder' => $itemorder
                );
                return view('transaksi.show', $data)->with('no', 1);
            } else {
                return abort('404');
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $itemuser = $request->user();
        if ($itemuser->role == 'admin') {
            $itemorder = Order::findOrFail($id);
            $data = array(
                'title' => 'Form Edit Transaksi',
                'itemorder' => $itemorder
            );
            return view('transaksi.edit', $data)->with('no', 1);
        } else {
            return abort('404'); //kalo bukan admin maka akan tampil error halaman tidak ditemukan
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'status_pembayaran' => 'required',
            'status_pengiriman' => 'required',
            'subtotal' => 'required|numeric',
            'ongkir' => 'required|numeric',
            'diskon' => 'required|numeric',
            'total' => 'required|numeric',
        ]);
        $inputan = $request->all();
        $inputan['status_pembayaran'] = $request->status_pembayaran;
        $inputan['status_pengiriman'] = $request->status_pengiriman;
        $inputan['subtotal'] = str_replace(',', '', $request->subtotal);
        $inputan['ongkir'] = str_replace(',', '', $request->ongkir);
        $inputan['diskon'] = str_replace(',', '', $request->diskon);
        $inputan['total'] = str_replace(',', '', $request->total);
        $itemorder = Order::findOrFail($id);
        $itemorder->cart->update($inputan);
        return back()->with('success', 'Order berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

```

**Laporan Transaksi**

Laporan transaksi kita buat dengan mengedit file **LaporanController.php** berikut

```php
<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;

class LaporanController extends Controller
{
    public function index()
    {
        $data = array('title' => 'Form Laporan Penjualan');
        return view('laporan.index', $data);
    }

    public function proses(Request $request)
    {
        $bulan = $request->bulan;
        $tahun = $request->tahun;
        $bulan_transaksi = date('Y-m', strtotime($request->tahun . '-' . $request->bulan));
        $itemtransaksi = Order::whereHas('cart', function ($q) use ($bulan_transaksi) {
            $q->where('status_pembayaran', 'sudah');
            $q->where('created_at', 'like', $bulan_transaksi . '%');
        })
            ->get();
        $data = array(
            'title' => 'Laporan Penjualan',
            'itemtransaksi' => $itemtransaksi,
            'bulan' => $this->cetakbulan($bulan),
            'tahun' => $tahun
        );
        return view('laporan.proses', $data)->with('no', 1);
    }

    public function cetakbulan($bulan)
    {
        switch ($bulan) {
            case '1':
                return "Januari";
                break;
            case '2':
                return "Februari";
                break;
            case '3':
                return "Maret";
                break;
            case '4':
                return "April";
                break;
            case '5':
                return "Mei";
                break;
            case '6':
                return "Juni";
                break;
            case '7':
                return "Juli";
                break;
            case '8':
                return "Agustus";
                break;
            case '9':
                return "September";
                break;
            case '10':
                return "Oktober";
                break;
            case '11':
                return "Nopember";
                break;
            case '12':
                return "Desember";
                break;

            default:
                return "";
                break;
        }
    }
}

```

Kemudian untuk menampilkan form laporan, update file **views/laporan/index.blade.php** menjadi seperti berikut.

```php+HTML
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-lg-4 col-md-4">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">Form Laporan</h3>
                    </div>
                    <div class="card-body">
                        <form action="/admin/proseslaporan">
                            <div class="form-group">
                                <label for="bulan">Bulan</label>
                                <select name="bulan" id="bulan" class="form-control">
                                    <option value="1">Januari</option>
                                    <option value="2">Februari</option>
                                    <option value="3">Maret</option>
                                    <option value="4">April</option>
                                    <option value="5">Mei</option>
                                    <option value="6">Juni</option>
                                    <option value="7">Juli</option>
                                    <option value="8">Agustus</option>
                                    <option value="9">September</option>
                                    <option value="10">Oktober</option>
                                    <option value="11">Nopember</option>
                                    <option value="12">Desember</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="tahun">Tahun</label>
                                <select name="tahun" id="tahun" class="form-control">
                                    @for ($a = 2019; $a <= 2050; $a++)
                                        <option value="{{ $a }}">{{ $a }}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Buka Laporan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
```

Kemudian untuk menampilkan hasil laporan, update file **views/laporan/proses.blade.php** menjadi seperti berikut.

```php+HTML
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">Laporan Penjualan</h3>
                        <div class="card-tools">
                            <a href="{{ URL('admin/laporan') }}" class="btn btn-sm btn-danger">
                                Tutup
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <h3 class="text-center">Periode {{ $bulan != '' ? 'Bulan ' . $bulan : '' }} {{ $tahun }}</h3>
                        <div class="row">
                            <div class="col col-lg-4 col-md-4">
                                <h4 class="text-center">Ringkasan Transaksi</h4>
                                <!-- cetak totalnya -->
                                <?php
                                $total = 0;
                                foreach ($itemtransaksi as $k) {
                                    $total += $k->cart->total;
                                }
                                ?>
                                <!-- end cetak totalnya -->
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <td>Total Penjualan</td>
                                            <td>Rp. {{ number_format($total, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Total Transaksi</td>
                                            <td>{{ count($itemtransaksi) }} Transaksi</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col col-lg-8 col-md-8">
                                <h4 class="text-center">Rincian Transaksi</h4>
                                <div class="table-responsive">
                                    <table class="table table-stripped">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Invoice</th>
                                                <th>Subtotal</th>
                                                <th>Ongkir</th>
                                                <th>Diskon</th>
                                                <th>Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($itemtransaksi as $transaksi)
                                                <tr>
                                                    <td>{{ $no++ }}</td>
                                                    <td>
                                                        {{ $transaksi->cart->no_invoice }}
                                                    </td>
                                                    <td>
                                                        {{ number_format($transaksi->cart->subtotal, 2) }}
                                                    </td>
                                                    <td>
                                                        {{ number_format($transaksi->cart->ongkir, 2) }}
                                                    </td>
                                                    <td>
                                                        {{ number_format($transaksi->cart->diskon, 2) }}
                                                    </td>
                                                    <td>
                                                        {{ number_format($transaksi->cart->total, 2) }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
```

Akses halaman transaksi dan laporan. Coba update transaksi.

![image-20220422123502605](img\image-20220422123502605.png)

Klik edit

![image-20220422123530475](img\image-20220422123530475.png)

![image-20220422123655036](img\image-20220422123655036.png)

Akses laporan, dan pilih bulan dan tahun 

![image-20220422124315114](img\image-20220422124315114.png)

# Penyesuaian akhir

## 1. Menu dashboard

Disini kita  harus menyesuaikan menu yang hanya bisa diakses oleh admin saja maupun oleh pembeli. Edit **menudashboard.blade.php**

![image-20220513134259441](img\image-20220513134259441.png)

```php+HTML
<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
            with font-awesome or any other icon font library -->
        <li class="nav-item">
            <a href="/admin" class="nav-link">
                <i class="nav-icon fas fa-th"></i>
                <p>
                    Dashboard
                </p>
            </a>
        </li>
        @if (Auth::check() && Auth::user()->role == 'admin')
            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-folder-open"></i>
                    <p>
                        Produk
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('produk.index') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Produk</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('kategori.index') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Kategori</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('promo.index') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Promo</p>
                        </a>
                    </li>
                </ul>
            </li>
        @endif
        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-shopping-cart"></i>
                <p>
                    Transaksi
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ route('transaksi.index') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Data Transaksi</p>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item">
            <a href="{{ route('wishlist.index') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Wishlist</p>
            </a>
        </li>

        @if (Auth::check() && Auth::user()->role == 'admin')
            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-folder"></i>
                    <p>
                        Data
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('customer.index') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Customer</p>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-cogs"></i>
                    <p>
                        Setting
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('slideshow.index') }}" class="nav-link">
                            <i class="far fa-images nav-icon"></i>
                            <p>Slideshow</p>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-list"></i>
                    <p>
                        Laporan
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ URL::to('admin/laporan') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Penjualan</p>
                        </a>
                    </li>
                </ul>
            </li>
        @endif
        <li class="nav-item">
            <a href="{{ URL::to('admin/profil') }}" class="nav-link">
                <i class="nav-icon fas fa-users"></i>
                <p>
                    Profil
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link" onclick="event.preventDefault();
                                                       document.getElementById('logout-form').submit();">
                <i class="nav-icon fas fa-sign-out-alt"></i>
                <p>
                    Sign Out
                </p>
            </a>
        </li>
    </ul>
</nav>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
</form>

```

 maka menu dibagian pembeli akan menjadi seperti berikut:

![image-20220513134659801](img\image-20220513134659801.png)

## 2. Ubah Dahsboard

![image-20220513135028758](img\image-20220513135028758.png)

```php+HTML
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>{{ $title }}</title>

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ asset('js/fontawesome-free/css/all.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('css/adminlte.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- jQuery -->
    <script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i
                            class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="/" class="nav-link">Home</a>
                </li>
            </ul>


            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <!-- Notifications Dropdown Menu -->
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="far fa-bell"></i>
                        <span class="badge badge-warning navbar-badge">15</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <span class="dropdown-header">15 Notifications</span>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <i class="fas fa-envelope mr-2"></i> 4 new messages
                            <span class="float-right text-muted text-sm">3 mins</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <i class="fas fa-users mr-2"></i> 8 friend requests
                            <span class="float-right text-muted text-sm">12 hours</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <i class="fas fa-file mr-2"></i> 3 new reports
                            <span class="float-right text-muted text-sm">2 days</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
                    </div>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="index3.html" class="brand-link">
                <img src="{{ asset('img/AdminLTELogo.png') }}" alt="AdminLTE Logo"
                    class="brand-image img-circle elevation-3" style="opacity: .8">
                <span class="brand-text font-weight-light">Toko Klowor</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <img src="{{ asset('img/user2-160x160.jpg') }}" class="img-circle elevation-2"
                            alt="User Image">
                    </div>
                    <div class="info">
                        <a href="#" class="d-block">{{Auth::user()->name}}</a>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                @include('layouts.menudashboard')
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">{{ $title }}</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Starter Page</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                @yield('content')
            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Main Footer -->
        <footer class="main-footer">
            <!-- To the right -->
            <div class="float-right d-none d-sm-inline">
                Anything you want
            </div>
            <!-- Default to the left -->
            <strong>Copyright &copy; 2022 <a href="#">JorKloworDev</a>.</strong> All rights
            reserved.
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->

    <!-- jQuery -->
    <script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('js/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('js/adminlte.min.js') }}"></script>
</body>

</html>
```

maka pada bagian ini nama akan sesuai dengan siapa yang login

![image-20220513135124976](img\image-20220513135124976-16524246860543.png)

## 3. Ubah view Profil

![image-20220513135953453](img\image-20220513135953453.png)

```php+HTML
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-lg-6 col-md-6">
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                        <div class="text-center">
                            <img src="{{ asset('img/user2-160x160.jpg') }}" alt="profil"
                                class="profile-user-img img-responsive img-circle">
                        </div>
                        <h3 class="profile-username text-center">{{Auth::user()->name}}</h3>
                        <p class="text-muted text-center">Member sejak : {{Auth::user()->created_at}}</p>
                        <hr>
                        <strong>
                            <i class="fas fa-map-marker mr-2"></i>
                            Alamat
                        </strong>
                        <p class="text-muted">
                            {{Auth::user()->alamat}}
                        </p>
                        <hr>
                        <strong>
                            <i class="fas fa-envelope mr-2"></i>
                            Email
                        </strong>
                        <p class="text-muted">
                            {{Auth::user()->email}}
                        </p>
                        <hr>
                        <strong>
                            <i class="fas fa-phone mr-2"></i>
                            No Tlp
                        </strong>
                        <p class="text-muted">
                            {{Auth::user()->phone}}
                        </p>
                        <hr>
                        <a href="{{ URL::to('admin/setting') }}" class="btn btn-primary btn-block">Setting</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
```

## 4. Ubah view Setting Profil

![image-20220513140058405](img\image-20220513140058405.png)

```php+HTML
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-lg-4 col-md-4">
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                        <div class="text-center">
                            <img src="{{ asset('img/user2-160x160.jpg') }}" alt="profil"
                                class="profile-user-img img-responsive img-circle">
                        </div>
                        <hr>
                        <form action="">
                            <div class="form-group">
                                <label for="name">Nama</label>
                                <input type="text" name="name" id="name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="phone">No Tlp</label>
                                <input type="text" name="phone" id="phone" class="form-control">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
```

