# Membuat Toko Online

## Membuat Projek Baru

Sebagai langkah awal, kita perlu melakukan **instalasi Laravel**. Ada beberapa cara instalasi Laravel, bisa melalui **VPS**, **cPanel**, dan di **komputer**. Nah, di tutorial kali ini kita akan menggunakan Laravel di komputer menggunakan sistem operasi Windows.

Cara Instal Laravel adalah sebagai berikut:

- Buka Laragon dan Jalankan.

  ![image-20220324203023758](img\image-20220324203023758.png)

- Klik kanan >  Quick App > Laravel

  ![image-20220310212122141](img\image-20220310212122141.png)

- Kemudian beri nama projek kalian dengan `toko-nim`

- Tunggu sampai proses instalasi selesai

  ![image-20220324203400647](img\image-20220324203400647.png)

  > Nb.
  >
  > 1. Project path adalah tempat dimana projek kita disimpan
  > 2. Pretty url adalah alamat yang digunakan untuk mengakses projek yang kita buat

- Sekarang kita jalankan `php artisan serve` pada terminal, atau dengan mengkakses url.hostname. Cara mengaturnya adalah dengan klik kanan pada laragon **Preferences..**  kemudian setting hostname menjadi **{name}.local**. Restart Laragon!

  ![image-20220324203647665](img\image-20220324203647665.png)

  Berikut adalah halaman awal jika kita sudah berhasil membuat projek baru laravel.

## Routing

Setelah project yang barusan kita buat sudah dapat diakses, sebelum lanjut kita pelajari dulu pengetahuan dasar dari laravel yaitu routing. Lebih lengkapnya bisa dibaca di dokumentasinya https://laravel.com/docs/9.x/routing. 

Untuk mengatur routing, silahkan buka file **web.php** di folder routes. Sekarang silahkan coba tambahkan baris baru dan ketik route seperti berikut.

```php
Route::get('/halo', function() {
    return "Halo nama saya fadlur";
});
```

Sekarang akses route kita http://toko-klowor.local/halo, maka akan tampil seperti berikut.

![image-20220324204051107](img\image-20220324204051107.png)

Kemudian kita akan mencoba membuat route menggunakan file controller. Buat 1 buah controller dengan perintah

```markup
php artisan make:controller LatihanController
```

Buka dan edit file LatihanController.php menjadi seperti di bawah ini.

![image-20220324204220936](img\image-20220324204220936.png)

```php
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LatihanController extends Controller
{
    public function index()
    {
        return "oke, ini dari controller";
    }
}
?>
```

Dan file web.php tambahkan 1 buah route lagi menjadi seperti berikut.

```php
<?php

use App\Http\Controllers\LatihanController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/halo', function () {
    return "Halo nama saya fadlur";
});
Route::get('/latihan', [LatihanController::class,'index']);

```

Jalankan kembali dan akses dengan alamat http://toko-klowor.local/latihan maka di browser akan tampil tulisan.

![image-20220324204613401](img\image-20220324204613401.png)

**# Route Parameters**

Kadang kala kita butuh mengambil "id" atau "judul" dari alamat yang kita ketik. Semisal http://toko-klowor.local/blog/2, dan kita butuh mengambil angka 2 ke function kita. Maka route parameter adalah solusinya.

Di file web.php kita tambahkan route

```php
Route::get('/blog/{id}', [LatihanController::class,'blog']);
```

Dan di file LatihanController kita tambahkan 1 buah function di bawah function index.

```php
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LatihanController extends Controller
{
    public function index()
    {
        return "oke, ini dari controller";
    }
    public function blog($id)
    {
        return "Ini blog dengan id " . $id;
    }
}

```

![image-20220324204918029](img\image-20220324204918029.png)

Kalau parameter lebih dari 1, sebagai contoh kita akan mengambil data komentar berdasarkan id blog dan id komentarnya.

Kita tambahkan 1 baris route lagi di file web.php nya

```php
Route::get('/blog/{idblog}/komentar/{idkomentar}',[LatihanController::class,'komentar']);
```

Dan LatihanController.php kita tambahkan 1 buah lagi function sehingga menjadi

```php
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LatihanController extends Controller
{
    public function index()
    {
        return "oke, ini dari controller";
    }
    public function blog($id)
    {
        return "Ini blog dengan id " . $id;
    }
    public function komentar($idblog, $idkomentar)
    {
        echo 'Id blognya : ' . $idblog;
        echo '<br />';
        echo 'Id komentarnya : ' . $idkomentar;
    }
}

```

Dan ketika diakses dengan alamat http://toko-klowor.local/blog/123/komentar/345, hasilnya adalah

![image-20220324205116504](img\image-20220324205116504.png)

Itu tadi adalah sekilas tentang route di laravel. Selanjutnya kita akan membahas tentang template bawaan dari laravel,yaitu blade template.

# Blade

Blade template adalah engine template bawaannya laravel. Blade tidak seperti umumnya file php yang membatasi penggunaan skrip di dalam view. View adalah salah satu bagian dari laravel yang berfungsi untuk menampilkan data ke pengguna atau user. File blade biasanya menggunakan ekstensi ***.blade.php*** dan ditaruh di dalam folder *`resources/views`*.

Sebagai bahan latihan, buatlah 1 buah file dengan nama **beranda.blade.php** di dalam folder `resources/views`. Dan isi dengan kalimat berikut.

```php+HTML
<h1>Selamat Datang di Kelas</h1>
<p>
    Ini adalah paragraph
</p>
<p>
    Kata ini datanya dari controller dengan variabel nama yang isinya : {{ $nama }}
</p>

```

dan di file LatihanController.php tambahkan 1 buah function lagi, sehingga menjadi

```php
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LatihanController extends Controller
{
    public function index()
    {
        return "oke, ini dari controller";
    }
    public function blog($id)
    {
        return "Ini blog dengan id " . $id;
    }
    public function komentar($idblog, $idkomentar)
    {
        echo 'Id blognya : ' . $idblog;
        echo '<br />';
        echo 'Id komentarnya : ' . $idkomentar;
    }
    public function beranda()
    {
        $data = array('nama' => 'Toko Klowor');
        return view('beranda', $data);
    }
}

```

Jangan lupa, di file web.php tambahkan 1 buah route dengan nama beranda.

```php
Route::get('/beranda', [LatihanController::class,'beranda']);
```

Sekarang coba dijalankan lagi dengan php artisan serve dan akses http://toko-klowor.local/beranda

Maka hasilnya akan tampil seperti gambar berikut

![image-20220324205623130](img\image-20220324205623130.png)

Jadi di dalam file controller, untuk memanggil file view kita menggunakan perintah **return view('nama-filenya-tanpa [.blade.php]', variabel-data-yang-mau-dimasukkan);**

Terus, kalau data sudah dimasukkan gimana cara menampilkannya ke file beranda.blade.php yang kita buat tadi??

Ada 2 pilihan, mau pake skrip php pada umumnya seperti ini.

```php+HTML
<p>
  Kata ini datanya dari controller dengan variabel nama yang isinya : <?php echo $nama;?>
</p>
```

atau pake bawaannya blade yang pasti lebih ringkas dan mudah. Cukup diapit tanda kurung kurawal **{{ $variabel }}** seperti yang kita buat sebelumnya.

Selain menampilkan data dari variabel tadi, ada fungsi lainnya yang akan kita gunakan di tutorial-tutorial selanjutnya.

**# Extending Layout**

Nanti kita akan memakainya saat membuat file view. Sebagai contoh, di setiap halaman yang kita buat pastinya kan ada bagian menu. Daripada kita mengulang-ulang kode menu, kita buat sebuah template. Lebih jelasnya kita akan praktikan pada tutorial setelah ini. Contoh sederhana kode templatenya.

```php+HTML
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $title }}</title>
</head>
<body>
    <!-- bagian menunya -->
    @include('menu')
    <!-- isinya mulai dari sini -->
    @yield('content')
    <!-- isinya sampai sini -->
</body>
</html>
```

Kemudian masing-masing view-nya isinya seperti ini.

```php
@extends('layouts.template')
@section('content')
<h1>Selamat Datang di Kelas</h1>
<p>
  Ini adalah paragraph
</p>
<p>
  Kata ini datanya dari controller dengan variabel nama yang isinya : {{ $nama }}
</p>
@endsection
```

**# FUNGSI IF**

Di dalam blade, fungsi if sedikit beda dengan if pada skrip php. Fungsi if di blade menggunakan awalan @ (@if, @else, @elseif, @endif) dan tanpa menggunakan kurung kurawal.

contoh.

```php
@if(kondisi)
dijalankan kalau kondisi terpenuhi
@else
dijalankan kalau kondisi tidak terpenuhi
@endif
```

**# FUNGSI SWITCH**

Switch digunakan untuk membuat pilihan dengan kondisi yang banyak yang semisal dibuat menggunakan if-else akan merepotkan.

Contoh.

```php
@switch($i)
    @case(1)
        Dijalankan kalau nilai $i adalah 1
        @break

    @case(2)
        Dijalankan kalau nilai $i adalah 2
        @break

    @default
        Dijalankan kalau nilai $i tidak ada dalam case
@endswitch
```

**# FUNGSI FOR**

Fungsi for digunakan untuk membuat pengulangan sepanjang nilai maksimal.

Contoh.

```php
@for($i = 0; $i < 10; $i++)
  Nilai i adalah {{ $i }}
@endfor
```

Dan masih banyak lagi, lebih lengkapnya baca di https://laravel.com/docs/9.x/blade

## Template Layout

Aplikasi toko online yang akan kita buat menggunakan https://getbootstrap.com/ sebagai framework CSSnya. Sebelumnya kita buat 1 lagi file controller dengan nama HomepageController dengan perintah ***php artisan make:controller HomepageController***

Kemudian buat 1 buah function dengan nama index.

![image-20220324205936766](img\image-20220324205936766.png)

```php
public function index() {
    $data = array('title' => 'Homepage');
    return view('homepage.index', $data);
}
```

Kemudian buatlah 1 buah folder di dalam folder views dengan nama **homepage**, dan di dalam homepage buat 1 buah file dengan nama **index.blade.php** dan isi dengan kode seperti berikut.

![image-20220324210129254](img\image-20220324210129254.png)

```php+HTML
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <!-- fontawesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <title>{{ $title }}</title>
  </head>
  <body>
    <!-- menunya kita taruh persis di bawah <body> -->
    @include('layouts.menu')
    <!-- di bawah menu baru kontennya -->

    <!-- Mulai sini kontennya depannya kasih @ sama yield-->
    @yield('content')
    <!-- Sampai sini -->

    @include('layouts.footer')
    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

  </body>
</html>
```

File web.php kita sesuaikan dengan mengkomen semua route, dan sisakan 1 route untuk mengarah ke controller yang baru kita buat, menjadi seperti berikut.

```php+HTML
<?php

use App\Http\Controllers\HomepageController;
use App\Http\Controllers\LatihanController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomepageController::class,'index']);

// Route::get('/', function () {
//     return view('welcome');
// });
// Route::get('/halo', function () {
//     return "Halo nama saya fadlur";
// });
// Route::get('/latihan', [LatihanController::class,'index']);
// Route::get('/blog/{id}', [LatihanController::class,'blog']);
// Route::get('/blog/{idblog}/komentar/{idkomentar}',[LatihanController::class,'komentar']);
// Route::get('/beranda', [LatihanController::class,'beranda']);

```

Dari file index.blade.php yang kita buat tadi, sekarang kita ubah menjadi template. Caranya, buat 1 buah folder di dalam folder view dengan nama ***layouts***

![image-20220324210346125](img\image-20220324210346125.png)

Kemudian di dalam folder layouts kita buat 1 buah file dengan nama **template.blade.php**

Kodenya copy paste aja dari file **index.blade.php** dan sesuaikan seperti di bawah ini.

```php+HTML
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <title>{{ $title }}</title>
</head>

<body>
    <!-- menunya kita taruh persis di bawah <body> -->
    @include('layouts.menu')
    <!-- di bawah menu baru kontennya -->

    <!-- Mulai sini kontennya depannya kasih @ sama yield-->
    @yield('content')
    <!-- Sampai sini -->

    @include('layouts.footer')
    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous">
    </script>

    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js"
        integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous">
    </script>
    -->
</body>

</html>
```

Menu sama footernya belum kita buat, sekarang kita buat menunya dulu. buat 1 buah file di dalam folder **layouts** dengan nama **menu.blade.php**

```php+HTML
<nav class="navbar navbar-expand-lg navbar-light bg-light mb-4">
    <div class="container">
        <a class="navbar-brand" href="/">Toko</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="mr-auto navbar-nav"></ul>
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Produk</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Kategori</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Kontak</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Tentang Kami</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Login</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
```

Tambahkan 1 buah file lagi di folder layouts juga dengan nama **footer.blade.php**

```php+HTML
<div class="container">
    <div class="row">
        <div class="col">
            <hr />
            <p>&copy Copyright Toko Klowor Powered by DevLover - 2020</p>
        </div>
    </div>
</div>
```

Terakhir ubah file **index.blade.php** yang berada di dalam folder **homepage** menjadi seperti ini.

```php+HTML
@extends('layouts.template')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <h1>Hello world</h1>
            </div>
        </div>
    </div>
@endsection
```

Akses dengan alamat http://toko-klowor.local maka hasilnya akan tampil seperti di bawah ini.

![image-20220324210937123](img\image-20220324210937123.png)

## Desain Home

Template telah selesai kita buat, selanjutnya kita bisa mendesain halaman home, about (tentang kami) dan kontak. Langkah pertama, kita buat halaman home dulu. Di halaman home nantinya ada slider image dan juga beberapa foto produk, oleh karena itu download beberapa gambar di https://pxhere.com/ . Ambil 3 buah gambar dulu kemudian rename menjadi slide1.jpg slide2.jpg dan slide3.jpg. 

Selanjutnya buat folder dengan nama **images** di dalam folder **public** terus copy paste 3 gambar tadi ke dalam folder **images** tersebut.

![image-20220324211534899](img\image-20220324211534899.png)

Sekarang edit file views di **views/homepage/index.blade.php** seperti berikut.

```php+HTML
@extends('layouts.template')
@section('content')
    <div class="container">
        <!-- carousel -->
        <div class="row">
            <div class="col">
                <div id="carousel" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="{{ asset('images/slide1.jpg') }}" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="{{ asset('images/slide2.jpg') }}" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="{{ asset('images/slide3.jpg') }}" class="d-block w-100" alt="...">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        <!-- end carousel -->
    </div>
@endsection

```

Kemudian jalankan laravel dan akses di halaman http://toko-klowor.local/ seharusnya tampil carousel atau slideshow seperti gambar berikut.

![image-20220324211809866](img\image-20220324211809866.png)

**# DAFTAR KATEGORI**

Di bawah carousel, kita tambahkan daftar kategori yang tersedia dengan menambahkan beberapa baris kode di bawah `<!-- end carousel -->`

```php+HTML
<!-- kategori produk -->
  <div class="row mt-4">
    <div class="col col-md-12 col-sm-12 mb-4">
      <h2 class="text-center">Kategori Produk</h2>
    </div>
    <!-- kategori pertama -->
    <div class="col-md-4">
      <div class="card mb-4 shadow-sm">
        <a href="{{ URL::to('kategori/satu') }}">
          <img src="{{asset('images/slide1.jpg') }}" alt="foto kategori" class="card-img-top">
        </a>
        <div class="card-body">
          <a href="{{ URL::to('kategori/satu') }}" class="text-decoration-none">
            <p class="card-text">Kategori Pertama</p>
          </a>
        </div>
      </div>
    </div>
    <!-- kategori kedua -->
    <div class="col-md-4">
      <div class="card mb-4 shadow-sm">
        <a href="{{ URL::to('kategori/dua') }}">
          <img src="{{asset('images/slide2.jpg') }}" alt="foto kategori" class="card-img-top">
        </a>
        <div class="card-body">
          <a href="{{ URL::to('kategori/dua') }}" class="text-decoration-none">
            <p class="card-text">Kategori Kedua</p>
          </a>
        </div>
      </div>
    </div>
    <!-- kategori ketiga -->
    <div class="col-md-4">
      <div class="card mb-4 shadow-sm">
        <a href="{{ URL::to('kategori/tiga') }}">
          <img src="{{asset('images/slide3.jpg') }}" alt="foto kategori" class="card-img-top">
        </a>
        <div class="card-body">
          <a href="{{ URL::to('kategori/tiga') }}" class="text-decoration-none">
            <p class="card-text">Kategori Ketiga</p>
          </a>
        </div>
      </div>
    </div>
  </div>
  <!-- end kategori produk -->
```

**# PRODUK PROMO**

Setelah menambahkan kategori, selanjutnya tambahkan produk yang diberikan promo potongan harga. Tambahkan kode di bawah `<!-- end kategori produk -->`

```php+HTML
        <!-- produk Promo-->
        <div class="row mt-4">
            <div class="col col-md-12 col-sm-12 mb-4">
                <h2 class="text-center">Promo</h2>
            </div>
            <!-- produk pertama -->
            <div class="col-md-4">
                <div class="card mb-4 shadow-sm">
                    <a href="{{ URL::to('produk/satu') }}">
                        <img src="{{ asset('images/slide1.jpg') }}" alt="foto produk" class="card-img-top">
                    </a>
                    <div class="card-body">
                        <a href="{{ URL::to('produk/satu') }}" class="text-decoration-none">
                            <p class="card-text">
                                Produk Pertama
                            </p>
                        </a>
                        <div class="row mt-4">
                            <div class="col">
                                <button class="btn btn-light">
                                    <i class="far fa-heart"></i>
                                </button>
                            </div>
                            <div class="col-auto">
                                <p>
                                    <del>Rp. 15.000,00</del>
                                    <br />
                                    Rp. 10.000,00
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- produk kedua -->
            <div class="col-md-4">
                <div class="card mb-4 shadow-sm">
                    <a href="{{ URL::to('produk/dua') }}">
                        <img src="{{ asset('images/slide2.jpg') }}" alt="foto produk" class="card-img-top">
                    </a>
                    <div class="card-body">
                        <a href="{{ URL::to('produk/dua') }}" class="text-decoration-none">
                            <p class="card-text">
                                Produk Kedua
                            </p>
                        </a>
                        <div class="row mt-4">
                            <div class="col">
                                <button class="btn btn-light">
                                    <i class="far fa-heart"></i>
                                </button>
                            </div>
                            <div class="col-auto">
                                <p>
                                    <del>Rp. 15.000,00</del>
                                    <br />
                                    Rp. 10.000,00
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- produk ketiga -->
            <div class="col-md-4">
                <div class="card mb-4 shadow-sm">
                    <a href="{{ URL::to('produk/tiga') }}">
                        <img src="{{ asset('images/slide3.jpg') }}" alt="foto produk" class="card-img-top">
                    </a>
                    <div class="card-body">
                        <a href="{{ URL::to('produk/tiga') }}" class="text-decoration-none">
                            <p class="card-text">
                                Produk Ketiga
                            </p>
                        </a>
                        <div class="row mt-4">
                            <div class="col">
                                <button class="btn btn-light">
                                    <i class="far fa-heart"></i>
                                </button>
                            </div>
                            <div class="col-auto">
                                <p>
                                    <del>Rp. 15.000,00</del>
                                    <br />
                                    Rp. 10.000,00
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end produk promo -->
```

**# PRODUK TERBARU**

Setelah produk promo, kita tambahkan lagi kode untuk menampilkan produk-produk terbaru. Tambahkan lagi kode di bawah `<!-- end produk promo -->`

```php
        <!-- produk Terbaru-->
        <div class="row mt-4">
            <div class="col col-md-12 col-sm-12 mb-4">
                <h2 class="text-center">Terbaru</h2>
            </div>
            <!-- produk pertama -->
            <div class="col-md-4">
                <div class="card mb-4 shadow-sm">
                    <a href="{{ URL::to('produk/satu') }}">
                        <img src="{{ asset('images/slide1.jpg') }}" alt="foto produk" class="card-img-top">
                    </a>
                    <div class="card-body">
                        <a href="{{ URL::to('produk/satu') }}" class="text-decoration-none">
                            <p class="card-text">
                                Produk Pertama
                            </p>
                        </a>
                        <div class="row mt-4">
                            <div class="col">
                                <button class="btn btn-light">
                                    <i class="far fa-heart"></i>
                                </button>
                            </div>
                            <div class="col-auto">
                                <p>
                                    Rp. 10.000,00
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- produk kedua -->
            <div class="col-md-4">
                <div class="card mb-4 shadow-sm">
                    <a href="{{ URL::to('produk/dua') }}">
                        <img src="{{ asset('images/slide2.jpg') }}" alt="foto produk" class="card-img-top">
                    </a>
                    <div class="card-body">
                        <a href="{{ URL::to('produk/dua') }}" class="text-decoration-none">
                            <p class="card-text">
                                Produk Kedua
                            </p>
                        </a>
                        <div class="row mt-4">
                            <div class="col">
                                <button class="btn btn-light">
                                    <i class="far fa-heart"></i>
                                </button>
                            </div>
                            <div class="col-auto">
                                <p>
                                    Rp. 10.000,00
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- produk ketiga -->
            <div class="col-md-4">
                <div class="card mb-4 shadow-sm">
                    <a href="{{ URL::to('produk/tiga') }}">
                        <img src="{{ asset('images/slide3.jpg') }}" alt="foto produk" class="card-img-top">
                    </a>
                    <div class="card-body">
                        <a href="{{ URL::to('produk/tiga') }}" class="text-decoration-none">
                            <p class="card-text">
                                Produk Ketiga
                            </p>
                        </a>
                        <div class="row mt-4">
                            <div class="col">
                                <button class="btn btn-light">
                                    <i class="far fa-heart"></i>
                                </button>
                            </div>
                            <div class="col-auto">
                                <p>
                                    Rp. 10.000,00
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end produk terbaru -->
```

**# TENTANG TOKO**

Di halaman depan, selain menampilkan produk dan kategori. Kita bisa mamasukkan ringkasan profil toko dengan menambahkan kode di bawah `<!-- end produk terbaru -->`

```php
        <!-- tentang toko -->
        <hr>
        <div class="row mt-4">
            <div class="col">
                <h5 class="text-center">Toko Klowor Menggunakan Laravel</h5>
                <p>
                    Toko adalah demo membangun toko online menggunakan laravel framework. Di dalam demo ini terdapat user bisa menginput data kategori, produk dan transaksi.
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Hic laborum aliquam dolorum sequi nulla maiores quos incidunt veritatis numquam suscipit. Cumque dolore rem  obcaecati. Eos quod ad non veritatis assumenda.
                </p>
                <p>
                    Toko adalah demo membangun toko online menggunakan laravel framework. Di dalam demo ini terdapat user
                    bisa menginput data kategori, produk dan transaksi.
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Hic laborum aliquam dolorum sequi nulla
                    maiores quos incidunt veritatis numquam suscipit. Cumque dolore rem obcaecati. Eos quod ad non veritatis
                    assumenda.
                </p>
                <p class="text-center">
                    <a href="" class="btn btn-outline-secondary">
                        Baca Selengkapnya
                    </a>
                </p>
            </div>
        </div>
        <!-- end tentang toko -->
```

![image-20220324212411919](img\image-20220324212411919.png)

## Desain Halaman Profil dan Kontak

Setelah halaman home selesai kita buat, selanjutnya kita buat halaman untuk about (tentang kami) dan kontak. Buka **HomepageController.php** kemudian tambahkan 2 function sehingga menjadi seperti berikut.

```php
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomepageController extends Controller
{
    public function index()
    {
        $data = array('title' => 'Homepage');
        return view('homepage.index', $data);
    }
    public function about()
    {
        $data = array('title' => 'Tentang Kami');
        return view('homepage.about', $data);
    }

    public function kontak()
    {
        $data = array('title' => 'Kontak Kami');
        return view('homepage.kontak', $data);
    }
}

```

Kemudian tambahkan 2 file di dalam folder `views/homepage`, pertama **about.blade.php** dan isi dengan kode.

```php
@extends('layouts.template')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <h1>Tentang Kami</h1>
            </div>
        </div>
    </div>
@endsection

```

yang kedua adalah **kontak.blade.php** dan isi dengan kode

```php
@extends('layouts.template')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <h1>Kontak Kami</h1>
            </div>
        </div>
    </div>
@endsection

```

Sementara kita kosongi aja dulu, nanti kita edit lagi.

Setelah controller dan viewsnya sudah kita buat, selanjutnya adalah menambahkan route ke file **web.php** dengan menambahkan 2 baris kode seperti berikut.

```php
Route::get('/about', [HomepageController::class,'about']);
Route::get('/kontak', [HomepageController::class,'kontak']);
```

Terakhir, buka file **views/layouts/menu.blade.php** kemudian edit pada menu kontak dan tentang kami menjadi seperti berikut.

```php+HTML
<nav class="navbar navbar-expand-lg navbar-light bg-light mb-4">
    <div class="container">
        <a class="navbar-brand" href="/">Toko</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="mr-auto navbar-nav"></ul>
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="/">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Produk</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Kategori</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL::to('kontak') }}">Kontak</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL::to('about') }}">Tentang Kami</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Login</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
```

Setelah selesai diedit, akses menu kontak dan tentang kami.

![image-20220324213011417](img\image-20220324213011417.png)

![image-20220324213031215](img\image-20220324213031215.png)

## Desain Halaman Produk

Pada tutorial kali ini, kita akan membuat halaman untuk menampilkan semua produk, kategori dan juga produk per kategori. Kali ini kita akan menambahkan 3 buah function di **HomepageController.php**.

**# Kategori Produk**

Pada halaman kategori produk, halaman akan kita isi dengan 2 section. Pada section atau bagian atas akan kita isi daftar kategori yang kita masukkan, kemudian pada bagian bawahnya kita tambahkan beberapa produk terbaru yang kita masukkan.

Buka HomepageController.php dan tambahkan kode berikut di bawah function kontak().

```php
public function kategori() {
    $data = array('title' => 'Kategori Produk');
    return view('homepage.kategori', $data);
}
```

Dan pada folder **views/homepage** tambahkan 1 buah file dan beri nama **kategori.blade.php**.

```php+HTML
@extends('layouts.template')
@section('content')
    <div class="container">
        <!-- kategori produk -->
        <div class="row mt-4">
            <div class="col col-md-12 col-sm-12 mb-4">
                <h2 class="text-center">Kategori Produk</h2>
            </div>
            <!-- kategori pertama -->
            <div class="col-md-4">
                <div class="card mb-4 shadow-sm">
                    <a href="{{ URL::to('kategori/satu') }}">
                        <img src="{{ asset('images/slide1.jpg') }}" alt="foto kategori" class="card-img-top">
                    </a>
                    <div class="card-body">
                        <a href="{{ URL::to('kategori/satu') }}" class="text-decoration-none">
                            <p class="card-text">Kategori Pertama</p>
                        </a>
                    </div>
                </div>
            </div>
            <!-- kategori kedua -->
            <div class="col-md-4">
                <div class="card mb-4 shadow-sm">
                    <a href="{{ URL::to('kategori/dua') }}">
                        <img src="{{ asset('images/slide1.jpg') }}" alt="foto kategori" class="card-img-top">
                    </a>
                    <div class="card-body">
                        <a href="{{ URL::to('kategori/dua') }}" class="text-decoration-none">
                            <p class="card-text">Kategori Kedua</p>
                        </a>
                    </div>
                </div>
            </div>
            <!-- kategori ketiga -->
            <div class="col-md-4">
                <div class="card mb-4 shadow-sm">
                    <a href="{{ URL::to('kategori/tiga') }}">
                        <img src="{{ asset('images/slide1.jpg') }}" alt="foto kategori" class="card-img-top">
                    </a>
                    <div class="card-body">
                        <a href="{{ URL::to('kategori/tiga') }}" class="text-decoration-none">
                            <p class="card-text">Kategori Ketiga</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- end kategori produk -->
        <!-- produk Terbaru-->
        <div class="row mt-4">
            <div class="col col-md-12 col-sm-12 mb-4">
                <h2 class="text-center">Terbaru</h2>
            </div>
            <!-- produk pertama -->
            <div class="col-md-4">
                <div class="card mb-4 shadow-sm">
                    <a href="{{ URL::to('produk/satu') }}">
                        <img src="{{ asset('images/slide2.jpg') }}" alt="foto produk" class="card-img-top">
                    </a>
                    <div class="card-body">
                        <a href="{{ URL::to('produk/satu') }}" class="text-decoration-none">
                            <p class="card-text">
                                Produk Pertama
                            </p>
                        </a>
                        <div class="row mt-4">
                            <div class="col">
                                <button class="btn btn-light">
                                    <i class="far fa-heart"></i>
                                </button>
                            </div>
                            <div class="col-auto">
                                <p>
                                    Rp. 10.000,00
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- produk kedua -->
            <div class="col-md-4">
                <div class="card mb-4 shadow-sm">
                    <a href="{{ URL::to('produk/dua') }}">
                        <img src="{{ asset('images/slide2.jpg') }}" alt="foto produk" class="card-img-top">
                    </a>
                    <div class="card-body">
                        <a href="{{ URL::to('produk/dua') }}" class="text-decoration-none">
                            <p class="card-text">
                                Produk Kedua
                            </p>
                        </a>
                        <div class="row mt-4">
                            <div class="col">
                                <button class="btn btn-light">
                                    <i class="far fa-heart"></i>
                                </button>
                            </div>
                            <div class="col-auto">
                                <p>
                                    Rp. 10.000,00
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- produk ketiga -->
            <div class="col-md-4">
                <div class="card mb-4 shadow-sm">
                    <a href="{{ URL::to('produk/tiga') }}">
                        <img src="{{ asset('images/slide2.jpg') }}" alt="foto produk" class="card-img-top">
                    </a>
                    <div class="card-body">
                        <a href="{{ URL::to('produk/tiga') }}" class="text-decoration-none">
                            <p class="card-text">
                                Produk Ketiga
                            </p>
                        </a>
                        <div class="row mt-4">
                            <div class="col">
                                <button class="btn btn-light">
                                    <i class="far fa-heart"></i>
                                </button>
                            </div>
                            <div class="col-auto">
                                <p>
                                    Rp. 10.000,00
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end produk terbaru -->
    </div>
@endsection

```

Terakhir tambahkan route ke file **web.php**

```php
Route::get('/kategori', [HomepageController::class, 'kategori']);
```

Terakhir, buka file **views/layouts/menu.blade.php** kemudian edit pada menu kontak dan tentang kami menjadi seperti berikut.

```php+HTML
<nav class="navbar navbar-expand-lg navbar-light bg-light mb-4">
    <div class="container">
        <a class="navbar-brand" href="/">Toko</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="mr-auto navbar-nav"></ul>
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="/">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Produk</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL::to('kategori') }}">Kategori</a>
                </li>
                <li class="nav-item">a
                    <a class="nav-link" href="{{ URL::to('kontak') }}">Kontak</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL::to('about') }}">Tentang Kami</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Login</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

```

Akses menu kategori

![image-20220324213637657](img\image-20220324213637657.png)

## Template Dashboard

Dashboard yang kita buat menggunakan template dari https://adminlte.io/. Download versi terbarunya kemudian exstract. Setelah di exstract, copy paste isi dari folder dist (**css, img, js**) ke dalam folder **toko/public**.

![image-20220324213938858](img\image-20220324213938858.png)

Selanjutnya buka folder plugins di folder hasil exstract tadi, kemudian copy 3 folder (**bootstrap, fontawesome-free, dan jquery**) ke dalam folder **toko/public/js**. Semua folder yang dibutuhkan sudah dipersiapkan di folder public. Kemudian ikuti langkah selanjutnya di bawah ini.

![image-20220324214051519](img\image-20220324214051519.png)

**# Buat DashboardController**

Buka terminal di visual studio code, kemudian buatlah 1 buah controller dengan perintah

```php
php artisan make:controller DashboardController
```

Kemudian pencet enter. Kemudian buka file **DashboardController.php** tadi dan edit menjadi seperti kode di bawah ini.

```php
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    //
    public function index()
    {
        $data = array('title' => 'Dashboard');
        return view('dashboard.index', $data);
    }
}

```

Kalau sudah, sekarang lanjut ke langkah selanjutnya.

**# Tambahkan Route**

Buka file routes/web.php dan pada baris paling bawah, tambahkan kode ini.

```php
<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HomepageController;
use App\Http\Controllers\LatihanController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomepageController::class, 'index']);
Route::get('/about', [HomepageController::class, 'about']);
Route::get('/kontak', [HomepageController::class, 'kontak']);
Route::get('/kategori', [HomepageController::class, 'kategori']);
Route::group(['prefix' => 'admin'], function() {
    Route::get('/', [DashboardController::class, 'index']);
  });

// Route::get('/', function () {
//     return view('welcome');
// });
// Route::get('/halo', function () {
//     return "Halo nama saya fadlur";
// });
// Route::get('/latihan', [LatihanController::class,'index']);
// Route::get('/blog/{id}', [LatihanController::class,'blog']);
// Route::get('/blog/{idblog}/komentar/{idkomentar}',[LatihanController::class,'komentar']);
// Route::get('/beranda', [LatihanController::class,'beranda']);

```

Pada langkah selanjutnya kita akan membuat template dashboardnya.

**# Buat Menu Dashboard**

Buat 1 buah file baru di dalam folder **views/layouts** dengan nama **menudashboard.blade.php** dan isi dengan kode berikut.

```php+HTML
<nav class="mt-2">
  <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
    <!-- Add icons to the links using the .nav-icon class
          with font-awesome or any other icon font library -->
    <li class="nav-item">
      <a href="#" class="nav-link">
        <i class="nav-icon fas fa-th"></i>
        <p>
          Dashboard
        </p>
      </a>
    </li>
    <li class="nav-item has-treeview">
      <a href="#" class="nav-link">
        <i class="nav-icon fas fa-folder-open"></i>
        <p>
          Produk
          <i class="right fas fa-angle-left"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">
        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Active Page</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Inactive Page</p>
          </a>
        </li>
      </ul>
    </li>
    <li class="nav-item has-treeview">
      <a href="#" class="nav-link">
        <i class="nav-icon fas fa-shopping-cart"></i>
        <p>
          Transaksi
          <i class="right fas fa-angle-left"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">
        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Active Page</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Inactive Page</p>
          </a>
        </li>
      </ul>
    </li>
    <li class="nav-item has-treeview">
      <a href="#" class="nav-link">
        <i class="nav-icon fas fa-list"></i>
        <p>
          Laporan
          <i class="right fas fa-angle-left"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">
        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Penjualan</p>
          </a>
        </li>
      </ul>
    </li>
    <li class="nav-item">
      <a href="#" class="nav-link">
        <i class="nav-icon fas fa-sign-out-alt"></i>
        <p>
          Sign Out
        </p>
      </a>
    </li>
  </ul>
</nav>
```

**# Template Dashboard**

Terakhir buat 1 buah file di dalam folder **views/layouts** dengan nama **dashboard.blade.php**

Karena kodenya agak panjang, copy paste saja kode di bawah ini.

```php+HTML
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>{{ $title }}</title>

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ asset('js/fontawesome-free/css/all.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('css/adminlte.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i
                            class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="/" class="nav-link">Home</a>
                </li>
            </ul>


            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <!-- Notifications Dropdown Menu -->
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="far fa-bell"></i>
                        <span class="badge badge-warning navbar-badge">15</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <span class="dropdown-header">15 Notifications</span>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <i class="fas fa-envelope mr-2"></i> 4 new messages
                            <span class="float-right text-muted text-sm">3 mins</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <i class="fas fa-users mr-2"></i> 8 friend requests
                            <span class="float-right text-muted text-sm">12 hours</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <i class="fas fa-file mr-2"></i> 3 new reports
                            <span class="float-right text-muted text-sm">2 days</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
                    </div>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="index3.html" class="brand-link">
                <img src="{{ asset('img/AdminLTELogo.png') }}" alt="AdminLTE Logo"
                    class="brand-image img-circle elevation-3" style="opacity: .8">
                <span class="brand-text font-weight-light">AdminLTE 3</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <img src="{{ asset('img/user2-160x160.jpg') }}" class="img-circle elevation-2"
                            alt="User Image">
                    </div>
                    <div class="info">
                        <a href="#" class="d-block">Alexander Pierce</a>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                @include('layouts.menudashboard')
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">{{ $title }}</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Starter Page</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                @yield('content')
            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Main Footer -->
        <footer class="main-footer">
            <!-- To the right -->
            <div class="float-right d-none d-sm-inline">
                Anything you want
            </div>
            <!-- Default to the left -->
            <strong>Copyright &copy; 2022 <a href="#">JorKloworDev</a>.</strong> All rights
            reserved.
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->

    <!-- jQuery -->
    <script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('js/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('js/adminlte.min.js') }}"></script>
</body>

</html>

```

**# Terakhir**

Terakhir, buatlah 1 buah folder di dalam folder views dengan nama **dashboard**. Kemudian tambahkan 1 buah file dengan nama **index.blade.php** dan isi dengan kode berikut.

```php+HTML
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <h1>Ini Dashboard</h1>
            </div>
        </div>
    </div>
@endsection

```

Setelah semua siap, sekarang jalankan dengan perintah php artisan serve dan akses di alamat http://toko-klowor.local/admin

Hasilnya akan tampak seperti gambar di bawah ini.

![image-20220324214700592](img\image-20220324214700592.png)

## Position di CSS

Properti position menentukan jenis metode pemosisian yang digunakan untuk suatu elemen.

Ada lima nilai posisi yang berbeda pada properti position yaitu :

- static
- relative
- fixed
- absolute
- sticky

Elemen kemudian diposisikan menggunakan properti **top**, **bottom**, **left**, dan **right**. Namun properti t**top**, **bottom**, **left**, dan **right** tidak akan berfungsi kecuali properti position diatur terlebih dahulu. Properti **top**, **bottom**, **left**, dan **right** juga memilki fungsi berbeda tergantung pada nilai posisi.

## position: static;

Elemen pada kode HTML akan diposisikan static secara default.

Elemen yang diposisikan static (default) tidak terpengaruh oleh properti **top**, **bottom**, **left**, dan **right**

Sebuah elemen dengan properti position: `static` tidak diposisikan dengan cara apapun selalu diposisikan sesuai dengan alur normal halaman:

Berikut adalah contoh penggunaannya pada CSS. Buat file **position_static.html**:

```html
<!DOCTYPE html>
<html>
<head>
<style>
div.static {
  position: static;
  border: 3px solid #73AD21;
}
</style>
</head>
<body>

<h2>position: static;</h2>

<p>Elemen dengan position: static tidak diposisikan dengan cara apapun selalu diposisikan sesuai dengan alur normal halaman</p>

<div class="static">
 Elemen div ini memiliki setelan position: static;
</div>

</body>
</html>
```

## position: relative;

Sebuah elemen dengan position: relative diposisikan secara relative terhadap posisi normalnya.

Menyetel properti **top**, **bottom**, **left**, dan **right** dari elemen yang posisinya relatif akan menyebabkannya posisi elemen menjauh dari posisi normalnya. Konten lain tidak akan menyesuikan dengan celah yang ditinggalkan oleh elemen.

Berikut contoh penggunaan position: `relative` pada CSS. Buat file **position_relative.html**:

```html
<!DOCTYPE html>
<html>
<head>
<style>
div.relative {
  position: relative;
  left: 30px;
  border: 3px solid #73AD21;
}
</style>
</head>
<body>

<h2>position: relative;</h2>

<p>Sebuah elemen dengan position: relative diposisikan secara relatif terhadap posisi normalnya.</p>

<div class="relative">
Elemen div ini memiliki setelan position: relative;
</div>

</body>
</html>
```

## position: fixed;

Sebuah elemen dengan position: fixed; diposisikan relati terhadap viewport, yang berarti elemen akan selalu di tempat yang sama meskipun halaman di-scroll. Properti properti **top**, **bottom**, **left**, dan **right** digunakan untuk memposisikan elemen.

Elemen fixed tidak meninggalkan celah pada halaman dimana tempat biasanya elemen tersebut ditempatkan.

Perhatikan elemen `fixed` di pojok kanan bawah halaman.

Berikut adalah contoh penggunannya pada CSS. Buat file **position_fixed.html**:

```html
<!DOCTYPE html>
<html>
<head>
<style>
div.fixed {
  position: fixed;
  bottom: 0;
  right: 0;
  width: 300px;
  border: 3px solid #73AD21;
}
</style>
</head>
<body>

<h2>position: fixed;</h2>

<p>Sebuah elemen dengan position: fixed; diposisikan relatif terhadap viewport, yang berarti elemen akan selalu di tempat yang sama meskipun halaman di-scroll:</p>

<div class="fixed">
Elemen ini memiliki pengaturan position: fixed;
</div>

</body>
</html>
```

## position: absolute;

Sebuah elemen dengan position: `absolute` diposisikan relatif terhadap posisi leluhur terdekat (bukan diposisikan relatif terhadap viewport, seperti fixed).

Namun apabila elemen yang diposisikan absolute tidak memiliki leluhur yang diposisikan, posisi absolut akan menggunakan badan dokumen, dan bergerak bersama dengan scroll bar pada halaman.

Catatan: Elemen “positioned” adalah semua jenis elemen yang diposisikan apapun kecuali statis.

Berikut ini contoh sederhananya. Buat file **position_absolute.html**:

```html
<!DOCTYPE html>
<html>
<head>
<style>
div.relative {
  position: relative;
  width: 400px;
  height: 200px;
  border: 3px solid #73AD21;
} 

div.absolute {
  position: absolute;
  top: 80px;
  right: 0;
  width: 200px;
  height: 100px;
  border: 3px solid #73AD21;
}
</style>
</head>
<body>

<h2>position: absolute;</h2>

<p>Sebuah elemen dengan position: absolute diposisikan relatif terhadap posisi leluhur terdekat (bukan diposisikan relatif terhadap viewport, seperti fixed):</p>

<div class="relative">Elemen div ini memeilki position: relative;
  <div class="absolute">Elemen div ini memeilki position: absolute;</div>
</div>

</body>
</html>
```

## position: sticky;

elemen dengan position: `sticky` diposisikan berdasarkan posisi scroll bar pengguna.

Elemen `sticky` adalah peralihan antara posisi relative dan fixed, bergantung pada posisi scroll bar. Elemen sticky diposisikan relative hingga posisi offset tertentu terpenuhi pada viewport – lalu menempel (stick) pada tempatnya (seperti position: fixed).

Dalam contoh ini, elemen sticky menempel di bagian atas halaman (top: 0) saat telah mencapai posisi scroll bar. Berikut ini contoh sederhananya. Buat file **position_sticky.html**:

```html
<!DOCTYPE html>
<html>
<head>
<style>
div.sticky {
  position: sticky;
  top: 0;
  padding: 5px;
  background-color: #cae8ca;
  border: 2px solid #4CAF50;
}
</style>
</head>
<body>

<p>Try to <b>scroll</b> Konten dalam frame ini bertujuan untuk memahami cara kerja position:sticky.</p>
<p>Catatan: IE / Edge 15 dan versi sebelumnya tidak mendukung position:sticky.</p>

<div class="sticky">Aku sticky!</div>

<div style="padding-bottom:2000px">
  <p>Dalam contoh ini, elemen sticky menempel di bagian atas halaman (top: 0) saat telah mencapai posisi scroll bar.</p>
  <p>Scroll kembali ke atas untuk menghilangkan stickyness.</p>
  <p>Teks untuk mengaktifkan scrolling.. Lorem ipsum dolor sit amet, illum definitiones no quo, maluisset concludaturque et eum, altera fabulas ut quo. Atqui causae gloriatur ius te, id agam omnis evertitur eum. Affert laboramus repudiandae nec et. Inciderint efficiantur his ad. Eum no molestiae voluptatibus.</p>
  <p>Teks untuk mengaktifkan scrolling.. Lorem ipsum dolor sit amet, illum definitiones no quo, maluisset concludaturque et eum, altera fabulas ut quo. Atqui causae gloriatur ius te, id agam omnis evertitur eum. Affert laboramus repudiandae nec et. Inciderint efficiantur his ad. Eum no molestiae voluptatibus.</p>
</div>

</body>
</html>
```

> **Catatan:**
>
> Internet Explorer, Edge 15 dan versi sebelumnya tidak mendukung position: sticky. Lalu pada Safari membutuhkan -webkit- prefix. Penentuan position **top**, **bottom**, **left**, dan **right** juga harus dilakukan setidaknya satu posisi tersebut agar position: `sticky` berfungsi.

## Element Overlapping

Saat elemen diposisikan, elemen-elemen tersebut bisa saja tumpang tindih (overlap) dengan elemen lain.

Properti z-index menentukan urutan tumpukan suatu elemen (elemen mana yang harus ditempatkan di depan, atau di belakang, yang lain).

Sebuah elemen dapat memiliki urutan tumpukan positif atau negatif.  Berikut ini contoh sederhananya. Buat file **position_overlapping.html**:

```html
<!DOCTYPE html>
<html>
<head>
<style>
img {
  position: absolute;
  left: 0px;
  top: 0px;
  z-index: -1;
}
</style>
</head>
<body>

<h1>Ini heading</h1>
<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRvjoTKQiymuk5AaI1mk5B3mr8EjyxnWHiN9Q&usqp=CAU" width="100" height="140">
<p>Karena gambar memiliki z-index -1, gambar akan ditempatkan di belakang teks.</p>

</body>
</html>
```

Elemen dengan urutan tumpukan lebih besar selalu berada di depan elemen dengan urutan tumpukan lebih rendah.

> **Catatan:** 
>
> Jika dua elemen yang diposisikan tumpang tindih tanpa z-index yang ditentukan, elemen yang diposisikan terakhir dalam kode HTMLnya akan ditampilkan di atas.

## Posisi Teks pada Gambar

Berikut adalah contoh-contoh memposisikan teks pada sebuah gambar.

Cara memposisikan teks atas gambar (posisi top left). Buat file **position_gambar.html**:

```html
<!DOCTYPE html>
<html>
<head>
<style>
.container {
  position: relative;
}

.topleft {
  position: absolute;
  top: 8px;
  left: 16px;
  font-size: 18px;
}

img { 
  width: 100%;
  height: auto;
  opacity: 0.3;
}
</style>
</head>
<body>

<h2>Teks pada Gambar</h2>
<p>Tambahkan beberapa teks ke gambar di pojok kiri atas:</p>

<div class="container">
  <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRvjoTKQiymuk5AaI1mk5B3mr8EjyxnWHiN9Q&usqp=CAU" alt="Cat" width="1000" height="300">
  <div class="topleft">Top Left</div>
</div>

</body>
</html>
```

Cara memposisikan teks atas gambar (posisi top right).  Buat file **position_gambar2.html**:

```html
<!DOCTYPE html>
<html>
<head>
<style>
.container {
  position: relative;
}

.topright {
  position: absolute;
  top: 8px;
  right: 16px;
  font-size: 18px;
}

img { 
  width: 100%;
  height: auto;
  opacity: 0.3;
}
</style>
</head>
<body>

<h2>Teks pada Gambar</h2>
<p>Tambahkan beberapa teks ke gambar di pojok kanan atas:</p>

<div class="container">
  <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRvjoTKQiymuk5AaI1mk5B3mr8EjyxnWHiN9Q&usqp=CAU" alt="Cat" width="1000" height="300">
  <div class="topright">Top Right</div>
</div>

</body>
</html>
```

Cara memposisikan teks atas gambar (posisi bottom left). Buat file **position_gambar3.html**:

```html
<!DOCTYPE html>
<html>
<head>
<style>
.container {
  position: relative;
}

.bottomleft {
  position: absolute;
  bottom: 8px;
  left: 16px;
  font-size: 18px;
}

img { 
  width: 100%;
  height: auto;
  opacity: 0.3;
}
</style>
</head>
<body>

<h2>Teks pada Gambar</h2>
<p>Tambahkan beberapa teks ke gambar di pojok kiri bawah:</p>

<div class="container">
  <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRvjoTKQiymuk5AaI1mk5B3mr8EjyxnWHiN9Q&usqp=CAU" alt="Cat" width="1000" height="300">
  <div class="bottomleft">Bottom Left</div>
</div>

</body>
</html>
```

Cara memposisikan teks atas gambar (posisi bottom right). Buat file **position_gambar4.html**:

```html
<!DOCTYPE html>
<html>
<head>
<style>
.container {
  position: relative;
}

.bottomright {
  position: absolute;
  bottom: 8px;
  right: 16px;
  font-size: 18px;
}

img { 
  width: 100%;
  height: auto;
  opacity: 0.3;
}
</style>
</head>
<body>

<h2>Teks pada Gambar</h2>
<p>Tambahkan beberapa teks ke gambar di pojok kanan bawah:</p>

<div class="container">
  <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRvjoTKQiymuk5AaI1mk5B3mr8EjyxnWHiN9Q&usqp=CAU" alt="Cat" width="1000" height="300">
  <div class="bottomright">Bottom Right</div>
</div>

</body>
</html>
```

Cara memposisikan teks atas gambar (posisi center). Buat file **position_gambar5.html**:

```html
<!DOCTYPE html>
<html>
<head>
<style>
.container {
  position: relative;
}

.center {
  position: absolute;
  top: 50%;
  width: 100%;
  text-align: center;
  font-size: 18px;
}

img { 
  width: 100%;
  height: auto;
  opacity: 0.3;
}
</style>
</head>
<body>

<h2>Teks pada Gambar</h2>
<p>Teks posisi Center di dalam gambar:</p>

<div class="container">
  <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRvjoTKQiymuk5AaI1mk5B3mr8EjyxnWHiN9Q&usqp=CAU" alt="Cat" width="1000" height="300">
  <div class="center">Centered</div>
</div>

</body>
</html>
```

## All CSS Positioning Properties

| Properti | Deskripsi                                             |
| -------- | ----------------------------------------------------- |
| bottom   | Mengatur tepi margin bawah untuk box yang diposisikan |
| clip     | Klip elemen yang diposisikan absolute                 |
| left     | Mengatur tepi margin kiri untuk box yang diposisikan  |
| right    | Mengatur tepi margin kanan untuk box yang diposisikan |
| top      | Mengatur tepi margin atas untuk box yang diposisikan  |
| z-index  | Menetapkan urutan tumpukan elemen                     |
| position | Menentukan jenis pemosisian untuk suatu elemen        |