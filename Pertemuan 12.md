# Hosting Toko Online di 000webhost

1. Terlebih dahulu, pastikan bahwa kamu sudah membuat website Laravel-nya. Jika sudah, kompres file website tersebut menjadi format zip. Nantinya, kita akan mengirim file ini kedalam media hosting kita.

   ![image-20220513141901615](img\image-20220513141901615.png)

   Kemudian kita export database aplikasi kita, buka **Navicat** kemudian klik kanan pilih seperti digambar

   ![image-20220513142032573](img\image-20220513142032573.png)

   Simpan pada direktori local kita

2. Masuk link berikut https://id.000webhost.com/login-cpanel. Lalu, pastikan bahwa kamu sudah terdaftar pada website 000webhost. Jika belum, maka silahkan daftar terlebih dahulu.

   ![image-20220513142344952](img\image-20220513142344952.png)

3. Klik Buat situs baru. Isi form yang ada(jangan sampai lupa)

   ![image-20220513142559449](img\image-20220513142559449.png)

4. Jika sudah, klik menu tools dan pilih database manager. 

   ![image-20220513142741973](img\image-20220513142741973.png)

   5. Buat Database baru

      ![image-20220513143017169](img\image-20220513143017169.png)

5. Selanjutnya kita menimport database yang dari aplikasi kita. Klik **PhpMyadmin**,

   ![image-20220513143115466](img\image-20220513143115466.png)

6. Klik nama database yang sudah kita buat, kemudian masuk ke tab **Import**.

   ![image-20220513143304817](img\image-20220513143304817.png)

   Pilih file yang sudah kita export, kemudian klik **Kirim**.

   Jika berhasil maka database akan seperti berikut.

   ![image-20220513143935416](img\image-20220513143935416.png)

7. Kemudia kita masuk ke menu **Tools** lagi, pilih file manager. Klik Upload.

   Jika pada proses ini internet time out, silahkan menggunakan VPN. https://chrome.google.com/webstore/detail/free-vpn-for-chrome-vpn-p/majdfhpaihoncoakbjgbdhglocklcgno

   ![image-20220513144048695](img\image-20220513144048695.png)

   Jika sudah masuk ke folder **public_html**

   ![image-20220513153551292](img\image-20220513153551292.png)

   Kemudian klik upload, pilih file. Klik Upload.

   ![image-20220513153705897](img\image-20220513153705897.png)

   Harus diingat! Masuk folder **public_html**. Jika sudah berhasil, Buat file baru bernama **extract.php** kemudian ubah file menjadi seperti berikut.

   ![image-20220513154144249](img\image-20220513154144249.png)

   ```php+HTML
   <?php  
        $zip = new ZipArchive;  
       //  sesuaikan nama file
        $res = $zip->open('toko-klowor.zip');  
        if ($res === TRUE) {  
            $zip->extractTo('');  
            $zip->close();  
            echo 'ok';  
        } else {  
            echo 'failed';  
        }  
   ?>  
   ```

8. Selanjutnya kita akses halaman website kita, kemudian akses **extract.php**. Contohnya seperti berikut: https://toko-klowor.000webhostapp.com/extract.php

   ![image-20220513155017892](img\image-20220513155017892.png)

   Klik **Unzip Archive**

9. Jika berhasil maka file manager pada hosting kita akan berubah menjadi seperti berikut:

    

