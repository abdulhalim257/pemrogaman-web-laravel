# Mengenal Framework Laravel Lanjut

Dalam pertemuan sebelumnya teman-teman pasti bingung, ini sedang buat apa? apa itu route, model, view maupun controller?! Dalam pertemuan kali ini akan dijelaskan tentang hal-hal tersebut.

## 1. Routing

Kebanyakan routes untuk aplikasi kamu, akan dideklarasikan pada file `routes/web.php` Routes Laravel yang paling sederhana terdiri dari URI dan callback. 

![image-20220317201225060](img\image-20220317201225060.png)

Contoh Route :

**Basic GET Route**

```
Route::get('/halo',function(){return'Hello World';});
```

Maka jika kita meng akses tampilan muncul adalah:

![image-20220317201521430](img\image-20220317201521430.png)

Nah pada pertemuan sebelumnya, kita sudah membuat route seperti berikut

```
Route::get('/test', function() {
     return view('master');
});
```

Hasilnya,

![image-20220310222742774](img\image-20220310222742774.png)

- `Route::get` -> fungsi get(mengambil)
- `'/test'` -> url yang diakses
-  `function() { return view('master'); });` -> fungsi yang digunakan untuk menampilkan view bernama master

#### Basic Route Parameters

```
Route::get('/detail/{id}', [ArtikelController::class, 'detail']);
```

Route menggunakan kunci, dalam hal ini `{id}` merupakan kunci yang dipakai.

Ada beberapa opsional dari route dari parameter, berikut adalah beberapa contohnya :

#### Optional Route Parameters

```
Route::get('user/{name?}',function($name =null){return $name;});
```

#### Optional Route Parameters dengan Defaults

```
Route::get('user/{name?}',function($name ='John'){return $name;});
```

Pada tutorial ini kita tidak akan secara detil belajar tentang routes. Untuk lebih lengkap, silakan kunjungi halaman [ini](http://laravel.com/docs/routing).

## 2. Model View Controller (MVC)

**MVC** adalah sebuah arsitektur perancangan kode program. Tujuannya untuk memecah kode program utama menjadi 3 komponen terpisah dengan tugas yang spesifik. Ketiga komponen tersebut adalah:

- Pengaksesan database, disebut sebagai **Model**.
- Tampilan design (user interface), disebut sebagai **View**.
- Alur logika program, disebut sebagai **Controller**.

Gabungan **Model-View-Controller** inilah yang disingkat sebagai **MVC**.

Ide awal dari perlunya konsep MVC adalah agar aplikasi yang dibuat bisa mudah dikelola dan dikembangkan, terutama untuk aplikasi besar.

Sebagai contoh, seorang web designer bisa fokus merancang bagian **View** saja, yakni tampilan design website yang terdiri dari kode HTML dan CSS plus sedikit JavaScript. Kode program untuk berkomunikasi dengan database bisa ditangani oleh programmer yang secara khusus bagian **Model**. Serta programmer lain mengatur alur logika program di bagian **Controller**.

Dengan pemisahan seperti ini, kerja tim menjadi mudah dikelola. Selain itu dengan penerapan konsep MVC yang baik, setiap bagian tidak saling bergantung sama lain. Jika ada perubahan atau modifikasi, cukup edit di bagian yang diperlukan saja, tidak harus merombak ulang semua aplikasi.

Dibalik keunggulan ini, kendala utama dari konsep **MVC** adalah cukup rumit untuk dipahami (terutama bagi pemula), serta file kode program menjadi banyak karena setiap bagian dari M-V-C harus ditulis dalam file terpisah.

Namun keuntungan yang didapat sebanding dengan “usaha” untuk mempelajari MVC tersebut, karena kode program kita menjadi lebih fleksibilitas dan mudah dikelola.

### Diagram Alur MVC

Agar lebih mudah dipahami, kita akan bahas menggunakan diagram berikut:

![Diagram Arsitektur MCV](img\Diagram-Arsitektur-MCV.png)

Ini adalah contoh alur yang terjadi dalam sebuah aplikasi yang menerapkan konsep MVC. Kita berangkat dari user yang sedang membuka sebuah web browser (sudut kanan bawah).

Dalam panah 1, setiap interaksi yang dilakukan user akan ditangani oleh **controller**. Misalnya ketika user mengetik alamat situs, maka sebuah controller di server duniailkom akan menangkap “request” tersebut. Atau ketika user selesai mengisi form register dan men-klik tombol submit, file controller akan menerima sebuah proses.

Controller pada dasarnya berisi logika program. Sekita inya perlu mengambil data dari database, maka controller akan memanggil **Model** (panah nomor 2). Model inilah yang bertanggung jawab mengakses database lalu mengembalikan hasilnya kembali ke controller.

Setelah data dari model diterima kembali, controller kemudian meneruskan data tersebut ke dalam **View** (panah 3). Data ini kemudian diproses sebagai kode HTML dan CSS di dalam view. Inilah yang dilihat oleh user di dalam web browser (panah 4).

Jika user men-klik halaman lain, maka itu akan diproses lagi oleh controller, yakni kembali ke langkah 1, demikian seterusnya alur kerja dari arsitektur MVC.

Dalam praktek arsitektur MVC di Laravel, nantinya juga terdapat komponen bernama **Route**, yang berfungsi untuk menyesuaikan alamat URL, serta “menyembunyikan” nama file URL. Route ini akan berada di panah 1, yakni tepat sebelum request dari user sampai ke controller.

1. Model

   Jika kita ingin membuat Model maka buka folder ini

   ![image-20220317203105020](img\image-20220317203105020.png)

   Atau kita mengetikkan pada cmd

   ```
   php artisan make:model <namaModel>
   ```

   nanti akan otomatis file model tersebut terbuat 

2. View

   Jika kita ingin membuat View maka buka folder ini

   ![image-20220317203252983](img\image-20220317203252983.png)

   View merupakan tampilan dari laravel, kemarin kita sudah membuat beberapa view seperti diatas

3. Controller

   Jika kita ingin membuat Controlller maka buka folder ini

   ![image-20220317203350386](img\image-20220317203350386.png)

Atau kita mengetikkan pada cmd

```
php artisan make:controller <namaController>
```

nanti akan otomatis file model tersebut terbuat, oke kita masuk ke tutorial selanjutnya

### 8. Membuat Halaman Admin

Untuk dapat mengatur hak akses di blog Laravel tersebut, kita perlu membuat halaman admin. Hal ini untuk mencegah pengguna yang tidak berhak menghapus atau mengedit artikel blog kita .

Langkah pertama, bukalah file **ArtikelController.php** pada folder **app/Http/Controllers** dan ubah kode menjadi seperti ini:

```php+HTML
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ArtikelController extends Controller
{
    public function show()
    {
        $articles = DB::table('artikel')->orderby('id', 'desc')->get();
        return view('show', ['articles'=>$articles]);
    }
    public function add()
    {
        return view('add');
    }
    public function add_process(Request $article)
    {
        DB::table('artikel')->insert([
            'judul'=>$article->judul,
            'deskripsi'=>$article->deskripsi
        ]);

        return redirect()->action([ArtikelController::class, 'show']);
    }
    public function detail($id)
    {
        $article = DB::table('artikel')->where('id', $id)->first();
        return view('detail', ['article'=>$article]);
    }
    public function show_by_admin()
    {
        $articles = DB::table('artikel')->orderby('id', 'desc')->get();
        return view('adminshow', ['articles'=>$articles]);
    }
}
?>
```

Setelah itu, kita bisa membuat file bernama **adminshow.blade.php**(view) yang akan menampilkan seluruh artikel dari sisi admin. Ubah kodenya menjadi sebagai berikut:

```php+HTML
<!-- menggunakan kerangka dari master.blade.php -->
@extends('master')

@section('header')
    <h2>
        <center>List Artikel</center>
    </h2>
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
    @endif

@endsection

@section('title', 'Halaman Khusus Admin')

@section('main')
    <div class="col-md-12 bg-white p-4">
        <a href="/add"><button class="btn btn-primary mb-3">Tambah Artikel</button></a>
        <table class="table table-responsive table-bordered table-hover table-stripped">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Judul</th>
                    <th>Deskripsi</th>
                    <th width="15%">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($articles as $i => $article)
                    <tr>
                        <td>{{ ++$i }}</td>
                        <td>{{ $article->judul }}</td>
                        <td>{{ $article->deskripsi }}</td>
                        <td>
                            <a href="/edit/{{ $article->id }}"><button class="btn btn-success">Edit</button></a>
                            <a href="/delete/{{ $article->id }}"><button class="btn btn-danger">Hapus</button></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
```

Jangan lupa, kita perlu membuat routing supaya halaman dapat ditampilkan dengan cara membuka file **web.php** pada folder **routes** dan tambahkan kode seperti ini:

```php+HTML
Route::get('/admin', [ArtikelController::class, 'show_by_admin']);
```

Selanjutnya akses **http://127.0.0.1:8000/admin** atau **http://blog-klowor.local/admin** untuk membuka halaman admin Kita:

![image-20220317204150202](img\image-20220317204150202.png)

Meskipun sudah memiliki halaman admin, mengedit dan menghapus artikel masih belum dapat dilakukan. Jadi, kita perlu menambahkan fiturnya terlebih dahulu. 

### 9. Membuat Fitur Edit Artikel

Untuk dapat membuat fitur edit, kita perlu menambahkan kode berikut pada file **web.php** yang ada di dalam folder **routes**.

```php+HTML
Route::get('/edit/{id}', [ArtikelController::class, 'edit']);
```

Setelah itu, tambahkan kode di bawah pada **ArtikelController.php** di dalam folder **app/Http/Controller**:

```php+HTML
public function edit($id)
{
$article = DB::table('artikel')->where('id', $id)->first();
return view('edit', ['article'=>$article]);
}
```

Setelah berhasil mengambil data artikel dari model, kita bisa buat file **edit.blade.php** pada folder **resources/views** dan isikan kode ini:

```php+HTML
@extends('master')

<!-- membuat judul bernama 'Edit Artikel' pada tab bar -->
@section('title', 'Edit Artikel')

@section('header')
<center class="mt-4">
    <h2>Edit Artikel</h2>
</center>
@endsection

@section('main')
<div class="col-md-8 col-sm-12 bg-white p-4">
    <form method="post" action="/edit_process">
    @csrf
	<input type="hidden" value="{{ $article->id }}" name="id">
        <div class="form-group">
            <label>Judul Artikel</label>
            <input type="text" class="form-control" value="{{ $article->judul }}" name="judul" placeholder="Judul artikel">
        </div>
        <div class="form-group">
            <label>Isi Artikel</label>
            <textarea class="form-control" name="deskripsi" rows="15">{{ $article->deskripsi }}
            </textarea>
        </div>
</div>
@endsection

<!-- membuat komponen sidebar yang berisi tombol untuk upload artikel -->
@section('sidebar')
<div class="col-md-3 ml-md-5 col-sm-12 bg-white p-4" style="height:120px !important">
    <div class="form-group">
        <label>Edit</label>
        <input type="submit" class="form-control btn btn-primary" value="Edit">
    </div>
</div>
</form>
@endsection
```

Selanjutnya, bukalah file **ArtikelController.php** dan tambahlah kode di dalamnya :

```php+HTML
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session as FacadesSession;

class ArtikelController extends Controller
{
    public function show()
    {
        $articles = DB::table('artikel')->orderby('id', 'desc')->get();
        return view('show', ['articles'=>$articles]);
    }
    public function add()
    {
        return view('add');
    }
    public function add_process(Request $article)
    {
        DB::table('artikel')->insert([
            'judul'=>$article->judul,
            'deskripsi'=>$article->deskripsi
        ]);

        return redirect()->action([ArtikelController::class, 'show']);
    }
    public function detail($id)
    {
        $article = DB::table('artikel')->where('id', $id)->first();
        return view('detail', ['article'=>$article]);
    }
    public function show_by_admin()
    {
        $articles = DB::table('artikel')->orderby('id', 'desc')->get();
        return view('adminshow', ['articles'=>$articles]);
    }
    public function edit($id)
    {
        $article = DB::table('artikel')->where('id', $id)->first();
        return view('edit', ['article'=>$article]);
    }
    public function edit_process(Request $article)
    {
        $id = $article->id;
        $judul = $article->judul;
        $deskripsi = $article->deskripsi;
        DB::table('artikel')->where('id', $id)
                            ->update(['judul' => $judul, 'deskripsi' => $deskripsi]);
        FacadesSession::flash('success', 'Artikel berhasil diedit');
        return redirect()->action([ArtikelController::class, 'show_by_admin']);
    }
}
?>
```

Jangan lupa untuk mengatur routingnya pada file **web.php** dengan mengubah kodenya menjadi:

```php+HTML
Route::post('/edit_process', [ArtikelController::class, 'edit_process']);
```

Cobalah untuk mengedit salah satu artikel dan simpan perubahannya. Jika berhasil, kita akan mendapatkan notifikasi seperti contoh berikut:

![image-20220317205109107](img\image-20220317205109107.png)

![image-20220317205229198](img\image-20220317205229198.png)

### 10. Membuat Fitur Hapus Artikel

Untuk membuat fitur hapus artikel di blog Laravel, inilah langkah yang perlu dilakukan. Pertama, bukalah file **ArtikelController.php** pada folder **app/Http/Controllers** dan tambahkan potongan kode di bawah ini:

```php
    public function delete($id)
    {
        //menghapus artikel dengan ID sesuai pada URL
        DB::table('artikel')->where('id', $id)
                            ->delete();

        //membuat pesan yang akan ditampilkan ketika artikel berhasil dihapus
        FacadesSession::flash('success', 'Artikel berhasil dihapus');
        return redirect()->action([ArtikelController::class, 'show_by_admin']);
    }
```

Selanjutnya, silakan menambahkan potongan kode di bawah pada file **web.php** yang ada di folder **routes**.

```
Route::get('/delete/{id}', [ArtikelController::class, 'delete']);
```

Setelah langkah di atas, cobalah menghapus salah satu artikel yang kita inginkan. Jika berhasil, inilah tampilannya:

![image-20220317205656888](img\image-20220317205656888.png)

![image-20220317205724313](img\image-20220317205724313.png)

### 11. Membuat Halaman Register dan Login

kita tentu tidak ingin siapapun dapat mengakses halaman admin, bukan? Untuk itu, kita perlu membuat halaman register dan login untuk mengakses halaman admin.

Pada **Navicat** database kita sudah terdapat tabel users. Jadi, kita bisa memanfaatkan tabel tersebut untuk menampung data user yang dapat mengelola artikel. Langkah berikutnya adalah membuat halaman register untuk mendaftarkan admin yang akan mengelola artikel. 

![image-20220317205919150](img\image-20220317205919150.png)

Caranya, ketikkan perintah **php artisan ui:auth** pada menu **Terminal**. Lalu, ketikkan juga **php artisan serve.** Setelah itu, kita bisa mengakses  **http://127.0.0.1:8000/login** atau **http://blog-klowor.local/login** sebagai halaman login:

![image-20220317210237798](img\image-20220317210237798.png)

Akses juga **http://127.0.0.1:8000/register** atau **http://blog-klowor.local/register**  **** sebagai halaman register blog Laravel Kita:

![image-20220317210355758](img\image-20220317210355758.png)

Kita juga bisa langsung mendaftar dan login tanpa perlu mengatur controller lagi. Selain itu, kita juga telah memiliki fitur logout secara default dengan menggunakan **auth**.

![image-20220317210534285](img\image-20220317210534285.png)

### 12. Mengarahkan Halaman Setelah Login

Setelah login, tentunya akan lebih baik jika kita diarahkan ke halaman admin. Nah, untuk mengaturnya, kita bisa membuka file **LoginController.php** pada folder **app/Http/Controllers/Auth** dan gantilah dengan kode di bawah:

```php
protected $redirectTo = RouteServiceProvider::HOME;
```

jadi

```php
protected $redirectTo = '/admin';
```

Cobalah melakukan login dan kita akan diarahkan ke halaman seperti di bawah ini:

![image-20220317210939685](img\image-20220317210939685.png)

![image-20220317211005023](img\image-20220317211005023.png)

### 13. Membatasi Akses Halaman Admin

Meskipun telah memiliki halaman login untuk admin, sebenarnya kita masih bisa mengaksesnya tanpa harus login. Oleh karena itu, penting untuk membatasi hak akses halaman admin agar pengguna yang belum login akan diarahkan ke halaman login.

Langkah yang perlu dilakukan adalah membuka file **ArtikelController.php** pada folder **app/Http/Controller** dan mengganti kode menjadi:

```php
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session as FacadesSession;

class ArtikelController extends Controller
{
    private function is_login()
    {
        if (Auth::user()) {
            return true;
        } else {
            return false;
        }
    }
    public function show()
    {
        $articles = DB::table('artikel')->orderby('id', 'desc')->get();
        return view('show', ['articles' => $articles]);
    }
    public function add()
    {
        if ($this->is_login()) {
            return view('add');
        } else {
            return redirect('/login');
        }
    }
    public function add_process(Request $article)
    {
        DB::table('artikel')->insert([
            'judul' => $article->judul,
            'deskripsi' => $article->deskripsi
        ]);

        return redirect()->action([ArtikelController::class, 'show']);
    }
    public function detail($id)
    {
        $article = DB::table('artikel')->where('id', $id)->first();
        return view('detail', ['article' => $article]);
    }
    public function show_by_admin()
    {
        if ($this->is_login()) {
            $articles = DB::table('artikel')->orderby('id', 'desc')->get();
            return view('adminshow', ['articles' => $articles]);
        } else {
            return redirect('/login');
        }
    }
    public function edit($id)
    {
        if ($this->is_login()) {
            $article = DB::table('artikel')->where('id', $id)->first();
            return view('edit', ['article' => $article]);
        } else {
            return redirect('/login');
        }
    }
    public function edit_process(Request $article)
    {
        $id = $article->id;
        $judul = $article->judul;
        $deskripsi = $article->deskripsi;
        DB::table('artikel')->where('id', $id)
            ->update(['judul' => $judul, 'deskripsi' => $deskripsi]);
        FacadesSession::flash('success', 'Artikel berhasil diedit');
        return redirect()->action([ArtikelController::class, 'show_by_admin']);
    }
    public function delete($id)
    {
        if ($this->is_login()) {
            //menghapus artikel dengan ID sesuai pada URL
            DB::table('artikel')->where('id', $id)
                ->delete();

            //membuat pesan yang akan ditampilkan ketika artikel berhasil dihapus
            FacadesSession::flash('success', 'Artikel berhasil dihapus');
            return redirect()->action([ArtikelController::class, 'show_by_admin']);
        } else {
            return redirect('/login');
        }
    }
}
?>
```

> Jika kita menemui hal seperti ini
>
> ![image-20220317211231155](img\image-20220317211231155.png)
>
> maka arahkan kursor ke `Auth` kemudian tekan Ctrl + spasi, 
>
> ![image-20220317211354274](img\image-20220317211354274.png)
>
> pilih class yang kita butuhkan,

Setelah itu, cobalah mengakses halaman **127.0.0.1:8000/admin** tanpa login. kita tentu akan diarahkan ke halaman login terlebih dahulu. Dengan begitu, blog kita menjadi lebih aman, bukan?

### 14. Mempermudah Navigasi

Penting untuk mempermudah navigasi pada blog Laravel kita . Mulai dari navigasi logout hingga melihat blog. Tanpa bantuan navigasi, admin harus mengakses **127.0.0.1:8000** melalui URL untuk melihat blog. 

Apa yang perlu dilakukan? Pertama, kita  bisa membuat navigasi dari sisi admin dan sisi pengunjung supaya dapat login dengan mudah. Caranya, bukalah file **master.blade.php** pada folder **resources/views** dan ubahlah kode di dalamnya menjadi seperti ini:

```php+HTML
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <title>@yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="{{ asset('js/app.js') }}" defer></script>
</head>
<body>
<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="/" target="_blank">
                                        Lihat Blog
                                    </a>

                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
    <div class="container mt-4">
        @yield('header')
        <div class="row">
            @yield('main')
            @yield('sidebar')
        </div>
    </div>
</body>
</html>
```

Setelah itu, kita bisa mencoba mengakses halaman admin dan akan melihat tampilan seperti ini: 

![image-20220317212623059](img\image-20220317212623059.png)