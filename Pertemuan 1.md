# Mengenal Framework Laravel

Laravel diluncurkan sejak tahun 2011 dan mengalami pertumbuhan yang cukup eksponensial. Di tahun 2015, Laravel adalah framework yang paling banyak mendapatkan bintang di Github. Sekarang framework ini menjadi salah satu yang populer di dunia, tidak terkecuali di Indonesia. 

Laravel fokus di bagian end-user, yang berarti fokus pada kejelasan dan kesederhanaan, baik penulisan maupun tampilan, serta menghasilkan fungsionalitas aplikasi web yang bekerja sebagaimana mestinya. Hal ini membuat ***developer*** maupun perusahaan menggunakan framework ini untuk membangun apa pun, mulai dari proyek kecil hingga skala perusahaan kelas atas.

Laravel mengubah pengembangan website menjadi lebih elegan, ekspresif, dan menyenangkan, sesuai dengan jargonnya “*The PHP Framework For Web Artisans*”. Selain itu, Laravel juga mempermudah proses pengembangan website dengan bantuan beberapa fitur unggulan, seperti *Template Engine*, *Routing*, dan *Modularity*.

## Manfaat Laravel untuk Proses Pengembangan Website

Laravel menawarkan beberapa keuntungan ketika kita mengembangkan website menggunakan dasar framework ini.

- Pertama, website menjadi lebih *scalable* (mudah dikembangkan).
- Kedua, terdapat *namespace* dan tampilan yang membantu kita untuk mengorganisir dan mengatur sumber daya website. 
- Ketiga, proses pengembangan menjadi lebih cepat sehingga menghemat waktu karena Laravel dapat dikombinasikan dengan beberapa komponen dari framework lain untuk mengembangkan website.

## 2 Tools Andalan Laravel

Selain itu, ada dua tools Laravel yang jarang dimiliki oleh framework lain (kecuali Symphony), yaitu Composer dan Artisan. Apa kegunaan masing-masing dari tool tersebut?

### 1. Composer

Composer merupakan tool yang di dalamnya terdapat *dependencies* dan kumpulan *library*. Seluruh *dependencies* disimpan menggunakan format file composer.json sehingga dapat ditempatkan di dalam folder utama website. Inilah mengapa composer terkadang dikenal dengan *dependencies management*.

Pertanyaannya lain, apa itu *dependencies management*?

Misalnya kita mempunyai sebuah website yang membutuhkan sebuah *library*. Saya ambil contoh library untuk mengimplementasikan validasi dan proteksi untuk *spamming*, yaitu Google reCaptcha.

Tentu saja untuk menyediakan Google reCaptcha tidak bisa menggunakan satu library saja, tapi membutuhkan beberapa *library*. kita tidak mungkin menginstall satu per satu library, kan?

Nah! Composer membantu kita untuk menginstall *library* yang dibutuhkan oleh *library* Google reCaptcha. Jadi jika menggunakan composer kita tinggal menginstall *library* Google reCaptcha dan secara otomatis *library* lain akan terinstall.

Begitu pun ketika ingin memperbarui *library*, kita cukup menggunakan perintah “**$ composer update”** dan satu per satu library akan diperbarui secara otomatis.

### 2. Artisan

Sudah pernah mendengar ini? Artisan merupakan command line interface yang dimiliki oleh Laravel. Artisan mencakup sekumpulan perintah yang membantu kita untuk membangun sebuah website atau aplikasi web.

Kumpulan perintah Artisan juga termasuk penggabungan dengan framework Symphony yang menghasilkan **fitur add-on di Laravel 8** (sekarang sudah masuk ke versi Laravel 8). Dengan adanya fitur add-on, kita bisa menambahkan berbagai macam fitur baru ke Laravel.

## Fitur-Fitur Laravel Lainnya

Laravel mempunyai berbagai macam fitur yang tidak semua framework menyediakannya. Apalagi Laravel adalah framework yang modern sehingga kita dapat melakukan berbagai hal menggunakan framework ini seperti proses otentifikasi terbaru.

Berikut ini beberapa fitur Laravel yang perlu kita ketahui.

| **Blade Template Engine**             | Laravel menggunakan Blade. Blade merupakan template engine untuk mendesain layout yang unik. Layout yang didesain dapat digunakan di tampilan lain sehingga menyediakan konsistensi desain dan struktur selama proses pengembangan.. Dibandingkan dengan template engine lain, Blade mempunyai kelebihan: tidak membatasi pengembang untuk menggunakan kode PHP biasa di dalam tampilan; desain tampilan blade akan tetap di-*cache* sampai dengan ada modifikasi. |
| ------------------------------------- | ------------------------------------------------------------ |
| **Routing**                           | Di Laravel, semua *request* dipetakan dengan bantuan rute. Dasar dari routing adalah merutekan *request* ke kontroler terkait. Routing ini dianggap dapat mempermudah pengembangan website dan meningkatkan performanya. Setidaknya ada tiga kategori routing di Laravel, yaitu *basic routing*, *route parameters*, dan *named routes*. |
| **Modularity**                        | Seperti yang sudah dibahas di bagian sebelumnya, di dalam Laravel terdapat kumpulan modul dan *library* yang terkait dengan composer. Fitur ini membantu kita untuk menyempurnakan dan meningkatkan fungsionalitas dari website yang dibangun, serta mempermudah proses *update*. |
| **Testability**                       | Laravel dibangun dengan fitur proses pengecekan yang cukup lengkap. Framework ini mendukung proses pengecekan dengan PHPUnit dan file phpunit.xml yang dapat disesuaikan dengan aplikasi web yang sedang dibangun. Framework ini juga dibangun menggunakan metode pembantu yang nyaman. Metode ini memungkinkan kita untuk menguji website secara ekspresif. |
| **Query Builder and ORM**             | Laravel database query builder menyediakan antarmuka yang lancar untuk membuat dan menjalankan database query. Fitur ini dapat digunakan untuk menjalankan berbagai operasi database di dalam website dan mendukung berbagai sistem database. |
| **Authentication**                    | Laravel membuat pengimplementasian otentikasi menjadi sangat sederhana. Seluruh proses konfigurasi otentikasi sudah berjalan secara otomatis. kita bisa menemukan file konfigurasi otentikasi ini di ‘**config/auth.php**’. Di dalam file ini terdapat beberapa opsi otentifikasi yang sudah terdokumentasikan dengan baik dan sewaktu-waktu dapat kita sesuaikan dengan kebutuhan sistem. |
| **Schema Builder**                    | Class Laravel Schema menyediakan database *agnostic* untuk memanipulasi tabel. Schema ini berjalan baik di berbagai tipe database yang didukung Laravel dan mempunyai API yang sama di seluruh sistem. |
| **Configuration Management Features** | Seluruh file konfigurasi Laravel disimpan di dalam direktori **config**. Setiap opsi didokumentasikan dengan baik. Jadi kita tidak perlu khawatir untuk mengubah setiap konfigurasi yang tersedia. |
| **E-mail Class**                      | Laravel menyediakan API beberapa *library* SwiftMailer yang cukup populer dengan koneksi ke SMTP, Postmark, Mailgun, SparkPost, Amazon SES, dan sendmail. Fitur ini memungkinkan kita untuk mengirimkan email dengan cepat melalui aplikasi lokal maupun layanan cloud. |
| **Redis**                             | Laravel menggunakan Redis untuk menghubungkan antara sesi yang sudah ada dengan cache *general-purpose*. Redis terkoneksi dengan session secara langsung. Redis merupakan aplikasi open source yang menyimpan key-value. Redis juga sering dikenal dengan server struktur data yang dapat menyimpan key dengan tipe *strings*, *hashes*, *lists*, *sets*, dan *sorted sets*. |
| **Event and Command Bus**             | Laravel Command Bus menyediakan metode pengumpulan tugas yang dibutuhkan aplikasi supaya dapat berjalan secara simpel dan perintah yang mudah dimengerti. |

## Laravel Add-On Package

Salah satu kelebihan menggunakan Laravel Add-On Package adalah mengizinkan kita untuk menggunakan berbagai macam fitur tambahan. Fitur ini dapat kita gunakan sebagai aplikasi hosting termasuk routing, migration, test, views, dan beberapa fitur yang sangat berguna lainnya. Keuntungan lain penggunaan package adalah prinsip ‘*Don’t Repeat Yourself* (DRY)’.

Ada banyak sekali packages untuk Laravel yang membuat aplikasi menjadi lebih cepat dan kencang, memperketat keamanan dan performanya juga. Saya akan sedikit membahas mengenai beberapa packages Laravel yang perlu kita ketahui dan sering dipakai di aplikasi Laravel pada umumnya.

### 1. Spatie

Bentuk aturan dan *permission* sangat penting di berbagai macam aplikasi web. Laravel sendiri juga mempunyai berbagai macam package yang bisa mendukung bentuk aturan dan permission. Bahkan packages tersebut dapat meningkatkan efektifitas dari kode program. Salah satu package yang disarankan adalah Spatie Roles & Permission.

Beberapa kelebihan dari Spatie adalah SpatieRoles, permissions, middleware, permissions langsung, terdiri banyak instruksi Blade, dan perintah Artisan

### 2. Entrust

Package ini menyediakan cara yang fleksibel untuk menambahkan Role-based Permission untuk aplikasi Laravel 5. Di dalam package ini setidaknya terdapat empat tabel: tabel *roles* untuk menyimpan ***role records\***, tabel *permissions* untuk menyimpan *permission record*, tabel role_user untuk menyimpan *one-to-many relations* di antara *roles* dan *users*, tabel permission_tole untuk menyimpan relasi *many-to-many* di antara *roles* dan *permissions*.

### 3. Laravel User Verification

Package User Verification memungkinkan kita untuk menangani verifikasi user dan memvalidasi email. Fitur ini juga menghasilkan dan menyimpan token verifikasi untuk user yang sudah teregistrasi, mengirim,mengatur antrian email dengan link token verifikasi, menangani token verifikasi, dan menandai user yang terpercaya. Package User Verification ini juga menyediakan fungsionalitas, contohnya pemeriksaan rute *middleware*.

### 4. Migration Generator

Migration Generator merupakan paket Laravel yang dapat kita gunakan untuk proses migrasi dari database yang sudah ada. Di dalamnya terdapat juga indeks dan foreign keys. Proses migrasi untuk seluruh tabel yang ada di dalam database dapat kita lakukan hanya dengan menjalankan package ini di dalam aplikasi Laravel.

### 5. Laravel Debugbar

Laravel Debugbar merupakan package populer Laravel lain yang membantu user untuk menambahkan toolbar developer di dalam aplikasi. Paket ini berguna khusus untuk tujuan *debugging*.

Ada banyak sekali opsi yang tersedia di dalam Debugbar. Fitur yang ada di dalamnya akan membantu kita untuk menunjukkan seluruh *query* yang tersedia di dalam aplikasi –semuanya terkait dengan rute.

Laravel Debugbar juga akan menampilkan seluruh template yang sudah pernah dirender dan juga parameter yang sudah kita pakai sebelumnya ketika kita menjalankannya.

kita dapat menambahkan pesan tambahan menggunakan Facade, dan itu akan muncul di bagian bawah tab ‘Messages’ di Laravel Debugbar.

# Cara Membuat Blog dengan Laravel

Beberapa hal yang dibutuhkan adalah sebagai berikut:

1. Laragon

   https://laragon.org/download/index.html

2. Text Editor (VS Code)

   https://code.visualstudio.com/download

3. Extension pada VS Code `Laravel Extension Pack`

Pastikan Laragon dan Text Editor sudah terinstal. Untuk membuat laravel blog, inilah beberapa langkah yang perlu kita lakukan:

### 1. Melakukan Instalasi Laravel

Sebagai langkah awal, kita perlu melakukan **instalasi Laravel**. Ada beberapa cara instalasi Laravel, bisa melalui **VPS**, **cPanel**, dan di **komputer**. Nah, di tutorial kali ini kita akan menggunakan Laravel di komputer menggunakan sistem operasi Windows.

Cara Instal Laravel adalah sebagai berikut:

- Buka Laragon dan Jalankan.

  ![image-20220310211800802](img\image-20220310211800802.png)

- Klik kanan >  Quick App > Laravel

  ![image-20220310212122141](img\image-20220310212122141.png)

- Kemudian beri nama projek kalian dengan `blog-nim`

- Tunggu sampai proses instalasi selesai

  ![image-20220310212614493](img\image-20220310212614493.png)

  > Nb.
  >
  > 1. Project path adalah tempat dimana projek kita disimpan
  > 2. Pretty url adalah alamat yang digunakan untuk mengakses projek yang kita buat



Setelah Laravel terinstal, coba buka foldernya dengan *text editor* favorit Anda. Di panduan ini, kami menggunakan **Visual Studio Code**. Kemudian masuk pada menu ***Extension*** lalu ketikkan `Laravel Extension Pack`

Selanjutnya klik **Install**, tunggu sampai proses selesai.

![image-20220310231917555](img\image-20220310231917555.png)

Buka terminal Laragon (*Klik Terminal*), masuk ke folder ***Project Path***  kalian ketikkan `php artisan serve` seperti contoh berikut:

![image-20220310213216933](img\image-20220310213216933.png)

Kemudian akses `http://127.0.0.2:8000` atau kita bisa ketikkan alamat ***Pretty Url*** pada address bar. Berikut adalh tampilan awal dari laravel kita.

![image-20220310213436019](img\image-20220310213436019.png)

### 2. Melakukan Konfigurasi Database

Setelah berhasil menginstall Laravel, kita perlu melakukan berbagai konfigurasi database. Namun, ketika kita menggunakan Laragon. Kita tidak perlu melakukan konfigurasi lagi.

Untuk  pengaturan database bisa dilihat pada file `.env` seperti berikut

```php
APP_NAME=Laravel
APP_ENV=local
APP_KEY=base64:wvq8RG62+V0qksLNrn5zKiXIOkaPHUrj8X6T/F31bkk=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack
LOG_DEPRECATIONS_CHANNEL=null
LOG_LEVEL=debug

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=blog-klowor
DB_USERNAME=root
DB_PASSWORD=

BROADCAST_DRIVER=log
CACHE_DRIVER=file
FILESYSTEM_DRIVER=local
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120

MEMCACHED_HOST=127.0.0.1

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_MAILER=smtp
MAIL_HOST=mailhog
MAIL_PORT=1025
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS=null
MAIL_FROM_NAME="${APP_NAME}"

AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=
AWS_USE_PATH_STYLE_ENDPOINT=false

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"
```

>  Nb. Perhatikan! Setting keterangan berikut sesuai dengan komputer masing-masing
>
> - DB_CONNECTION adalah tipe koneksi 
> - DB_HOST adalah host server
> - DB_PORT adalah port server
> - DB_DATABASE adalah nama database kita
> - DB_USERNAME adalah username database kita
> - DB_PASSWORD adalah password database kita

### 3. Menghubungkan Laravel dengan Bootstrap

Langkah selanjutnya adalah menyambungkan Laravel dengan Bootstrap. Sebagai sebuah framework CSS, Bootstrap dapat membantu kita membuat tampilan website yang menarik dengan mudah.

Secara default, Laravel telah memiliki file bootstrap di dalam folder **public/css** dan **public/js**. Jadi, tak perlu mendownloadnya secara khusus terlebih dahulu.

Untuk membuat halaman Laravel blog kita terhubung dengan bootstrap, kita perlu membuat file bernama **master.blade.php** di dalam folder **resources/view**. Dengan begitu, strukturnya akan menjadi seperti ini:

![Master blade](img\Master-Blade.png)

Setelah itu, kita bisa isikan kode di bawah ini pada file **master.blade.php**:

```php+HTML
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
    <button class="btn btn-info">Tes</button>
</body>
</html>
```

Kemudian, kita bisa buka folder **routes/web.php** dan tambahkan perintah di bawah:

```php+HTML
Route::get('/test', function() {
     return view('master');
});
```

Dengan begitu, kode pada **web.php** akan menjadi seperti ini:

```php+HTML
<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/test', function() {
    return view('master');
});
?>
```

Selanjutnya kita install bootstrap melalui cmd, ikuti langkah berikut:

1. `composer require laravel/ui`
2. `php artisan ui bootstrap`
3. `npm install && npm run dev`
4. jika terjadi error ketikkan `npm development`

Untuk menguji apakah Laravel blog kita berhasil terhubung dengan Bootstrap, ketikkan perintah **php artisan serve** pada menu **Terminal**. Setelah itu, silakan mengakses **127.0.0.1:8000/test**  dan pastikan hasilnya seperti di bawah ini:

![image-20220310222742774](img\image-20220310222742774.png)

### 4. Membuat Tabel Artikel

Berikutnya, kita bisa membuat tabel bernama **artikel** yang berfungsi untuk menyimpan data artikel blog kita. Caranya, ketikkan perintah di bawah ini pada terminal:

`php artisan make:migration create_artikel_table`

Hasilnya, file **create_artikel_table.php** akan ditemukan di dalam folder **database/migrations**.

Bukalah file tersebut dan isi dengan kode di bawah:

```php+HTML
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArtikelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artikel', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('judul');
            $table->text('deskripsi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artikel');
    }
}
?>
```

Setelah itu, kita bisa ketikkan kode berikut pada terminal untuk membuat tabel **artikel**:

```
php artisan migrate
```

Tabel **artikel** yang baru dibuat akan bisa kamu lihat melalui **navicat** ataupun pada **localhost/phpmyadmin**. 

![image-20220310223520208](img\image-20220310223520208.png)

### 5. Membuat Halaman Tambah Artikel

Setelah membuat tabel artikel, saatnya membuat halaman form untuk menambahkan artikel Laravel blog Anda.

Caranya, buatlah file bernama **add.blade.php** pada folder **resources/view**.

```php+HTML
<!-- membuat kerangka dari master.blade.php -->
@extends('master')

<!-- membuat komponen title sebagai judul halaman -->
@section('title', 'Menambah Artikel')

<!-- membuat komponen main yang berisi form untuk mengisi judul dan isi artikel -->
@section('main')
    <div class="col-md-8 col-sm-12 bg-white p-4">
        <form method="post" action="/add_process">
            @csrf
            <div class="form-group">
                <label>Judul Artikel</label>
                <input type="text" class="form-control" name="judul" placeholder="Judul artikel">
            </div>
            <div class="form-group">
                <label>Isi Artikel</label>
                <textarea class="form-control" name="deskripsi" rows="15"></textarea>
            </div>
    </div>
@endsection

<!-- membuat komponen sidebar yang berisi tombol untuk upload artikel -->
@section('sidebar')
    <div class="col-md-3 ml-md-5 col-sm-12 bg-white p-4" style="height:120px !important">
        <div class="form-group">
            <label>Upload</label>
            <input type="submit" class="form-control btn btn-primary" value="Upload">
        </div>
    </div>
    </form>
@endsection
```

Setelah itu, kita bisa mencari file **master.blade.php** pada folder **resources/views** dan ubah kode menjadi seperti ini:

```php+HTML
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <title>@yield('title')</title>
</head>
<body>
    <div class="container mt-4">
        <div class="row">
            @yield('main')
            @yield('sidebar')
        </div>
    </div>
</body>
</html>
```

Jangan lupa atur routingnya supaya form yang baru saja kita buat dapat diakses. Caranya adalah dengan membuka file **web.php** yang ada di dalam folder **routes** dan mengubah kodenya menjadi seperti ini:

```php+HTML
Route::get('/add', function () {
    return view('add');
});
```

Untuk melihat tampilan halaman tambah artikel, kita bisa membuka **127.0.0.1:8000/add**. Halaman blog kita akan terlihat seperti gambar di bawah:

![image-20220310224009365](img\image-20220310224009365.png)

Namun, kita belum bisa mengirim artikel ke database karena masih perlu melakukan pengaturan pada controller. Sebelum membuat controller, kita perlu mengatur routing untuk mengarahkan data artikel yang dikirimkan.

Silakan untuk membuka file **web.php** pada folder **routes**. Kemudian, tambahkan kode di bawah ini:

```php+HTML
Route::post('/add_process', 'ArtikelController@add_process');
```

Setelah itu, kita bisa membuat controller dengan mengetikkan perintah **php artisan make:controller ArtikelController** pada **Terminal**. 

![image-20220310224255719](img\image-20220310224255719.png)

Jika berhasil, file **ArtikelController.php** akan berada pada folder **app/Http/Controller**. Selanjutnya, bukalah file **ArtikelController.php** dan ubah kode menjadi seperti ini:

```php+HTML
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ArtikelController extends Controller
{
    public function show()
    {
        return "Artikel berhasil ditambahkan";
    }
    public function add_process(Request $article)
    {
        DB::table('artikel')->insert([
            'judul'=>$article->judul,
            'deskripsi'=>$article->deskripsi
        ]);

        return redirect()->action([ArtikelController::class, 'show']);
    }
}
?>
```

Setelah itu, silakan buat routing untuk mengarahkan pengguna setelah berhasil memasukkan data. Kita bisa membuka file **web.php** pada folder **routes**. Kemudian, ubah kode menjadi seperti ini:

```php+HTML
<?php

use App\Http\Controllers\ArtikelController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/add', function () {
    return view('add');
});
Route::get('/', [ArtikelController::class, 'show']);
Route::post('/add_process', [ArtikelController::class, 'add_process']);
?>
```

Selanjutnya, Kita bisa membuka **127.0.0.1:8000/add** dan mencoba memasukkan artikel blog. Jika artikel berhasil tersimpan, akan muncul tampilan berikut:

![image-20220310225710324](img\image-20220310225710324.png)

Untuk memastikannya, kita bisa membuka **localhost/phpmyadmin** pada tabel **artikel**. Artikel blog kita sudah ada dalam daftar, bukan? 

![image-20220310231257910](img\image-20220310231257910.png)

### 6. Menampilkan Seluruh Artikel

Bagaimana cara menampilkan artikel yang sudah dibuat? Kita bisa membuka file **ArtikelController.php** pada folder **app/Http/Controller** kemudian ganti kode menjadi seperti di bawah:

```php+HTML
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ArtikelController extends Controller
{
    public function show()
    {
        $articles = DB::table('artikel')->orderby('id', 'desc')->get();
        return view('show', ['articles'=>$articles]);
    }
    public function add_process(Request $article)
    {
        DB::table('artikel')->insert([
            'judul'=>$article->judul,
            'deskripsi'=>$article->deskripsi
        ]);

        return redirect()->action([ArtikelController::class, 'show']);
    }
}
?>
```

Setelah itu, Kita bisa membuat file bernama **show.blade.php** pada folder **resources/views** dan isikan kode di bawah:

```php+HTML
<!-- menggunakan kerangka dari halaman master.blade.php -->
@extends('master')

<!-- membuat komponen title sebagai judul halaman -->
@section('title', 'Blog Klowor')

<!-- membuat header dan tombol tambah artikel di atas -->
@section('header')
    <center>
        <h2>Blog Klowor</h2>
        <a href="/add"><button class="btn btn-success">Tambah Artikel</button></a>
    </center>
@endsection

<!-- membuat komponen main yang berisi form untuk mengisi judul dan isi artikel -->
@section('main')
    @foreach ($articles as $article)
        <div class="col-md-4 col-sm-12 mt-4">
            <div class="card">
                <img src="https://www.whatsnxt.io/wp-content/uploads/2016/04/dummy-post-horisontal-thegem-blog-default.jpg"
                    class="card-img-top" alt="gambar">
                <div class="card-body">
                    <h5 class="card-title">{{ $article->judul }}</h5>
                    <a href="/detail/{{ $article->id }}" class="btn btn-primary">Baca Artikel</a>
                </div>
            </div>
        </div>
    @endforeach
@endsection
```

Kemudian, Kita bisa membuka file **master.blade.php** pada folder **resources/views** dan letakkan kode **@yield(‘header’)** sebelum kode **`<div class="row">`**. Setelah itu, buka halaman **127.0.0.1/8000** untuk mendapatkan tampilan sebagai berikut:

![image-20220310231338373](img\image-20220310231338373.png)

### 7. Menampilkan Detail Artikel

Di blog Laravel tersebut, kita sudah memiliki opsi **Tambah Artikel** tapi belum bisa untuk klik **Baca Artikel**. Jadi, kita perlu membuat fitur untuk membaca artikel terlebih dahulu.

Caranya, bukalah file **ArtikelController.php** dan ubah kode di dalamnya menjadi seperti ini:

```php+HTML
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ArtikelController extends Controller
{
    public function show()
    {
        $articles = DB::table('artikel')->orderby('id', 'desc')->get();
        return view('show', ['articles'=>$articles]);
    }
    public function add_process(Request $article)
    {
        DB::table('artikel')->insert([
            'judul'=>$article->judul,
            'deskripsi'=>$article->deskripsi
        ]);

        return redirect()->action([ArtikelController::class, 'show']);
    }
    public function detail($id)
    {
        $article = DB::table('artikel')->where('id', $id)->first();
        return view('detail', ['article'=>$article]);
    }
}
?>
```

Langkah di atas adalah untuk membuat controller agar dapat mengambil data artikel dari model berdasarkan ID pada URL. 

Nah, untuk bisa membuat tampilan untuk detail artikelnya, buatlah file bernama **detail.blade.php** di dalam folder **resources/views** dan masukkan kode di bawah:

```php+HTML
@extends('master')

<!-- memberikan judul di tab sesuai dengan judul artikel yang sedang dibaca -->
@section('title')
{{ $article->judul }}
@endsection

<!-- menampilkan gambar, judul, dan isi artikel -->
@section('main')
<div class="col-md-7 col-sm-12 mb-5 bg-white p-0">
    <img src="https://www.whatsnxt.io/wp-content/uploads/2016/04/dummy-post-horisontal-thegem-blog-default.jpg" class="card-img-top" alt="gambar" >
    <div class="p-4">
        <h2>{{ $article->judul }}</h2>
        <p class="mt-5">{{ $article->deskripsi }}</p>
    </div>
</div>
@endsection

<!-- menampilkan tombol kembali ke halaman utama -->
@section('sidebar')
<div class="col-md-4 offset-md-1 col-sm-12 bg-white p-4 h-100">
   <center>
        <a href="/">
            <button class="btn btn-info text-white w-100">Kembali</button>
        </a>
    </center>
</div>
@endsection
?>
```

Langkah di atas adalah untuk membuat tampilan yang akan menampilkan detail artikel dari database. Selanjutnya, Kita perlu mengatur routing untuk dapat mengakses halaman tersebut.

Caranya adalah dengan membuka file **web.php** pada folder **routes** dan ubahlah kode di dalam **web.php** menjadi seperti ini:

```php+HTML
<?php

use App\Http\Controllers\ArtikelController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/add', function () {
    return view('add');
});
Route::get('/', [ArtikelController::class, 'show']);
Route::post('/add_process', [ArtikelController::class, 'add_process']);

Route::get('/detail/{id}', [ArtikelController::class, 'detail']);
?>
```

Kita bisa mencoba klik **Baca Artikel** untuk mendapatkan tampilan seperti di bawah ini:

![image-20220310231441213](img\image-20220310231441213.png)