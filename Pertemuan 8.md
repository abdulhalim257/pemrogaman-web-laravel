# Dashboard Slideshow

Pada halaman depan kita akan menampilkan slide gambar yang sebelumnya kita buat secara statis. Yaitu memasukkan gambar-gambar yang akan kita tampilkan ke folder public di aplikasi laravel kita. Sekarang kita akan membuatnya menjadi lebih dinamis, jadi nanti kita bisa menambahkan gambar yang akan ditampilkan dalam slideshow.

Langkah-langkahnya adalah :

1. Buat migration slideshow
2. Update file model Slideshow
3. Tambahkan route slideshow ke file **web.php**
4. Tambahkan menu ke menudashboard
5. Buat file view slideshow
6. Update file controller **SlideshowController.php**

Sekarang kita jalankan command untuk membuat Controller, model dan migrations slideshow sekaligus. Buka terminal dan jalankan perintah berikut dan kemudian pencet enter.

```markup
php artisan make:model Slideshow -crm
```

Keterangan :

- -crm setelah Slideshow adalah perintah untuk membuat Controller Resource berikut migrationsnya. Jadi sekali perintah langsung membuat 3 buah file.
- Soal controller resource bisa dibaca di sini (https://laravel.com/docs/9.x/controllers#resource-controllers)

3 Buah file baru telah dibuat, sekarang kita koreksi satu per satu.

**#1. Migrations slideshow**

buka folder migrations dan edit file **_create_slideshows_table.php**

```php
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlideshowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slideshow', function (Blueprint $table) {
            $table->increments('id');
            $table->string('foto');
            $table->string('caption_title')->nullable();
            $table->string('caption_content')->nullable();
            $table->integer('user_id')->unsigned();
            $table
                ->foreign('user_id')
                ->references('id')
                ->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slideshow');
    }
}
```

**#2. Model Slideshow**

Setelah migrations, selanjutnya edit file **app/Models/Slideshow.php**

```php+HTML
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Slideshow extends Model
{
    protected $table = "slideshow";
    protected $fillable = [
        'foto',
        'caption_title',
        'caption_content',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
```

**#3. Tambahkan route**

Buka file web.php dan pada bagian di bawah `Route::delete('produkimage/{id}','ProdukController@deleteimage')` tambahkan route slideshow.

```php
// hapus image produk
Route::delete('produkimage/{id}', [ProdukController::class, 'deleteimage']);
// slideshow
Route::resource('slideshow', SlideshowController::class);
```

**#4. Tambahkan menu item ke menudashboard.blade.php**

Buka file **menudashboard.blade.php** kemudian tambahkan menu Setting di bawah data.

```php+HTML
<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
            with font-awesome or any other icon font library -->
        <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-th"></i>
                <p>
                    Dashboard
                </p>
            </a>
        </li>
        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-folder-open"></i>
                <p>
                    Produk
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ route('produk.index') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Produk</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('kategori.index') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Kategori</p>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-shopping-cart"></i>
                <p>
                    Transaksi
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ route('transaksi.index') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Data Transaksi</p>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-folder"></i>
                <p>
                    Data
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ route('customer.index') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Customer</p>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-cogs"></i>
                <p>
                    Setting
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ route('slideshow.index') }}" class="nav-link">
                        <i class="far fa-images nav-icon"></i>
                        <p>Slideshow</p>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-list"></i>
                <p>
                    Laporan
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ URL::to('admin/laporan') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Penjualan</p>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item">
            <a href="{{ URL::to('admin/profil') }}" class="nav-link">
                <i class="nav-icon fas fa-users"></i>
                <p>
                    Profil
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link" onclick="event.preventDefault();
                                                       document.getElementById('logout-form').submit();">
                <i class="nav-icon fas fa-sign-out-alt"></i>
                <p>
                    Sign Out
                </p>
            </a>
        </li>
    </ul>
</nav>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
</form>
```

**#5. Tambahkan file view slideshow**

Buka folder **views** dan tambahkan folder **slideshow**, pada folder slideshow buat 1 buah file dengan nama **index.blade.php**

![image-20220419115853894](img\image-20220419115853894.png)

```php+HTML
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <!-- table slideshow -->
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Slideshow</h4>
                    </div>
                    <div class="card-body">
                        @if ($message = Session::get('error'))
                            <div class="alert alert-warning">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th width="50px">No</th>
                                        <th>Gambar</th>
                                        <th>Title</th>
                                        <th>content</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($itemslideshow as $slide)
                                        <tr>
                                            <td>
                                                {{ ++$no }}
                                            </td>
                                            <td>
                                                @if ($slide->foto != null)
                                                    <img src="{{ \Storage::url($slide->foto) }}"
                                                        alt="{{ $slide->caption_title }}" width='150px'
                                                        class="img-thumbnail">
                                                @endif
                                            </td>
                                            <td>
                                                {{ $slide->caption_title }}
                                            </td>
                                            <td>
                                                {{ $slide->caption_content }}
                                            </td>
                                            <td>
                                                <form action="{{ route('slideshow.destroy', $slide->id) }}" method="post"
                                                    style="display:inline;">
                                                    @csrf
                                                    {{ method_field('delete') }}
                                                    <button type="submit" class="btn btn-sm btn-danger mb-2">
                                                        Hapus
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $itemslideshow->links() }}
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-4">
                                <form action="{{ url('/admin/slideshow') }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label for="foto">Foto</label>
                                        <br />
                                        <input type="file" name="image" id="image">
                                    </div>
                                    <div class="form-group">
                                        <label for="caption_title">Title</label>
                                        <input type="text" name="caption_title" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="caption_content">Content</label>
                                        <textarea name="caption_content" id="caption_content" rows="3" class="form-control"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary">Upload</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
```

**6. Update file SlideshowController.php**

Buka file **SlideshowController.php** dan update kodenya menjadi seperti berikut.

```php
<?php

namespace App\Http\Controllers;

use App\Models\Slideshow;
use Illuminate\Http\Request;

class SlideshowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $itemslideshow = Slideshow::paginate(10);
        $data = array('title' => 'Dashboard Slideshow',
                    'itemslideshow' => $itemslideshow);
        return view('slideshow.index', $data)->with('no', ($request->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'image' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        // ambil data user yang login
        $itemuser = $request->user();
        // masukkan data yang dikirim ke dalam variable $inputan
        $inputan = $request->all();
        $inputan['user_id'] = $itemuser->id;
        // ambil url foto yang diupload
        $fileupload = $request->file('image');
        $folder = $request->file('image')->store('public/images');
        $itemgambar = (new ImageController)->upload($fileupload, $itemuser, $folder);
        // masukkan url yang telah diupload ke $inputan
        $inputan['foto'] = $itemgambar->url;
        $itemslideshow = Slideshow::create($inputan);
        return back()->with('success', 'Foto berhasil diupload');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Slideshow  $slideshow
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Slideshow  $slideshow
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Slideshow  $slideshow
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Slideshow  $slideshow
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $itemslideshow = Slideshow::findOrFail($id);
        // cek kalo foto bukan null
        if ($itemslideshow->foto != null) {
            \Storage::delete($itemslideshow->foto);
        }
        if ($itemslideshow->delete()) {
            return back()->with('success', 'Data berhasil dihapus');
        } else {
            return back()->with('error', 'Data gagal dihapus');
        }
    }
}
```

Terakhir jangan lupa jalankan perintah migrate untuk membuat table baru yang kita buat di file migrations tadi. Kembali ke terminal dan ketik perintah berikut kemudian pencet enter.

```php
php artisan migrate
```

Jalankan! http://toko-klowor.local/admin/slideshow

![image-20220419131046997](img\image-20220419131046997.png)

### Dashboard Produk Promo

Pada homepage kita juga akan menampilkan produk promo. Produk promo adalah produk pilihan yang diberi potongan harga sekian persen. Oleh karena itu kita harus membuat dashboard dan juga tabel baru untuk menampung kumpulan produk-produk promo ini

Langkah-langkahnya adalah :

1. Buat migrations
2. Buat model **ProdukPromo**
3. Buat **ProdukPromoController**
4. Buat views **promo**
5. Tambahkan route di web.php
6. Tambahkan function untuk load produk secara **asynchronous**
7. Tambahkan menu di **dashboard**
8. Pindahkan link jquery ke tag `<head>`

Sebagai langkah awal kita akan membuat model, controller dan migration sekaligus. Buka terminal dan jalankan perintah berikut kemudian tekan enter.

```markup
php artisan make:model ProdukPromo -crm
```

**#1. Migrations**

Buka file **_create_produk_promos_table.php** kemudian update kodenya

```php
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdukPromosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produk_promo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produk_id')->unsigned();
            $table->decimal('harga_awal', 16, 2)->default(0);
            $table->decimal('harga_akhir', 16, 2)->default(0);
            $table->integer('diskon_persen')->default(0);
            $table->decimal('diskon_nominal', 16, 2)->default(0);
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('produk_id')->references('id')->on('produk');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produk_promo');
    }
}
```

**\#2. Model ProdukPromo**

Dari migrations di atas sekarang kita koreksi file Model **ProdukPromo.php**

```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProdukPromo extends Model
{
    protected $table = 'produk_promo';
    protected $fillable = [
        'produk_id',
        'harga_awal',
        'harga_akhir',
        'diskon_persen',
        'diskon_nominal',
        'user_id',
    ];

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
```

**#3. Update Produk Promo Controller**

Setelah model sekarang lengkapi **ProdukPromoController.php**

```php
<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use App\Models\ProdukPromo;
use Illuminate\Http\Request;

class ProdukPromoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $itempromo = ProdukPromo::orderBy('id','desc')->paginate(20);
        $data = array('title' => 'Produk Promo',
                    'itempromo'=>$itempromo);
        return view('promo.index', $data)->with('no', ($request->input('page', 1) - 1) * 20);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // kita ambil data produk
        $itemproduk = Produk::orderBy('nama_produk', 'desc')
                            ->where('status', 'publish')
                            ->get();
        $data = array('title' => 'Form Produk Promo',
                    'itemproduk' => $itemproduk);
        return view('promo.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'produk_id' => 'required',
            'harga_awal' => 'required',
            'harga_akhir' => 'required',
            'diskon_persen' => 'required',
            'diskon_nominal' => 'required',
        ]);
        // cek dulu apakah sudah ada, produk hanya bisa masuk 1 promo
        $cekpromo = ProdukPromo::where('produk_id', $request->produk_id)->first();
        if ($cekpromo) {
            return back()->with('error', 'Data sudah ada');
        } else {
            $itemuser = $request->user();
            $inputan = $request->all();
            $inputan['user_id'] = $itemuser->id;
            $itempromo = ProdukPromo::create($inputan);
            return redirect()->route('promo.index')->with('success', 'Data berhasil disimpan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProdukPromo  $produkPromo
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProdukPromo  $produkPromo
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $itempromo = ProdukPromo::findOrFail($id);
        $data = array('title' => 'Detail Produk',
                    'itempromo' => $itempromo);
        return view('promo.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProdukPromo  $produkPromo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'produk_id' => 'required',
            'harga_awal' => 'required',
            'harga_akhir' => 'required',
            'diskon_persen' => 'required',
            'diskon_nominal' => 'required',
        ]);
        $itempromo = ProdukPromo::findOrFail($id);
        // cek dulu apakah sudah ada, produk hanya bisa masuk 1 promo
        $cekpromo = ProdukPromo::where('produk_id', $request->produk_id)
                            ->where('id', '!=', $itempromo->id)
                            ->first();
        if ($cekpromo) {
            return back()->with('error', 'Data sudah ada');
        } else {
            $itemuser = $request->user();
            $inputan = $request->all();
            $inputan['user_id'] = $itemuser->id;
            $itempromo->update($inputan);
            return redirect()->route('promo.index')->with('success', 'Data berhasil diupdate');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProdukPromo  $produkPromo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $itempromo = ProdukPromo::findOrFail($id);
        if ($itempromo->delete()) {
            return back()->with('success', 'Data berhasil dihapus');
        } else {
            return back()->with('error', 'Data gagal dihapus');
        }
    }
}
```

**#4. Views Promo**

Buat folder dengan nama **promo** di dalam folder **views**. Kemudian buat 3 buah file :

![image-20220419133334100](img\image-20220419133334100.png)

**# index.blade.php**

Buat file dengan nama **index.blade.php**

```php+HTML
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <!-- table produk -->
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Produk</h4>
                        <div class="card-tools">
                            <a href="{{ route('promo.create') }}" class="btn btn-sm btn-primary">
                                Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="#">
                            <div class="row">
                                <div class="col">
                                    <input type="text" name="keyword" id="keyword" class="form-control"
                                        placeholder="ketik keyword disini">
                                </div>
                                <div class="col-auto">
                                    <button class="btn btn-primary">
                                        Cari
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-body">
                        @if ($message = Session::get('error'))
                            <div class="alert alert-warning">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th width="50px">No</th>
                                        <th>Gambar</th>
                                        <th>Kode</th>
                                        <th>Harga Awal</th>
                                        <th>Nama</th>
                                        <th>Diskon</th>
                                        <th>Harga Akhir</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($itempromo as $promo)
                                        <tr>
                                            <td>
                                                {{ ++$no }}
                                            </td>
                                            <td>
                                                @if ($promo->produk->foto != null)
                                                    <img src="{{ \Storage::url($promo->produk->foto) }}"
                                                        alt="{{ $promo->produk->nama_produk }}" width='150px'
                                                        class="img-thumbnail">
                                                @endif
                                            </td>
                                            <td>
                                                {{ $promo->produk->kode_produk }}
                                            </td>
                                            <td>
                                                {{ $promo->produk->nama_produk }}
                                            </td>
                                            <td>
                                                {{ number_format($promo->harga_awal, 2) }}
                                            </td>
                                            <td>
                                                {{ number_format($promo->diskon_nominal, 2) }}
                                                ({{ $promo->diskon_persen }}%)
                                            </td>
                                            <td>
                                                {{ number_format($promo->harga_akhir, 2) }}
                                            </td>
                                            <td>
                                                <a href="{{ route('promo.edit', $promo->id) }}"
                                                    class="btn btn-sm btn-primary mr-2 mb-2">
                                                    Edit
                                                </a>
                                                <form action="{{ route('promo.destroy', $promo->id) }}" method="post"
                                                    style="display:inline;">
                                                    @csrf
                                                    {{ method_field('delete') }}
                                                    <button type="submit" class="btn btn-sm btn-danger mb-2">
                                                        Hapus
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $itempromo->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
```

**# create.blade.php**

Selanjutnya tambah 1 lagi dengan nama **create.blade.php**

```php+HTML
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Form Produk</h3>
                        <div class="card-tools">
                            <a href="{{ route('promo.index') }}" class="btn btn-sm btn-danger">
                                Tutup
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        @if (count($errors) > 0)
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-warning">{{ $error }}</div>
                            @endforeach
                        @endif
                        @if ($message = Session::get('error'))
                            <div class="alert alert-warning">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        <form action="{{ route('promo.store') }}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="produk_id">Produk</label>
                                <select name="produk_id" id="produk_id" class="form-control">
                                    <option value="">Pilih Produk</option>
                                    @foreach ($itemproduk as $produk)
                                        <option value="{{ $produk->id }}">{{ $produk->nama_produk }} -
                                            ({{ $produk->kode_produk }})</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="harga_awal">Harga Awal</label>
                                <input type="text" name="harga_awal" id="harga_awal" class="form-control"
                                    value={{ old('harga_awal') }}>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="diskon_persen">Diskon Persen</label>
                                        <input type="text" name="diskon_persen" id="diskon_persen" class="form-control"
                                            value={{ old('diskon_persen') }}>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="diskon_nominal">Diskon Nominal</label>
                                        <input type="text" name="diskon_nominal" id="diskon_nominal" class="form-control"
                                            value={{ old('diskon_nominal') }}>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="harga_akhir">Harga Akhir</label>
                                <input type="text" name="harga_akhir" id="harga_akhir" class="form-control"
                                    value={{ old('harga_akhir') }}>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <button type="reset" class="btn btn-warning">Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        // cari nominal diskon
        $('#diskon_persen').on('keyup', function() {
            var harga_awal = $('#harga_awal').val();
            var diskon_persen = $('#diskon_persen').val();
            var diskon_nominal = diskon_persen / 100 * harga_awal;
            var harga_akhir = harga_awal - diskon_nominal;
            $('#diskon_nominal').val(diskon_nominal);
            $('#harga_akhir').val(harga_akhir);
        })
        // cari nominal persen
        $('#diskon_nominal').on('keyup', function() {
            var harga_awal = $('#harga_awal').val();
            var diskon_nominal = $('#diskon_nominal').val();
            var diskon_persen = diskon_nominal / harga_awal * 100;
            var harga_akhir = harga_awal - diskon_nominal;
            $('#diskon_persen').val(diskon_persen);
            $('#harga_akhir').val(harga_akhir);
        })
        // load produk detail
        $('#produk_id').on('change', function() {
            var id = $('#produk_id').val();
            $.ajax({
                url: '{{ URL::to('admin/loadprodukasync') }}/' + id,
                type: 'get',
                dataType: 'json',
                success: function(data, status) {
                    if (status == 'success') {
                        $('#harga_awal').val(data.itemproduk.harga);
                    }
                },
                error: function(x, t, m) {}
            })
        })
    </script>
@endsection
```

**# edit.blade.php**

Terakhir tambahkan 1 buah file dengan nama **edit.blade.php**

```php+HTML
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Form Produk</h3>
                        <div class="card-tools">
                            <a href="{{ route('promo.index') }}" class="btn btn-sm btn-danger">
                                Tutup
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        @if (count($errors) > 0)
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-warning">{{ $error }}</div>
                            @endforeach
                        @endif
                        @if ($message = Session::get('error'))
                            <div class="alert alert-warning">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        <form action="{{ route('promo.update', $itempromo->id) }}" method="post">
                            {{ method_field('patch') }}
                            @csrf
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="kode_produk">Kode Produk</label>
                                        <input type="text" name="kode_produk" id="kode_produk" class="form-control"
                                            disabled value={{ $itempromo->produk->kode_produk }}>
                                        <input type="hidden" name="produk_id" value={{ $itempromo->produk_id }}>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="nama_produk">Nama Produk</label>
                                        <input type="text" name="nama_produk" id="nama_produk" class="form-control"
                                            disabled value={{ $itempromo->produk->nama_produk }}>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="harga_awal">Harga Awal</label>
                                <input type="text" name="harga_awal" id="harga_awal" class="form-control"
                                    value={{ $itempromo->harga_awal }}>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="diskon_persen">Diskon Persen</label>
                                        <input type="text" name="diskon_persen" id="diskon_persen" class="form-control"
                                            value={{ $itempromo->diskon_persen }}>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="diskon_nominal">Diskon Nominal</label>
                                        <input type="text" name="diskon_nominal" id="diskon_nominal" class="form-control"
                                            value={{ $itempromo->diskon_nominal }}>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="harga_akhir">Harga Akhir</label>
                                <input type="text" name="harga_akhir" id="harga_akhir" class="form-control"
                                    value={{ $itempromo->harga_akhir }}>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Update</button>
                                <button type="reset" class="btn btn-warning">Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        // cari nominal diskon
        $('#diskon_persen').on('keyup', function() {
            var harga_awal = $('#harga_awal').val();
            var diskon_persen = $('#diskon_persen').val();
            var diskon_nominal = diskon_persen / 100 * harga_awal;
            var harga_akhir = harga_awal - diskon_nominal;
            $('#diskon_nominal').val(diskon_nominal);
            $('#harga_akhir').val(harga_akhir);
        })
        // cari nominal persen
        $('#diskon_nominal').on('keyup', function() {
            var harga_awal = $('#harga_awal').val();
            var diskon_nominal = $('#diskon_nominal').val();
            var diskon_persen = diskon_nominal / harga_awal * 100;
            var harga_akhir = harga_awal - diskon_nominal;
            $('#diskon_persen').val(diskon_persen);
            $('#harga_akhir').val(harga_akhir);
        })
        // load produk detail
        $('#produk_id').on('change', function() {
            var id = $('#produk_id').val();
            $.ajax({
                url: '{{ URL::to('admin/loadprodukasync') }}/' + id,
                type: 'get',
                dataType: 'json',
                success: function(data, status) {
                    if (status == 'success') {
                        $('#harga_awal').val(data.itemproduk.harga);
                    }
                },
                error: function(x, t, m) {}
            })
        })
    </script>
@endsection
```

**#5. Tambahkan route ke file web.php**

Agar bisa kita akses, kita perlu menambahkan route ke file **web.php**. tambahkan 2 buah route persis di bawah `Route::resource('slideshow', 'SlideshowController');`

```php+HTML
// produk promo
Route::resource('promo', ProdukPromoController::class);
// load async produk
Route::get('loadprodukasync/{id}', [ProdukController::class, 'loadasync']);
```

**#6. Tambahkan function untuk load produk secara asynchronous**

Karena kita perlu mengakses data produk secara asynchronous maka kita perlu menambahkan 1 buah function sesuai route yang kita tambahkan di file **web.php**. Buka file **ProdukController.php** kemudian tambahkan 1 buah function.

```php
public function loadasync($id)
{
    $itemproduk = Produk::findOrFail($id);
    $respon = [
        'status' => 'success',
        'msg' => 'Data ditemukan',
        'itemproduk' => $itemproduk
    ];
    return response()->json($respon, 200);
}
```

**#7. Tambahkan menu promo ke menu dashboard**

Buka file **menudashboard.blade.php** pada menu Produk tambahkan menu item di bawah menu item kategori.

```php+HTML
<li class="nav-item">
    <a href="{{ route('promo.index') }}" class="nav-link">
        <i class="far fa-circle nav-icon"></i>
        <p>Promo</p>
    </a>
</li>
```

**#8. Pindahkan link jquery ke tag `<head>`**

Buka file **dashboard.blade.php** kemudian *pindahkan* link jquery yang tadinya berikut ke atas `</head>`

```php+HTML
<script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
```

Terakhir jangan lupa jalankan perintah migrate untuk membuat table baru yang kita buat di file migrations tadi. Kembali ke terminal dan ketik perintah berikut kemudian pencet enter.

```php
php artisan migrate
```

Akses halaman http://toko-klowor.local/ dan login ke dashboard untuk membuka halaman dashboard produk promo.

![image-20220419134824413](img\image-20220419134824413.png)

![image-20220419134843511](img\image-20220419134843511.png)

### Menampilkan Slideshow dan Produk ke Homepage

Slideshow dan produk promo telah kita buatkan dashoardnya. Sekarang kita akan menampilkan ke homepage. Sebelumnya download 1 buah file gambar dan kasih nama **bag.jpg** kemudian pindahkan ke folder **public/images**.

![image-20220419135536504](img\image-20220419135536504.png)

File **bag.jpg** nanti akan kita gunakan sebagai gambar pengganti kalo semisal produk tidak ada gambarnya.

Seperti bab sebelumnya pada bagian ini terdiri dari beberapa langkah.

1. Koreksi file **HomepageController.php**
2. Koreksi file **views homepage/index.blade.php**
3. Koreksi file **KategoriController.php**
4. Koreksi file **views kategori/index.blade.php**

**#1. HomepageController.php**

Kita akan menampilkan **data produk, produk promo, kategori** dan **slideshow**. Maka, kita perlu menambahkan model dan juga mengedit `function index`.

```php+HTML
<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use App\Models\Produk;
use App\Models\ProdukPromo;
use App\Models\Slideshow;
use Illuminate\Http\Request;

class HomepageController extends Controller
{
    public function index()
    {
        $itemproduk = Produk::orderBy('created_at', 'desc')->limit(6)->get();
        $itempromo = ProdukPromo::orderBy('created_at', 'desc')->limit(6)->get();
        $itemkategori = Kategori::orderBy('nama_kategori', 'asc')->limit(6)->get();
        $itemslide = Slideshow::get();
        $data = array(
            'title' => 'Homepage',
            'itemproduk' => $itemproduk,
            'itempromo' => $itempromo,
            'itemkategori' => $itemkategori,
            'itemslide' => $itemslide,
        );
        return view('homepage.index', $data);
    }
    public function about()
    {
        $data = array('title' => 'Tentang Kami');
        return view('homepage.about', $data);
    }

    public function kontak()
    {
        $data = array('title' => 'Kontak Kami');
        return view('homepage.kontak', $data);
    }
    public function kategori()
    {
        $data = array('title' => 'Kategori Produk');
        return view('homepage.kategori', $data);
    }
}
```

**#2. Koreksi file homepage/index.blade.php**

Kita akan mengubah yang tadinya menampilkan data secara statis menjadi dinamis sesuai data yang kita masukkan lewat dashboard.

```php+HTML
@extends('layouts.template')
@section('content')
    <div class="container">
        <!-- carousel -->
        <div class="row">
            <div class="col">
                <div id="carousel" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        @foreach ($itemslide as $index => $slide)
                            @if ($index == 0)
                                <div class="carousel-item active">
                                    <img src="{{ \Storage::url($slide->foto) }}" class="d-block w-100" alt="...">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>{{ $slide->caption_title }}</h5>
                                        <p>{{ $slide->caption_content }}</p>
                                    </div>
                                </div>
                            @else
                                <div class="carousel-item">
                                    <img src="{{ \Storage::url($slide->foto) }}" class="d-block w-100" alt="...">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>{{ $slide->caption_title }}</h5>
                                        <p>{{ $slide->caption_content }}</p>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        <!-- end carousel -->
        <!-- kategori produk -->
        <div class="row mt-4">
            <div class="col col-md-12 col-sm-12 mb-4">
                <h2 class="text-center">Kategori Produk</h2>
            </div>
            @foreach ($itemkategori as $kategori)
                <!-- kategori pertama -->
                <div class="col-md-4">
                    <div class="card mb-4 shadow-sm">
                        <a href="{{ URL::to('kategori/' . $kategori->slug_kategori) }}">
                            @if ($kategori->foto != null)
                                <img src="{{ \Storage::url($kategori->foto) }}" alt="{{ $kategori->nama_kategori }}"
                                    class="card-img-top">
                            @else
                                <img src="{{ asset('images/bag.jpg') }}" alt="{{ $kategori->nama_kategori }}"
                                    class="card-img-top">
                            @endif
                        </a>
                        <div class="card-body">
                            <a href="{{ URL::to('kategori/' . $kategori->slug_kategori) }}" class="text-decoration-none">
                                <p class="card-text">{{ $kategori->nama_kategori }}</p>
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
            <!-- end kategori produk -->
            <!-- produk Promo-->
            <div class="row mt-4">
                <div class="col col-md-12 col-sm-12 mb-4">
                    <h2 class="text-center">Promo</h2>
                </div>
                @foreach ($itempromo as $promo)
                    <!-- produk pertama -->
                    <div class="col-md-4">
                        <div class="card mb-4 shadow-sm">
                            <a href="{{ URL::to('produk/' . $promo->produk->slug_produk) }}">
                                @if ($promo->produk->foto != null)
                                    <img src="{{ \Storage::url($promo->produk->foto) }}"
                                        alt="{{ $promo->produk->nama_produk }}" class="card-img-top">
                                @else
                                    <img src="{{ asset('images/bag.jpg') }}" alt="{{ $promo->produk->nama_produk }}"
                                        class="card-img-top">
                                @endif
                            </a>
                            <div class="card-body">
                                <a href="{{ URL::to('produk/' . $promo->produk->slug_produk) }}"
                                    class="text-decoration-none">
                                    <p class="card-text">
                                        {{ $promo->produk->nama_produk }}
                                    </p>
                                </a>
                                <div class="row mt-4">
                                    <div class="col">
                                        <button class="btn btn-light">
                                            <i class="far fa-heart"></i>
                                        </button>
                                    </div>
                                    <div class="col-auto">
                                        <p>
                                            <del>Rp. {{ number_format($promo->harga_awal, 2) }}</del>
                                            <br />
                                            Rp. {{ number_format($promo->harga_akhir, 2) }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                <!-- end produk promo -->
                <!-- produk Terbaru-->
                <div class="row mt-4">
                    <div class="col col-md-12 col-sm-12 mb-4">
                        <h2 class="text-center">Terbaru</h2>
                    </div>
                    @foreach ($itemproduk as $produk)
                        <!-- produk pertama -->
                        <div class="col-md-4">
                            <div class="card mb-4 shadow-sm">
                                <a href="{{ URL::to('produk/satu') }}">
                                    @if ($produk->foto != null)
                                        <img src="{{ \Storage::url($produk->foto) }}" alt="{{ $produk->nama_produk }}"
                                            class="card-img-top">
                                    @else
                                        <img src="{{ asset('images/bag.jpg') }}" alt="{{ $produk->nama_produk }}"
                                            class="card-img-top">
                                    @endif
                                </a>
                                <div class="card-body">
                                    <a href="{{ URL::to('produk/' . $produk->slug_produk) }}"
                                        class="text-decoration-none">
                                        <p class="card-text">
                                            {{ $produk->nama_produk }}
                                        </p>
                                    </a>
                                    <div class="row mt-4">
                                        <div class="col">
                                            <button class="btn btn-light">
                                                <i class="far fa-heart"></i>
                                            </button>
                                        </div>
                                        <div class="col-auto">
                                            <p>
                                                Rp. {{ number_format($produk->harga, 2) }}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <!-- end produk terbaru -->
                    <!-- tentang toko -->
                    <hr>
                    <div class="row mt-4">
                        <div class="col">
                            <h5 class="text-center">Toko Online Menggunakan Laravel</h5>
                            <p>
                                Toko adalah demo membangun toko online menggunakan laravel framework. Di dalam demo ini
                                terdapat user bisa menginput data kategori, produk dan transaksi.
                                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Hic laborum aliquam dolorum sequi
                                nulla maiores quos incidunt veritatis numquam suscipit. Cumque dolore rem obcaecati. Eos
                                quod ad non veritatis assumenda.
                            </p>
                            <p>
                                Toko adalah demo membangun toko online menggunakan laravel framework. Di dalam demo ini
                                terdapat user bisa menginput data kategori, produk dan transaksi.
                                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Hic laborum aliquam dolorum sequi
                                nulla maiores quos incidunt veritatis numquam suscipit. Cumque dolore rem obcaecati. Eos
                                quod ad non veritatis assumenda.
                            </p>
                            <p class="text-center">
                                <a href="" class="btn btn-outline-secondary">
                                    Baca Selengkapnya
                                </a>
                            </p>
                        </div>
                    </div>
                    <!-- end tentang toko -->
                </div>
            </div>
        </div>
    </div>
@endsection
```

**#3. KategoriController.php**

Karena pada tutorial sebelumnya banyak yang mengeluhkan error pada saat mengupload gambar kategori. Jadi kita koreksi saja `function uploadimage` dengan menghapus validasi "image".

```php
public function uploadimage(Request $request)
{
    $this->validate($request, [
        'image' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'kategori_id' => 'required',
    ]);
    $itemuser = $request->user();
    $itemkategori = Kategori::where('user_id', $itemuser->id)
        ->where('id', $request->kategori_id)
        ->first();
    if ($itemkategori) {
        $fileupload = $request->file('image');
        $folder = $request->file('image')->store('public/images');
        $itemgambar = (new ImageController())->upload($fileupload, $itemuser, $folder);
        $inputan['foto'] = $itemgambar->url; //ambil url file yang barusan diupload
        $itemkategori->update($inputan);
        return back()->with('success', 'Image berhasil diupload');
    } else {
        return back()->with('error', 'Kategori tidak ditemukan');
    }
}
```

**#4. Koreksi file views kategori/index.blade.php**

Kita tambahkan beberapa baris kode di atas `<div class="table-responsive">`

```php
@if (count($errors) > 0)
    @foreach ($errors->all() as $error)
        <div class="alert alert-warning">{{ $error }}</div>
    @endforeach
@endif
```

Sekarang akses http://toko-klowor.local/ untuk mengetes hasil jadinya.

![image-20220419140732659](img\image-20220419140732659.png)